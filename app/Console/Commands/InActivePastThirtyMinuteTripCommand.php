<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use \App\Models\UserTrip;

class InActivePastThirtyMinuteTripCommand extends Command
{
      /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inactive:trips';

    /**
     * The console command description.
     *
     * @var string 
     */
    protected $description = 'The Command will inactive all the pending trips by 30 minutes before';
 

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $now = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
            $schedule = \Carbon\Carbon::now()->subMinutes(10)->format('Y-m-d H:i:s');
            $pending_tips = UserTrip::where([['status','pending'],['driver_status','pending']])->whereNull('schedule_datetime')->get();
            foreach ($pending_tips as $key => $trips) {
                $created_at = \Carbon\Carbon::parse($trips->created_at)->addMinutes(30)->format('Y-m-d H:i:s');
                if($created_at < $now){
                    $trips->delete();
                }
            }
            //dd($pending_tips);

            UserTrip::whereNotNull('schedule_datetime')->where([['status','pending'],['driver_status','pending'],['schedule_datetime','<=',$schedule]])->delete();
        }catch(\Exception $e)
        {
            Log::warning("InActive User Trip Command Failed");
            Log::warning($e->getMessage());
        }
    }
}
