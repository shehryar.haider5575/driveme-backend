<?php

namespace App\DataTables;

use App\Models\BookRentalCar;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BookRentalCarDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
                    ->setRowId('id')
                    ->editColumn('select_date', function($model){
                        if($model->multiple_days == 0)
                        {
                            return $model->select_date;
                        }
                        if($model->multiple_days > 0)
                        {
                            return 'From: '.$model->date_from.' To: '.$model->date_to;
                        }
                    })
                    ->editColumn('multiple_days', function($model){
                        return $model->multiple_days<=1?'1 Day':$model->multiple_days.' Days';
                    })
                    ->editColumn('request_status', function($model){
                        $html = "<select name='request_status' onchange='changeStatus(this, {$model->id})'>
                                    <option value='pending' ".($model->request_status=='pending'?'selected':'').">Pending</option>
                                    <option value='accept' ".($model->request_status=='accept'?'selected':'').">Accept</option>
                                    <option value='reject' ".($model->request_status=='reject'?'selected':'').">Reject</option>
                        </select>";
                        return $html;
                    })
                    ->editColumn('status', function($model){
                        $html = "<select name='status' onchange='changebookingStatus(this, {$model->id})'>
                                    <option value='pending' ".($model->status=='pending'?'selected':'').">Pending</option>
                                    <option value='complete' ".($model->status=='complete'?'selected':'').">Complete</option>
                                    <option value='cancel' ".($model->status=='cancel'?'selected':'').">Cancel</option>
                        </select>";
                        return $html;
                    })
                    ->addColumn('action', 'pages.book_rental_cars.datatable_action')
                    ->rawColumns(['action', 'request_status', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\BookRentalCar $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BookRentalCar $model): QueryBuilder
    {
        return $model->with(['user' => function($q){
            $q->withTrashed();
        }, 'rental_car' => function($q){
            $q->withTrashed();
        }])->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('bookrentalcar-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make(['data'=>'id', 'name'=>'id', 'title'=>'Booking Id']),
            Column::make('rental_car_id'),
            Column::make('rental_car.name'),
            Column::make(['data' => 'user.full_name', 'name' => 'user.full_name', 'title' => 'User Name', 'searchable' => false, 'orderable' => false]),
            Column::make('select_date'),
            Column::make(['data'=>'multiple_days', 'name' => 'multiple_days', 'title'=>'No. of Days']),
            Column::make('time_from'),
            Column::make('time_to'),
            Column::make(['data'=>'status', 'name' => 'status', 'title' => 'Current Status']),
            Column::make('payment_status'),
            Column::make('request_status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'BookRentalCar_' . date('YmdHis');
    }
}
