<?php

namespace App\DataTables;

use App\Models\UserTrip;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class BookingListDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->setRowId('id')
        ->addColumn('action', 'pages.trips.datatable_action')
        ->editColumn('user_name', function(UserTrip $trip)
        {
            return $trip->user->full_name??null;
        })
        ->editColumn('chauffer_name', function(UserTrip $trip)
        {
            return is_null($trip->driver_id)?'N/A':$trip->driver->first_name??null.' '.$trip->driver->last_name??null;
        })
        ->editColumn('driver_status', function(UserTrip $trip)
        {
            $str = str_ireplace('_', ' ', $trip->driver_status);
            return $str;
        })
        ->editColumn('schedule_datetime', function(UserTrip $trip)
        {
            return $trip->schedule_datetime??'N/A';
        })
        ->editColumn('amount', function(UserTrip $trip)
        {
            return $trip->amount.' AED';
        })
        ->editColumn('created_at', function(UserTrip $trip)
        {
            return !is_null($trip->created_at)?$trip->created_at->diffForHumans():'';
        })
        ->editColumn('trip_type', function(UserTrip $trip)
        {
            return ucfirst(str_ireplace('_', ' ', $trip->trip_type));
        })
        ->editColumn('booking_type', function(UserTrip $trip)
        {
            if($trip->trip_type == 'garage_pickup' || $trip->trip_type == 'rta_inspection' || $trip->trip_type == 'inter_city') return 'One Time';
            return ucfirst(str_ireplace('_', ' ', $trip->booking_type));
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\UserTrip $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserTrip $model): QueryBuilder
    {
        $request = request();
        $query = $model->with(['user' => function($q){
            $q->withTrashed();
        }, 'driver' => function($q){
            $q->withTrashed();
        }]);

        if($request->has('trip_type') && !empty($request->trip_type))
        {
            $query = $query->where('trip_type', $request->trip_type);
        }

        if($request->has('ds') && !empty($request->ds))
        {
            $query = $query->where('driver_status', $request->ds);
        }

        if($request->has('booking_type') && !empty($request->booking_type))
        {
            if($request->booking_type == 'one_time' && empty($request->trip_type))
            {
                $query = $query->orWhere('trip_type', 'rta_inspection')
                               ->orWhere('trip_type', 'garage_pickup')
                               ->orWhere('trip_type', 'inter_city');
            }else
            {
                $query = $query->where('booking_type', $request->booking_type);
            }
        }
        return $query->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('bookinglist-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('created_at'),
            Column::make(['data'=>'id', 'name'=>'id', 'title'=>'Trip Id']),
            Column::make(['data'=>'user_name', 'name'=>'user_name', 'searchable' => false, 'orderable' => false]),
            Column::make(['data'=>'trip_type', 'name'=>'trip_type', 'searchable' => false, 'orderable' => false]),
            Column::make(['data'=>'booking_type', 'name'=>'booking_type', 'searchable' => false, 'orderable' => false]),
            Column::make(['data'=>'chauffer_name', 'name'=>'chauffer_name', 'searchable' => false, 'orderable' => false]),
            Column::make(['data'=>'schedule_datetime', 'name'=>'schedule_datetime', 'title' => 'Schedule']),
            Column::make(['data'=>'amount', 'name'=>'amount', 'title' => 'Trip Amount']),
            Column::make('status'),
            Column::make(['data'=>'driver_status', 'name'=>'driver_status', 'title' => 'Chauffer Status']),
            Column::make('payment_status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'BookingList_' . date('YmdHis');
    }
}
