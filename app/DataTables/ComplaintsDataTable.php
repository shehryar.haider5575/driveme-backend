<?php

namespace App\DataTables;

use App\Models\UserComplaint;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ComplaintsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('user_name', function(UserComplaint $complaint)
            {
                return $complaint->user()->withTrashed()->first()->full_name??'';
            })
            ->editColumn('trip_id', function(UserComplaint $complaint)
            {
                return $complaint->trips()->withTrashed()->first()->id??'';
            })
            ->editColumn('message', function(UserComplaint $complaint)
            {
                return substr($complaint->message, 0, 200);
            })
            ->editColumn('created_at', function(UserComplaint $complaint)
            {
                return !is_null($complaint->created_at)?$complaint->created_at->diffForHumans():'';
            })
            ->editColumn('status', 'pages.complaints.datatable_status')
            ->addColumn('action', 'pages.complaints.datatable_action')
            ->setRowId('id')
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\UserComplaint $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(UserComplaint $model): QueryBuilder
    {
        return $model->with(['trips'])->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('complaints-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make(['data'=>'id', 'name'=>'id', 'title'=>'Complaint Id']),
            Column::make(['data'=>'trips.driver_status', 'name'=>'trips.driver_status', 'title'=>'Driver Status', 'orderable'=>false, 'searchable'=>false]),
            Column::make(['data'=>'trips.status', 'name'=>'trips.status', 'title'=>'Trip Status', 'orderable'=>false, 'searchable'=>false]),
            Column::make('trip_id'),
            Column::make('user_name'),
            Column::make('message'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Complaints_' . date('YmdHis');
    }
}
