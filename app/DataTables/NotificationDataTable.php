<?php

namespace App\DataTables;

use App\Models\Notification;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use App\Models\User;
use Auth;

class NotificationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
                    ->setRowId('id')
                    ->editColumn('user_name', function($model){
                        $data = $model->data;
                        return $data['user_name']??$model->user()->withTrashed()->first()->full_name;
                    })
                    ->editColumn('subject', function($model){
                        $data = $model->data;
                        return $data['subject']??null;
                    })
                    ->editColumn('created_at', function($model){
                        return !is_null($model->created_at)?$model->created_at->diffForHumans():'';
                    })
                    ->editColumn('read_at', function($model){
                        return !is_null($model->read_at)?$model->read_at->diffForHumans():'';
                    })
                    ->addColumn('action', 'pages.notifications.datatable_action')
                    ->rawColumns(['subject', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Notification $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Notification $model): QueryBuilder
    {
        return $model->where('type', '!=', 'App\Notifications\CustomEmail')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('notification-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(2)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make(['data' => 'user_name', 'name' => 'user_name', 'searchable' => false, 'orderable' => false]),
            Column::make('subject'),
            Column::make('created_at'),
            Column::make('read_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('d-flex justify-content-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Notification_' . date('YmdHis');
    }
}
