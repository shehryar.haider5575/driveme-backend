<?php

namespace App\DataTables;

use App\Models\Payment;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PaymentHistoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('user.first_name', function(Payment $payment){
                return $payment->user->full_name??null;
            })
            ->editColumn('promocode', function(Payment $payment){
                return $payment->promocode;
            })
            ->editColumn('discount_value', function(Payment $payment){
                return $payment->discount_value;
            })
            ->editColumn('trip_id', function(Payment $payment){
                return $payment->trip_id;
            })
            ->editColumn('payment_type', function(Payment $payment){
                return $payment->payment_type=='stripe'?'card':$payment->payment_type;
            })
            ->editColumn('created_at', function(Payment $payment){
                return !is_null($payment->created_at)?$payment->created_at->diffForHumans():'';
            })
            ->editColumn('type', function(Payment $payment)
            {
                return ucfirst(str_ireplace('_', ' ', $payment->type));
            })
            ->addColumn('action', 'pages.payments.datatable_action')
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PaymentHistory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Payment $model): QueryBuilder
    {
        $request = request();
        $query = $model->with(['user' => function($q){
            $q->withTrashed();
        }]);
        if($request->has('type') && !empty($request->type))
        {
            $query = $query->where('type', $request->type);
        }

        if($request->has('pm') && !empty($request->pm))
        {
            $query = $query->where('payment_type', $request->pm);
        }

        return $query->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('paymenthistory-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('created_at'),
            Column::make(['data' => 'trip_id', 'name' => 'trip_id', 'title' => 'Trip Id']),
            Column::make(['data' => 'user.first_name', 'name' => 'user.first_name', 'title' => 'User Name', 'orderable' => false]),
            Column::make(['data'=>'type', 'name'=>'type', 'searchable' => false, 'orderable' => false]),
            Column::make(['data' => 'payment_type', 'name' => 'payment_type', 'title' => 'Payment Method']),
            Column::make(['data' => 'promocode', 'name' => 'promocode', 'title' => 'Avail Promocode']),
            Column::make('amount'),
            Column::make(['data' => 'discount_value', 'name' => 'discount_value', 'title' => 'Discount']),
            // Column::make('id'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'PaymentHistory_' . date('YmdHis');
    }
}
