<?php

namespace App\DataTables;

use App\Models\Promocode;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PromocodesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('started_at', function(Promocode $promocode){
                return !is_null($promocode->started_at)?$promocode->started_at->diffForHumans():'';
            })
            ->editColumn('created_at', function(Promocode $promocode){
                return !is_null($promocode->created_at)?$promocode->created_at->diffForHumans():'';
            })
            ->editColumn('expired_at', function(Promocode $promocode){
                return !is_null($promocode->expired_at)?$promocode->expired_at->diffForHumans():'';
            })
            ->addColumn('action', 'pages.promocodes.datatable_action')
            ->setRowId('id');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Promocode $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Promocode $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('promocodes-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(0)
                    ->selectStyleSingle()
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     *
     * @return array
     */
    public function getColumns(): array
    {
        return [
            Column::make('id'),
            Column::make('title'),
            Column::make('promocode'),
            Column::make('discount_type'),
            Column::make('discount_value'),
            Column::make(['data'=>'status', 'name'=>'status', 'title' => 'Validity Status']),
            Column::make('started_at'),
            Column::make('expired_at'),
            Column::make('created_at'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Promocodes_' . date('YmdHis');
    }
}
