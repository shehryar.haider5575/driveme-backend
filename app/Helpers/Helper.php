<?php

namespace App\Helpers;

use App\Models\Ledger;
use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Exception;
use Twilio\Rest\Client;
use Log;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class Helper {

    public static function file_upload(Request $request, string $field_name, string $dir)
    {
        return "storage/app/".$request->file($field_name)->storePubliclyAs(
            $dir,$request->file($field_name)->hashName()
        );
    }

    public static function file_upload_with_array($request, string $field_name, string $dir)
    {
        return "storage/app/".$request[$field_name]->storePubliclyAs(
            $dir,$request[$field_name]->hashName()
        );
    }

    public static function file_delete(string $file_path)
    {
        $file = str_ireplace("storage/app/",'', $file_path);
        if(Storage::exists($file)){
            Storage::delete($file);

            return true;
        }
        return false;
    }
    public static function reqValue($key): string
    {
        if(empty(\request()->toArray()[$key])){
            return "";
        }
        return \request()->toArray()[$key];
    }
    public static function dates_month($month, $year,$format='d-M-Y') {
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $dates_month = array();

        for ($i = 1; $i <= $num; $i++) {
            $mktime = mktime(0, 0, 0, $month, $i, $year);
            $date = date($format, $mktime);
            $dates_month[$i] = $date;
        }

        return $dates_month;
    }


    public static function rangeMonth (): array
    {
        $months = [];
        for ($m=1; $m<=12; $m++) {
            $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
            $months[]= $month;
        }
        return $months;
    }

    public static function getBalance($natureId,$nature){
        $balance = Ledger::where("nature_id",$natureId)->where('nature',$nature)->orderBy('id', 'desc')->get('balance')->first()['balance'];
        return (!empty($balance) ? $balance : 0);
    }

    public static function price($amount): string
    {
        $format = getenv('MIX_APP_PRICE_FORMAT');
        return str_ireplace("#AMOUNT",number_format(floatval($amount)),$format);
    }

    /**
     * Get Specified Site Setting
     * @return array
     */
    public static function getGeneralSetting($key)
    {
        $setting =  GeneralSetting::where('key',$key)->first();
        if(!empty($setting))
        {
            return $setting->value;
        }
        return "";
    }

    public static function sendSms($number, $message)
    {
        $receiverNumber = $number;
        if(strstr($number, '+'))
        {
            $receiverNumber = $number;
        }else
        {
            $receiverNumber = '+'.$number;
        }

        try {
		
	    // Log::debug($receiverNumber);
        //     $token = env("TWILIO_AUTH_TOKEN");
        //     $twilio_sid = env("TWILIO_SID");
        //     $twilio_verify_sid = env("TWILIO_VERIFY_SID");
        //     $twilio = new Client($twilio_sid, $token);
            
	    // $twilio->messages->create($receiverNumber, [
	   	// 'from' => '+12543182585',
		// 'body' => $message
 	    // ]);
            
	    return true;
        } catch (Exception $e) {
            Log::debug("Error Send SMS Twilio in Helper ");
            Log::debug($e->getMessage());

            return false;
        }
    }

    public static function Charge(Request $request)
    {
        $stripe = new Stripe(env('STRIPE_SECRET'));
        try {
            $token = $stripe->tokens()->create([
                'card' => [
                'number' => $request->get('card_no'),
                'exp_month' => $request->get('ccExpiryMonth'),
                'exp_year' => $request->get('ccExpiryYear'),
                'cvc' => $request->get('cvc'),
                ],
            ]);
        if (!isset($token['id'])) {
            throw new Exception("Stripe Issue", 1);
        }
        $charge = $stripe->charges()->create([
            'currency' => env('CASHIER_CURRENCY'),
            'amount' => (int)$request->get('amount'),
            'card' => $token['id'],
            'description' => $request->get('decription'),
            ]);

        if($charge['status'] == 'succeeded') {
            return $charge;
        } else {
            throw new Exception('Money not add in wallet!!', 1);
        }
        } catch (Exception $e) {
            return ['message' => $e->getMessage(), 'success' => false];
        }

    }
}

?>
