<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Lang;
use App\Notifications\PasswordResetLink;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;
use Validator;
use App\Helpers\Helper;

class ForgotPasswordController extends Controller
{

    public function sendResetLinkEmail(Request $request)
    {
        try
            {
            $validate = Validator::make($request->all(), [
                'email' => 'required|exists:users,email',
            ]);

            if($validate->fails())
            {
                return $this->sendErrorResponse($validate->errors()->first(), 'Email not Found', 409);
            }

            $user = User::where('email', $request->email)->first();

            $token = rand(1000, 9999);

            $resetPassword = DB::table('password_resets')->where(['email' => $user->email])->first();

            if(!$resetPassword)
            {
                DB::table('password_resets')->insert(['email' => $user->email, 'token' => $token, 'created_at' => Carbon::parse(now())]);
            }else
            {
                DB::table('password_resets')->where(['email' => $user->email])->update(['token' => $token]);
            }

            Helper::sendSms($user->phone, "Dear {$user->full_name} Your OTP code is {$user->remember_token}");
            $user->notify(new PasswordResetLink($token));


            return $this->sendResponse(['remember_token' => $token,'message' => 'We sent you an 4 Digit OTP '.$token], 'OTP Sent Successfully',200);
        }catch(\Exception $e)
        {
            return $this->sendErrorResponse($e->getMessage(), 'Something went Wrong', 400);
        }

    }

   public function validateOTP(Request $request)
   {
        try
            {
            $validate = Validator::make($request->all(), [
                'email' => 'required|exists:password_resets,email',
                'token' => 'required|exists:password_resets,token'
            ]);

            if($validate->fails())
            {
                return $this->sendErrorResponse($validate->errors()->first(), 'Invalid Token or Expired', 409);
            }

            $delete = DB::table('password_resets')->where(['email' => $request->email ,'token' => $request->token])->delete();

            return $this->sendResponse('OTP Matched Successfully', 'OTP Matched', 200);
        }catch(\Exception $e)
        {
            return $this->sendErrorResponse($e->getMessage(), 'Something went Wrong', 400);
        }
   }

   public function resetPassword(Request $request)
   {
        try
            {
            $validate = Validator::make($request->all(), [
                'email' => 'required|exists:users,email',
                'password' => 'required|min:8',
                'cpass' => 'required|same:password'
            ]);

            if($validate->fails())
            {
                return $this->sendErrorResponse($validate->errors()->first(), 'Invalid Token or Expired', 409);
            }

            $user = User::where('email', $request->email)->first();
            $user->password = Hash::make($request->password);
            $user->save();

            return $this->sendResponse('Password Updated', 'password Changed Successfully', 200);
        }catch(\Exception $e)
        {
            return $this->sendErrorResponse($e->getMessage(), 'Something went Wrong', 400);
        }
   }

}
