<?php

namespace App\Http\Controllers\API\Auth;

use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\AdminNewRegistration;
use Illuminate\Support\Facades\Validator;
use App\Notifications\PasswordResetLink;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use App\Models\User;
use App\Models\Promocode;
use App\Models\PromoCodeUser;
use Carbon\Carbon;
use Log;
use Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        if($this->_checkUserType() == 'CAPTAIN')
        {
            $validate = $this->_clientValidation($request->all());
            if($validate->fails())
            {
                return $this->sendErrorResponse($validate->errors()->first(), 'Sinup Failed', 409);
            }
        }else{
            if($this->validator($request->all())->fails())
            {
                return $this->sendErrorResponse($this->validator($request->all())->errors()->first(), 'Sinup Failed', 409);
            }
            $checkuser = User::where('email',$request->email)->orWhere('phone',$request->phone)->first();

            if(!empty($checkuser)){
                return $this->sendErrorResponse('Email or Phone Already exists, please login', 'Email or Phone Already exists, please login', 409);
            }
            // {
            //     return $this->sendErrorResponse('Not Verified', 'Not Verified', 402);
            // }else{
            // }

        }

        if($this->_checkUserType() != 'CAPTAIN')
        {
            unset($request['pay_per_minute']);
            unset($request['pay_per_hour']);
        }

        event(new Registered($user = $this->create($request->all())));
        if($request->has('driver_license_front_image'))
        {
            $driver_license_front = Helper::file_upload($request, 'driver_license_front_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_front, 'type' => 'driver_license_front']);

        }

        if($request->has('driver_license_back_image'))
        {
            $driver_license_back = Helper::file_upload($request, 'driver_license_back_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_back, 'type' => 'driver_license_back']);
        }
        if($request->has('driver_emirate_front_image'))
        {
            $driver_emirate_front = Helper::file_upload($request, 'driver_emirate_front_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_emirate_front, 'type' => 'driver_emirate_front']);

        }

        if($request->has('driver_emirate_back_image'))
        {
            $driver_emirate_back = Helper::file_upload($request, 'driver_emirate_back_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_emirate_back, 'type' => 'driver_emirate_back']);
        }

        if($request->has('profile_image'))
        {
            if(is_file($request->profile_image))
            {
                $profile_image = Helper::file_upload($request, 'profile_image', 'users');
                $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
            }
            if(is_string($request->profile_image))
            {
                $path = $request->profile_image;
                $contents = file_get_contents($path);
                $name = 'captains/'.substr($path, strrpos($path, '/') + 1);
                $image = Storage::put($name, $contents);
                $profile_image = 'storage/app/'.$name;
                $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
            }
        }

        if($request->has('passport_pic'))
        {
            $profile_image = Helper::file_upload($request, 'passport_pic', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'passport_pic']);
        }

        if($request->has('refferal_code') && !empty($request->refferal_code))
        {
            $user_refferal = User::where('refferal_code', $request->refferal_code)->first();
            $promo = Promocode::find(1);
            PromocodeUser::create([
                'user_id'       => $user_refferal->id,
                'promocode_id'  => $promo->id,
                'status'        => 'active',
            ]);
            $user->refferals()->create(['refferal_code' => $request->refferal_code, 'refferal_user_id' => $user_refferal->id, 'points' => $promo->discount_value??0]);
        }

        if($request->has('address'))
        {
            $user->user_address()->create(['address' => $request->address, 'latitude' => $request->latitude, 'longitude' => $request->longitude, 'type' => $request->address_type??'home']);
        }

        if($request->has('device_token'))
        {
            $user->device_token = $request->device_token;
            $user->save();
        }

        $token = rand(1000, 9999);
        $user->refferal_code = rand(1000, 9999).$user->id.date('Y');
        $user->remember_token = $token;
        $user->is_archive = 0;
        if($this->_checkUserType() == 'CAPTAIN') $user->is_admin_approve = '0';
        $user->save();
        // try
        // {
        //     // $user->notify(new PasswordResetLink($token));
        // }catch(\Exception $e)
        // {
        //     Log::debug("OTP Email Failed");
        //     Log::debug($user);
        //     Log::debug($e->getMessage());
        // }

        $this->guard()->login($user);
        $response = $this->registered($request, $user);
        $response = Auth::user()->load(['promo_codes.promocode']);
        $response['user_promo_codes'] = array_filter($response->promo_codes->pluck('promocode')->toArray());
        $response['rta']    = Helper::getGeneralSetting('garage_pickup');
        $response['garage'] = Helper::getGeneralSetting('rta_charges');
        $response['otp_token'] = $token;
        $response['otp_code'] = $token;

        return $this->sendResponse($response, 'Registration Successfully', 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required_unless:phone,null', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'same:cpass'],
            'cpass' => ['required', 'string', 'min:8'],
            'phone' => ['required_unless:email,null', 'numeric'],
            'refferal_code' => ['nullable', 'exists:users,refferal_code'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => !empty($data['password']) ? Hash::make($data['password']) : null,
            'type' => $this->_checkUserType(),
            'phone' => $data['phone']??null,
            'is_admin_approve' => $this->_checkUserType()=='CAPTAIN'?'0':'1',
            'pay_per_minute' => $data['pay_per_minute']??null,
            'pay_per_hour' => $data['pay_per_hour']??null,
            'emirate' => $data['emirate'] ?? null,
            'city' => $data['city'] ?? null,
            'dob' => !empty($data['dob']) ? Carbon::parse($data['dob'])->format('Y-m-d') : null,
            'similar_app' => $data['similar_app'] ?? null,
            'vendor_app' => $data['vendor_app'] ?? null,
            'disability' => $data['disability'] ?? null,
            'emirates_id' => $data['emirates_id'] ?? null,
            'license_number' => $data['license_number'] ?? null,
            'driver_license_front_image_website' => $data['driver_license_front_image_website'] ?? null,
            'driver_license_back_image_website' => $data['driver_license_back_image_website'] ?? null,
            'driver_emirate_front_image_website' => $data['driver_emirate_front_image_website'] ?? null,
            'driver_emirate_back_image_website' => $data['driver_emirate_back_image_website'] ?? null,
        ]);
    }

    protected function _checkUserType(): String
    {
        return strtoupper(request()->route()->getName());
    }

    protected function registered(Request $request, $user)
    {
        $user->token = $user->createToken('bearer')->accessToken;
        Helper::sendSms($user->phone, "Dear {$user->full_name} Your OTP code is {$user->remember_token}");
        try
        {
            $admin = User::where('type', 'ADMIN')->first();
            $admin->notify(new AdminNewRegistration($user));
        }catch(\Exception $e)
        {
            Log::debug('Email Send Fail IN Register Controller');
            Log::debug($e->getMessage());
        }

        return $user;
    }

    protected function _clientValidation(array $data)
    {
	// Log::debug("Signup data");
	// Log::debug($data);
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required_unless:phone,null', 'string', 'max:255', 'unique:users,email'],
            'emirate' => ['nullable', 'string', 'max:255'],
            'city' => ['nullable', 'string', 'max:255'],
            'dob' => ['nullable'],
            'driver_license_front_image' => ['nullable', 'file', 'max:1024', 'mimes:jpeg,png,jpg,gif'],
            'driver_license_back_image' => ['nullable', 'file', 'max:1024', 'mimes:jpeg,png,jpg,gif'],
            'driver_emirate_front_image' => ['nullable', 'file', 'max:1024', 'mimes:jpeg,png,jpg,gif'],
            'driver_emirate_back_image' => ['nullable', 'file', 'max:1024', 'mimes:jpeg,png,jpg,gif'],
            // 'profile_image' => ['nullable', 'image', 'max:1024', 'mimes:jpeg,png,jpg,gif'],
            'similar_app' => ['nullable'],
            'vendor_app' => ['nullable'],
            'disability' => ['nullable'],
            'password' => ['nullable', 'string', 'min:8', 'same:cpass'],
            'cpass' => ['nullable', 'string', 'min:8'],
            'phone' => ['required_unless:email,null', 'numeric', 'unique:users,phone'],
            'refferal_code' => ['nullable', 'exists:users,refferal_code'],
            'emirates_id' => ['nullable', 'string', 'max:255', 'unique:users,emirates_id'],
            'license_number' => ['nullable', 'string', 'max:255' , 'unique:users,license_number'],
            'pay_per_minute' => ['nullable', 'double'],
            'pay_per_hour' => ['nullable', 'double'],
            'driver_license_front_image_website' => ['nullable'],
            'driver_license_back_image_website' => ['nullable'],
            'driver_emirate_front_image_website' => ['nullable'],
            'driver_emirate_back_image_website' => ['nullable'],

        ]);
    }
}
