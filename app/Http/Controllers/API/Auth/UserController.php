<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Models\BankDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helper;
use App\Models\Promocode;
use App\Models\PromocodeUser;
use Carbon\Carbon;
use Validator;
use Hash;

class UserController extends Controller
{
    public function __construct() {
        $this->middleware('auth:api');
    }

    public function user()
    {
        $response = Auth::user()->load(['promo_codes.promocode']);
        $response['user_promo_codes'] = array_filter($response->promo_codes->pluck('promocode')->toArray());
        $response['rta']    = Helper::getGeneralSetting('garage_pickup');
        $response['garage'] = Helper::getGeneralSetting('rta_charges');
        return $this->sendResponse($response, 'User Retrived', 200);
    }

    public function updateprofile(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'email' => ['nullable','string', 'max:255', 'unique:users,email,'.$user->id]
        ]);
        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Update Profile Failed', 409);
        }
        if ($request->hasFile('imageFile')) {
            if(!empty($user->profile_image)){

                Helper::file_delete($user->profile_image);

                $user->profile_image()->delete();
            }

            $profile_image = Helper::file_upload($request, 'imageFile', 'users');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
        }

        if($request->has('address'))
        {
            $user->user_address()->create(['address' => $request->address, 'latitude' => '24.8295871', 'longitude' => '67.041746', 'city' => $request->city??null, 'type' => $request->address_type??'home']);
            // $user->user_address()->create(['address' => $request->address, 'latitude' => $request->latitude, 'longitude' => $request->longitude, 'city' => $request->city??null, 'type' => $request->address_type??'home']);
        }

        if($request->has('driver_license_front_image') && $user->type == 'CAPTAIN')
        {
            $license = $user->attachments()->where('attachments.type', 'driver_license_front')->first();
            if($license)
            {
                Helper::file_delete($license->file_path);
                $license->delete();
            }

            $driver_license_front = Helper::file_upload($request, 'driver_license_front_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_front, 'type' => 'driver_license_front']);

        }

        if($request->has('driver_license_back_image') && $user->type == 'CAPTAIN')
        {
            $license = $user->attachments()->where('attachments.type', 'driver_license_back')->first();
            if($license)
            {
                Helper::file_delete($license->file_path);
                $license->delete();
            }

            $driver_license_front = Helper::file_upload($request, 'driver_license_back_image', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_front, 'type' => 'driver_license_back']);
        }

        $user->update($request->except('imageFile'));
        $response = Auth::user()->load(['promo_codes.promocode']);
        $response['user_promo_codes'] = array_filter($response->promo_codes->pluck('promocode')->toArray());
        $response['rta']    = Helper::getGeneralSetting('garage_pickup');
        $response['garage'] = Helper::getGeneralSetting('rta_charges');
        return $this->sendResponse($response, 'User Profile Update', 200);
    }

    // public function addBankDetail(Request $request)
    // {
    //     $user = Auth::user();
    //     $request['user_id'] = $user->id;
    //     $bank = BankDetail::create($request->all());
    //     return $this->sendResponse($bank, 'Bank Detail Successfully added', 200);
    // }

    // public function userBankDetailList()
    // {
    //     $user = Auth::user();
    //     return $this->sendResponse($user->bank, 'Retrived Bank Detail', 200);
    // }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|alpha_num|min:8|same:confirm_password',
            'confirm_password' => 'required',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Somrthing went wrong..', 400);
        }

        $user = auth()->user();
        if(Hash::check($request->current_password, $user->password))
        {
            $user->password = Hash::make($request->new_password);
            $user->save();
        }else
        {
            return $this->sendErrorResponse('Current Password is Incorrect', 'Password not Match', 400);
        }

        return $this->sendResponse('Password Updated Successfully', 'Password changed', 200);

    }

    public function verify_otp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|exists:users,remember_token',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Invalid OTP', 400);
        }

        $user = auth()->user();
        if(!$user) return $this->sendErrorResponse('Login First', 'Your not Login', 400);

        if($user->remember_token != $request->otp)
        {
            return $this->sendErrorResponse('Invalid OTP', 'Invalid OTP', 400);
        }

        $user->remember_token = null;
        $user->is_archive = null;
        $user->save();
        // $user->token = $user->createToken('bearer')->accessToken;
        $response = Auth::user()->load(['promo_codes.promocode']);
        $response['user_promo_codes'] = array_filter($response->promo_codes->pluck('promocode')->toArray());
        $response['rta']    = Helper::getGeneralSetting('garage_pickup');
        $response['garage'] = Helper::getGeneralSetting('rta_charges');
        return $this->sendResponse($response, 'Otp Verified Successfully', 200);
    }

    public function getNotifications()
    {
        $user = Auth::user();
        return $this->sendResponse($user->unreadNotifications, 'All Unread Notifications', 200);
    }

    public function getNotificationRead($id)
    {
        $notification = auth()->user()->notifications()->where('id', $id)->first();

        if ($notification) {
            $notification->markAsRead();

            return $this->sendResponse($notification, 'Read Notification', 200);
        }

        return $this->sendErrorResponse('Notification not Found', 'Read Notification', 409);
    }

    public function readAllNotification()
    {

        Auth::user()->unreadNotifications->markAsRead();

        return $this->sendResponse([], 'Read All Notifications', 200);
    }

    public function promocodes()
    {
        $user_pormos = PromocodeUser::where([['user_id',auth()->user()->id],['status','active']])->get();
        $promos = [];
        if(!empty($user_pormos)){
            $promo_ids = $user_pormos->pluck('promocode_id')->toArray();
            $promos = Promocode::where('status', 'active')->whereIn('id',$promo_ids)->whereDate('expired_at', '<=', date('Y-m-d H:i:s'))->paginate();
        }

        return $this->sendResponse($promos, 'All Promocodes', 200);
    }

    public function DeleteUser()
    {
        $user = Auth::user();
        $user->status = 3;
        $user->save();

        $user->delete();

        return $this->sendResponse('Your Account Has been Deleted', 'Deleted Successfully', 201);
        // $user->logout();
    }

    public function verified(Request $request){
        $user = Auth::user();
        $user->is_archive = 1;
        $user->save();
        return $this->sendResponse('Verification Done', 'Verification Successfully', 201);
    }
}
