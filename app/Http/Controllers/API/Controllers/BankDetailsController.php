<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BankDetail;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Carbon\Carbon;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class BankDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = BankDetail::where('user_id', auth()->user()->id)->get()->toArray();

        return $this->sendResponse($banks, 'All Bank Details', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'bank_country' => 'required',
            'account_number' => 'required',
            'bank_name' => 'required_if:type,=,bank',
            'bank_currency' => 'required',
            'account_holder_name' => 'required',
            'type' => 'required|in:bank,card,others',
            'branch_code' => 'required_if:type,=,bank',
            'address' => 'required_if:type,=,card',
            'expiry_date' => 'required_if:type,=,card',
            'cvc' => 'required_if:type,=,card',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }
        $user = Auth::user();
        $BankDetail = BankDetail::where([['user_id',$user->id],['account_number',$request->account_number]])->first();
        if(!empty($BankDetail)){
            return $this->sendErrorResponse('Card already exists', 'Card already exists..', 409);
        }
        $expiry = explode('/', $request->expiry_date);

        $card_data = [
            'card' => [
                'number' => $request->get('account_number'),
                'exp_month' => $expiry[0],
                'exp_year' => Carbon::createFromFormat('y', $expiry[1])->format('Y'),
                'cvc' => $request->get('cvc'),
            ],
        ];
        $currentYear = date('Y');
        $enteredYear = $card_data['card']['exp_year'];
        if ($enteredYear < $currentYear || $enteredYear > $currentYear + 10) {
            return $this->sendErrorResponse('Invalid card year', 'Invalid card year..', 409);
        }
        try {
            $stripe = new Stripe(env('STRIPE_SECRET'));
            $token = $stripe->tokens()->create($card_data);
            $request['user_id'] = $user->id;
            $bank = BankDetail::create($request->all());

        }
        catch (\Exception $e){
            // Handle card-related errors
          $message = $e->getMessage();
          return $this->sendErrorResponse($message, 'Invalid Card..', 409);
        }
        if (!isset($token['id'])) {
            throw new Exception("Stripe Issue", 1);
        }
        return $this->sendResponse($bank, 'Bank Detail Successfully added', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bank = BankDetail::where('user_id', auth()->user()->id)->where('id', $id)->first();

        return $this->sendResponse($bank, 'Bank Details', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'bank_country' => 'required',
            'account_number' => 'required',
            'bank_name' => 'required_if:type,=,bank',
            'bank_currency' => 'required',
            'account_holder_name' => 'required',
            'branch_code' => 'required_if:type,=,bank',
            'address' => 'required_if:type,=,card',
            'expiry_date' => 'required_if:type,=,card',
            'cvc' => 'required_if:type,=,card',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        $user = Auth::user();
        $request['user_id'] = $user->id;
        $bank = BankDetail::where('user_id', auth()->user()->id)->where('id', $id)->first();
        if(!$bank) return $this->sendErrorResponse('Invalid Bank Id', 'Bank Not Found', 409);

        $bank->update($request->except(['type']));

        return $this->sendResponse($bank, 'Bank Detail Successfully Updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
