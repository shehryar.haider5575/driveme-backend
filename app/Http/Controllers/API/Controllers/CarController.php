<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Car;
use App\Models\CarBrand;
use Validator;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cars = Car::where('user_id', auth()->user()->id)->orderBy('created_at', 'desc')->paginate(100);
        $data = [];
        foreach ($cars as $key => $car) {
            $brand_logo = CarBrand::where('name','LIKE','%'.$car->brand.'%')->first();
            $data[$key] = $car;
            $data[$key]['car_brand'] = $brand_logo->logo ?? '';
        }
        return $this->sendResponse($cars, 'Cars Retrieved Sucessfully', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string|max:256',
            'model' => 'required',
            'brand' => 'required',
            'plate_number' => 'required|unique:cars,plate_number',
            'transmission_type' => 'required|in:manual,automatic,both',
            'is_insured' => 'required|boolean',
            'type' => 'nullable',
        ])->validate();

        $car = auth()->user()->cars()->insert(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        return $this->sendResponse($car, 'Car Added Successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = Car::where('user_id', auth()->user()->id)->find($id);

        return $this->sendResponse($car, 'Car Retrieve Successfully', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:256',
            'model' => 'required|string',
            'plate_number' => 'required|unique:cars,plate_number,'.$id,
            'transmission_type' => 'required|in:manual,automatic,both',
            'is_insured' => 'required|boolean',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        $car = Car::where('id', $id)->update($request->all());

        return $this->sendResponse($car, 'Car Updated Successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = Car::find($id);

        $car->delete();

        return $this->sendResponse('', 'Car Deleted Successfully', 201);
    }

    public function getBrands()
    {
        $brands = CarBrand::with(['models' => function($query)
        {
            $query->where('status', 'active');

        }])->where('status', 'active')->get();
        $data = [];
        foreach ($brands as $key => $brand) {
            $data[$key] = $brand;
            $data[$key]['logo'] = trim($brand->logo);
        }
        return $this->sendResponse($brands, 'Car Brands and Models', 200);
    }
}
