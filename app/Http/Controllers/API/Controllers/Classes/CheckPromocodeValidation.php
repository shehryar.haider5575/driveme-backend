<?php

namespace App\Http\Controllers\API\Controllers\Classes;

use App\Models\Promocode;
use Exception;
use Log;

class CheckPromocodeValidation
{
    private static $code, $request, $promocode;

    public function __construct($promo_code, $request)
    {
        self::$code = $promo_code;
        self::$request = $request;
    }

    private static function isPromoExists()
    {
        try {
            $prmocodes = Promocode::where('promocode', self::$code)->first();
            if(!$prmocodes) throw new Exception("Invalid Promo Code", 1);
            self::$promocode = $prmocodes;
            return true;

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    private static function IsStarted()
    {
        try {

            $check = !is_null(self::$promocode->started_at)?date_diff(date_create(date('Y-m-d')), date_create(self::$promocode->started_at)):0;
            $check = !$check?0:$check->format('%R%a');


            if($check > 0) throw new Exception("Promocode can not be used before ".self::$promocode->started_at->format('j M Y'), 1);

            return true;
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    private static function IsExpired()
    {
        try {

            $check = !is_null(self::$promocode->expired_at)?date_diff(date_create(date('Y-m-d')), date_create(self::$promocode->expired_at)):0;
            $check = !$check?0:$check->format('%R%a');


            if($check < 0) throw new Exception("Promocode is Expired ".self::$promocode->expired_at->format('j M Y'), 1);

            return true;
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    private static function promoType()
    {
        $promoType = self::$promocode->discount_type??null;

        if(!$promoType) return false;

        return $promoType;
    }

    private static function checkAmount()
    {
        try {
            if(isset(self::$request->amount)) return true;

            throw new Exception("Request dosen't have Amount", 1);

        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }

    public static function applyDisscount($price)
    {
        $disscountType = self::$promocode->discount_type;
        $discount = self::$promocode->discount_value;
        if($disscountType == 'percent')
        {
            $discount = (float)$discount/100;
            $price = (float)$price - ($price * $discount);
        }

        if($disscountType == 'fixed')
        {
            $price = (float)$price - (float)$discount;
            if($price < 0) $price = 0;
        }

        return $price;
    }

    public static function ApplyPromocode()
    {
        try {
            if(!is_bool(self::isPromoExists())) throw new Exception(self::isPromoExists(), 1);

            if(!is_bool(self::IsStarted())) throw new Exception(self::IsStarted(), 1);

            if(!is_bool(self::IsExpired())) throw new Exception(self::IsExpired(), 1);

            if(!is_bool(self::checkAmount())) throw new Exception(self::checkAmount(), 1);

            $result = self::applyDisscount(self::$request->amount);

            if(!is_numeric($result)) throw new Exception($result, 1);

            return $result;

        } catch (\Exception $e) {
            Log::debug("Promocode Class Error");
            Log::debug($e->getMessage());
            return $e->getMessage();
        }
    }
}
