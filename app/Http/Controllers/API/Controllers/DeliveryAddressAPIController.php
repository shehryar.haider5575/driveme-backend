<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserAddress;
use Auth;
use Validator;

class DeliveryAddressAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $address = auth()->user()->user_address()->get()->toArray();

        return $this->sendResponse($address, 'Your Addresses', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'type' => 'required|in:home,office,other',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        $address = auth()->user()->user_address()->create(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        return $this->sendResponse($address, 'Address Added Successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'type' => 'required|in:home,office,other',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }
        $address = UserAddress::find($id);
        $address->update($request->except('user_id'));

        return $this->sendResponse($address, 'Address Updated Successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
