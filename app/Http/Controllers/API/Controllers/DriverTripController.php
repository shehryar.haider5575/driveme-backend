<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserTrip;
use App\Models\User;
use App\Models\Promocode;
use App\Models\PromocodeUser;
use App\Models\Payment;
use Illuminate\Support\Collection;
use App\Notifications\RideStatusUpdate;
use App\Events\LocateDriver;
use Log;
use Validator;
use Illuminate\Support\Facades\Event;
use LRedis;
use Carbon\Carbon;
use App\Helpers\Helper;
use DB;
use Str;
use Auth;
use Exception;
use App\Models\BankDetail;
class DriverTripController extends Controller
{
    public function __constructor()
    {
	    date_default_timezone_set('UTC');
 	    $this->middleware('auth');
    }

    public function searchRide()
    {
        date_default_timezone_set('UTC');
        $latitude = auth()->user()->address->latitude??null;
        $longitude = auth()->user()->address->longitude??null;
        // if(!$latitude && !$longitude)
        // {
        //     $trips = [];
        // }else
        // {
            // nearBy($latitude, $longitude)
            $trips = UserTrip::with(['pickup_address', 'dropoff_address', 'car','user', 'avail_promocode','payment','topupDetails','tipDetails','rentCarsDetails'])
                    ->whereHas('user', function($q){
                        $q->where('status', 1);
                    })
                    ->where('driver_id', NULL)
                    ->where('driver_status', '<>', 'accept')
                    ->where('status', '!=', 'cancel')
                    ->orderBy('created_at','desc')
                    ->get();
        // }

        return $this->sendResponse($trips, 'Near By Trips', 200);
    }

    public function RideStatus(Request $request, $trip_id)
    {
        try{
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:accept,reject,timeout,complete,confirm_arrival,start_drive,end_drive',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        $trip =  UserTrip::with(['pickup_address', 'dropoff_address', 'car' => function($q){
            $q->withTrashed();
        }, 'user' => function($q){
            $q->withTrashed();
        },'paymentDetails','avail_promocode','payment','topupDetails','tipDetails','rentCarsDetails'])->where('id',$trip_id)->first();
        if(!$trip)
        {
            return $this->sendErrorResponse('Invalid Trip ID', 'Something went wrong.', 409);
        }
        $driver = auth()->user();
        $now = carbon::now();
        $total_amount = $trip->amount;
        $msg = str_replace('_',' ',strtoupper($request->status));
        if($request->status == 'start_drive')
        {
            // Schedule Ride Case
            if(!empty($trip->schedule_datetime)){
                $schedule_datetime_after15 = carbon::parse($trip->schedule_datetime);
                $totalMinutes = $now->diffInMinutes($schedule_datetime_after15);
                if($totalMinutes > 15){
                    return $this->sendErrorResponse('You can only start the ride before 15 minutes of scheduled time', 'failed.', 409);
                }
            }

        }

        if($request->status == 'accept')
        {
            $msg = 'The ride has been accepted';
            // One Ride Accepted at the same time case
            $now = carbon::now();
            $driver_current_accept_trip = UserTrip::whereIn('driver_status',['accept','confirm_arrival','start_drive'])->where([['id','!=',$trip->id],['driver_id',auth()->user()->id],['status','!=','cancel']])->whereNull('schedule_datetime')->first();
            // Ride Already Started Case
            $driver_current_trip = UserTrip::where([['driver_status','start_drive'],['id','!=',$trip->id],['driver_id',auth()->user()->id],['schedule_datetime',$trip->schedule_datetime],['status','!=','cancel']])->whereNotNull('schedule_datetime')->first();

            if(!empty($driver_current_accept_trip) || !empty($driver_current_trip)){
                return $this->sendErrorResponse('You already have accepted the ride at the same datetime', 'failed.', 409);
            }
            // try
            // {
            //     $user = User::find($trip->user_id);
            //     $trip->driver_status = $request->status;
            //     $user->notify(new RideStatusUpdate($driver, $trip));
            // }catch(\Exception $e)
            // {
            //     Log::warning("Email not Sent TO user on Ride Status Update In DriverTrip Controller");
            //     Log::warning($e->getMessage());
            //     Log::warning($e->getLine());
            // }
        }

        $trip->driver_status = $request->status;
        if($request->status == 'start_drive')
        {
            $msg = 'The ride has been started';
            $trip->start_at = \Carbon\Carbon::now();
        }
        if($request->status == 'reject')
        {
            $msg = 'The ride has been rejected';
            $trip->status = 'pending';
        }
        if($request->status != 'reject')
        {
            $status = $trip->driver_status == 'complete' ?  'complete' : 'processing';
            $trip->status = $status;
            try
            {
                $user = User::find($trip->user_id);
                $user->notify(new RideStatusUpdate($driver, $trip));
            }catch(\Exception $e)
            {
                Log::warning("Email not Sent TO user on Ride Status Update In DriverTrip Controller");
                Log::warning($e->getMessage());
                Log::warning($e->getLine());
            }
        }
        if($trip->driver_status == 'complete'){
            $msg = 'The ride has been completed';
            $startTime = $trip->start_at;
            $finishTime = \Carbon\Carbon::now();
            $totalDuration = $finishTime->diffInMinutes($startTime);
            $amount = Helper::getGeneralSetting('pay_per_hour');

            $custom_amount = false;

            if ($trip->trip_type == "garage_pickup"){
                $total_amount = Helper::getGeneralSetting('garage_pickup') ?? 0;
            }
            if ($trip->trip_type == "rta_charges"){
                $total_amount = Helper::getGeneralSetting('rta_charges') ?? 0;
            }
            if ($trip->trip_type == "ride_now" && $trip->booking_type == "pay_per_minute"){
                $amount = 1.50;
                $meter_start_amount = Helper::getGeneralSetting('pay_per_minute') ?? 0;
                if($totalDuration > 1){
                    $totalDuration = $totalDuration - 1;
                    $total_amount = $amount * $totalDuration;
                }else if($totalDuration <= 1){
                    $total_amount = 0;
                }

                $total_amount = $total_amount + $meter_start_amount;
                $custom_amount = false;
            }
            if($custom_amount){
                $total_amount = $amount * $totalDuration;
            }
            if($trip->booking_type == "pay_per_hour"){
                $hour_est = $totalDuration > 1 ? $totalDuration / 60 : 1;
                $hour_half_amount = $amount / 2;
                if($hour_est > $trip->hours){
                    $pay_per_amount = $trip->trip_type == "ride_now" ? Helper::getGeneralSetting('pay_per_minute') : Helper::getGeneralSetting($trip->trip_type);
                    $fin_hour = $hour_est - $trip->hours;
                    $total_amount = ($fin_hour * 60) * $pay_per_amount;
                } 
                if($hour_est < $trip->hours){
                    $din_hour = $trip->hours - $hour_est;
                    $total_amount = round($hour_est) * $amount;
                    // dd($amount, $hour_est, round($hour_est),$din_hour);
                    // $min_minute_charges = $din_hour * 60;
                    // if($min_minute_charges >= 15){

                    // }
                    // if($min_minute_charges < 15){

                    // }
                }
                $total_amount = $trip->hours * $amount;
                $totalDuration = $trip->hours;
                $custom_amount = true;
            }
            $trip->total_ride_time = $totalDuration > 0 ? $totalDuration : 1;
            $trip->completed_at = $finishTime;

        }

        if(!empty($trip->schedule_datetime) && $trip->status == "pending"){
            $trip->status = "schedule";
        }
        if($trip->driver_status == 'complete'){
            if($trip->pm_type == 'Card'){
                $user_bank = BankDetail::where([['user_id',auth()->user()->id]])->first();
                $request['payment_id'] = !empty($trip->payment_id) ? $trip->payment_id : $user_bank->id;
                $request['amount'] = $total_amount;
                $successPayment = $this->payForTrip($request,$trip->id);
                if(!empty($successPayment)){
                    $total_amount = 0;
                    $trip->status = 'processing';
                    $trip->driver_status = 'start_drive';
                    $trip->amount = $total_amount;
                    $trip->save();
                    return $this->sendErrorResponse('Payment Failed' , $successPayment, 409);
                }
            }
            else if(Str::contains($trip->pm_type, 'Wallet') || Str::contains($trip->pm_type, 'Cash')){
                $trip->amount = $total_amount;
                $this->payUsingWalletForTrip($trip, $trip->pm_type);
            }
        }
        $trip->amount = $total_amount;
        $trip->driver_id = $driver->id;
        $trip->save();

        return $this->sendResponse($trip->load(['user','car','driver','payment','paymentDetails','topupDetails','tipDetails','rentCarsDetails']), "$msg", 201);
     } catch (Exception $e) {
        return $this->sendErrorResponse($e->getMessage(), $e->getMessage(), 400);

        }
    }

    public function payForTrip(Request $request ,$trip_id)
    {
        $trip = UserTrip::find($trip_id);
        $price = $trip->amount;

        $validator = Validator::make($request->all(), [
            'payment_id' => 'required|exists:bank_details,id',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went wrong..', 400);
        }


        DB::beginTransaction();
        try {
            if(is_null($trip->driver_id)) throw new Exception("Trip has no Driver", 1);
            $payment_inputs = [
                'user_id' => $trip->user_id,
                'trip_id' => $trip_id,
                'payment_id' => $request->payment_id,
                'amount' => $price,
                'payment_intent' => Str::random(30),
                'transaction_id' => Str::random(10),
            ];

            if(!empty($trip->promocode_id))
            {
                $user_promocode = PromocodeUser::where([['user_id',$trip->user_id],['promocode_id',$trip->promocode_id]])->first();
                if(!empty($user_promocode)){
                    $promocode = Promocode::find($trip->promocode_id);
                    if(!$promocode) throw new Exception("Invalid promocode", 1);
                    
                    array_merge($payment_inputs, ['promocode', $promocode->promocode, 'discount_type' => $promocode->discount_type, 'discount_value' => $promocode->discount_value]);
                    if($promocode->discount_type == 'fixed' && $promocode->discount_value < $price){
                        $result = $price - $promocode->discount_value;
                        $user_promocode->status = 'inactive';
                        $user_promocode->save();
                    }
                    if($promocode->discount_type == 'percent'){
                        $subtractAmount = $price * ($promocode->discount_value / 100);
                        $result = $price - $subtractAmount;
                        $user_promocode->status = 'inactive';
                        $user_promocode->save();
                    }
                    $payment_inputs['amount'] = $result;
                }
            }

            $card_info = BankDetail::find($request->payment_id);
            $expiry = explode('/', $card_info->expiry_date);

            $card_request = new Request();
            $card_request->request->add([
                'card_no' => $card_info->account_number,
                'ccExpiryMonth' => $expiry[0],
                'ccExpiryYear' => Carbon::createFromFormat('y', $expiry[1])->format('Y'),
                'cvc' => $card_info->cvc,
                'amount' => $payment_inputs['amount'],
                'description' => 'Trip Payment '.$trip->id,
            ]);
            $result = Helper::Charge($card_request);
            if(isset($result['success']))
            {
                Log::error("Payment Failed:");
                Log::error(json_encode($result));
                return $result['message'];
            }else
            {
                $payment_inputs['payment_intent'] = $result['id'];
                $payment_inputs['transaction_id'] = $result['balance_transaction'];
            }

            $payment = Payment::create($payment_inputs);
            $trip->payment_status = 'paid';
            $trip->status = 'complete';
            $trip->driver_status = 'complete';
            $trip->save();
            DB::commit();

            return [];
        } catch (Exception $e) {
            DB::rollback();
            Log::warning("Payment Failed Method - Pay Card");
            Log::warning($e->getMessage());
            Log::warning($e->getLine());
            throw new Exception($e->getMessage(), 1);
        }


    }

    public function payUsingWalletForTrip($trip, $payment_method){
        // DB::beginTransaction();
        try {
            $trip_id = $trip->id; 
            $price = $trip->amount;
            $user = auth()->user();

            $payment_inputs = [
                'user_id' => $trip->user_id,
                'trip_id' => $trip_id,
                'payment_type' => strtolower($payment_method),
                'amount' => $price,
                'payment_intent' => Str::random(30),
                'transaction_id' => Str::random(10),
            ];
            if(!empty($trip->promocode_id))
            {
                $user_promocode = PromocodeUser::where([['user_id',$trip->user_id],['promocode_id',$trip->promocode_id]])->first();
                $promocode = Promocode::find($trip->promocode_id);
                if(!$promocode) throw new Exception("Invalid promocode", 1);

                array_merge($payment_inputs, ['promocode', $promocode->promocode, 'discount_type' => $promocode->discount_type, 'discount_value' => $promocode->discount_value]);
                if($promocode->discount_type == 'fixed' && $promocode->discount_value < $price){
                    $result = $price - $promocode->discount_value;
                    $user_promocode->status = 'inactive';
                    $user_promocode->save();
                }
                if($promocode->discount_type == 'percent'){
                    $subtractAmount = $price * ($promocode->discount_value / 100);

                    $result = $price - $subtractAmount;

                    $user_promocode->status = 'inactive';
                    $user_promocode->save();
                }
                $payment_inputs['amount'] = $result;
            }

            $payment = Payment::create($payment_inputs);
            $trip->payment_status = 'paid';
            $trip->status = 'complete';
            $trip->driver_status = 'complete';
            $trip->save();
            if(Str::contains($trip->pm_type, 'wallet')){
                $total_balance = (float)$user->total_balance - (float)$payment_inputs['amount'];
                \Auth::user()->update(['total_balance'=>$total_balance]);
            }
            // DB::commit();
            return true;
        } catch (Exception $e) {
            // DB::rollback();
            Log::warning("Payment Failed Method - Pay Wallet");
            Log::warning($e->getMessage());
            Log::warning($e->getLine());
            throw new Exception($e->getMessage(), 1);
        }
    }

    public function DriverRides(Request $request)
    {
        date_default_timezone_set('UTC');
        $trips = UserTrip::with(['pickup_address', 'dropoff_address', 'car' => function($q){
            $q->withTrashed();
        }, 'user' => function($q){
            $q->withTrashed();
        },'paymentDetails','avail_promocode','payment','topupDetails','tipDetails','rentCarsDetails'])->where('driver_id', auth()->user()->id);
        if($request->has('trip_type'))
        {
            $trips = $trips->where('trip_type', $request->trip_type);
        }
        if($request->has('driver_history'))
        {
            $trip_tip_ids = Payment::where([['driver_id', auth()->user()->id],['type','tip']])->get()->pluck('trip_id')->toArray();
            $trips = $trips->whereIn('id', $trip_tip_ids);
        }

        if($request->has('driver_status'))
        {
            $trips = $trips->where('driver_status', $request->driver_status);
        }

        if($request->has('status'))
        {
            $trips = $trips->where('status', $request->status);
        }

        if($request->has('date'))
        {
            $trips = $trips->where('created_at', '>=', $request->date);
        }

        if($request->has('booking_type'))
        {
            $trips = $trips->where('booking_type', $request->booking_type);
        }

        return $this->sendResponse($trips->orderBy('created_at','desc')->get(), 'Trips Retrived', 200);
    }

    public function DriverRidesById($id)
    {
        date_default_timezone_set('UTC');
        $trips = UserTrip::with(['car' => function($q){
            $q->withTrashed();
        }, 'user' => function($q){
            $q->withTrashed();
        }, 'avail_promocode','payment','topupDetails','tipDetails','rentCarsDetails', 'paymentDetails' => function($q){
            $q->withTrashed();
        }])->find($id);

        if(!$trips) return $this->sendErrorResponse('Trip not Found', 'Invalid trip Id', 404);

        return $this->sendResponse($trips, 'Trips Retrived', 200);
    }

    public function driverBroadcast(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        // $redis = LRedis::connection('http://localhost:6379');
        // $redis->publish('driverLocation11', json_encode($request->all()));
        // DriverLocation::dispatch($request->all());
        event(new LocateDriver($request->all()));
        // $io = new \PHPSocketIO\SocketIO(6001);
        // $io->on('connection', function ($socket) use ($io) {
        //     echo "New client connected: {$socket->id}\n";
        // });
        // $io->on('disconnect', function ($socket) use ($io) {
        //     echo "Client disconnected: {$socket->id}\n";
        // });
        // $io->on('DriverLocation', function ($socket, $data) use ($io) {
        //     echo "Client DriverLocation: {$data}\n";
        //     $io->emit('driverLocation11', $data);
        // });

        // Worker::runAll();
        return $this->sendResponse('Data Emiited', 'Succesfully', 200);
    }
}
