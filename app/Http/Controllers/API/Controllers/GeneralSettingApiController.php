<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use App\Models\Faq;
use App\Models\AppBanner;

class GeneralSettingApiController extends Controller
{
    public function getGeneralSetting($key)
    {
        $privacy = GeneralSetting::where('key', $key)->pluck('value', 'key');
        // dd($privacy);
        if(!count($privacy))
        {
            $privacy = [$key => ''];
        }

        return $this->sendResponse($privacy, 'Privacy Policy', 200);
    }

    public function Faqs()
    {
        $user = Auth::user();

        $faqs = Faq::where([['status', 1],['type',$user->type]])->get();

        return $this->sendResponse($faqs, 'FAQs', 200);
    }

    public function Banner()
    {
        $banner = AppBanner::where('status', 1)->first();

        return $this->sendResponse($banner, 'App Banner', 200);
    }

}
