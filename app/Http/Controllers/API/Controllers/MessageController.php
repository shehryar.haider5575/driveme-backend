<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Events\UserMessage;
use App\Notifications\ChatMessage;

class MessageController extends Controller
{
    public function getUserWiseMessageList()
    {
        $chatList = Message::where('sender',Auth::id())->groupBy('receiver')->get();

        return $this->sendResponse($chatList, "Chat list fetch",200);
    }


    public function getUserWiseMessages($receiverId)
    {
        $messages = Message::where([['sender',Auth::id()], ['receiver', $receiverId]])->orWhere([['receiver',Auth::id()], ['sender', $receiverId]])->get();
        return $this->sendResponse($messages, "Messages fetch",200);
    }

    public function message(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'receiver' => 'required|exists:users,id|not_in:'.Auth::id(),
            'type' => 'required|in:text,file',
        ]);
        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 404);
        }
        $request['sender']=Auth::id();
        $message = Message::create($request->only('sender','receiver','type','message'));

        // event(new UserMessage($message));
        $receiver = User::find($request->receiver);
        $receiver->notify(new ChatMessage($receiver, $request->message));

        return $this->sendResponse($message, "Message SuccessFully Send",200);
    }
}
