<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\BankDetail;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use App\Helpers\Helper;
use Carbon\Carbon;
use Validator;
use Log;
use DB;
use Auth;
use Exception;

class PaymentAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payment = Payment::with(['trip','bankDetail'])->where('user_id', auth()->user()->id)->paginate();

        return $this->sendResponse($payment, 'All User Payment', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['type'] = Route::currentRouteName();
        if($request['type'] == 'topup') $request['from'] = 'payment';
        if($request->payment_id == false || $request->payment_id == 'false') unset($request['payment_id']);
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required_if:type,tip|exists:users,id',
            'trip_id' => 'required_if:type,tip|exists:user_trips,id',
            'amount' => 'required|numeric',
            'payment_id' => 'required_if:from,payment|exists:bank_details,id',
            'from' => 'required|in:payment,wallet,cash,tip',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }


        DB::beginTransaction();
        try {
            Log::debug("User Request In Payment Api Controller");
            Log::debug($request->all());

            if($request['type'] != 'topup' && $request['type'] != 'tip') $request['type'] = 'trip';

            $payment_inputs = [
                'user_id' => auth()->user()->id,
                'trip_id' => $request->trip_id??null,
                'payment_id' => $request->payment_id??null,
                'amount' => $request->amount,
                'payment_intent' => Str::random(30),
                'transaction_id' => Str::random(10),
                'type' => $request['type'],
                'payment_type' => $request['from'] ,
                'driver_id' => $request->driver_id??null,
            ];
            if($request['from'] == 'payment')
            {
                $card_info = BankDetail::find($request->payment_id);
                $expiry = explode('/', $card_info->expiry_date);

                $card_request = new Request();
                $card_request->request->add([
                    'card_no' => $card_info->account_number,
                    'ccExpiryMonth' => $expiry[0],
                    'ccExpiryYear' => $expiry[1],
                    'cvc' => $card_info->cvc,
                    'amount' => $request['amount'],
                    'description' => ucfirst($request['type']),
                ]);
                $result = Helper::Charge($card_request);
                if(isset($result['success']))
                {
                    return $this->sendErrorResponse($result['message'], 'Payment Error', 400);
                }else
                {
                    $payment_inputs['payment_intent'] = $result['id'];
                    $payment_inputs['transaction_id'] = $result['balance_transaction'];
                }
            }
            $payment = Payment::create($payment_inputs);
            if($request->has('driver_id') && $request['type'] == 'tip')
            {
                $user = User::find($request->driver_id);
                $user->total_balance = (float)$user->total_balance + (float)$request->amount;
                $user->save();
            }

            if($request['type'] == 'topup')
            {
                $user = Auth::user();
                $user->total_balance = (float)$user->total_balance + (float)$request->amount;
                $user->save();
            }

            if($request['from'] == 'wallet')
            {
                $user = Auth::user();
                $user->total_balance = (float)$user->total_balance - (float)$request->amount;

                if($user->total_balance < 0) throw new Exception("Your Wallet Amount is Insufficient", 1);

                $user->save();
            }

            DB::commit();

            return $this->sendResponse('Payment for '.$request['type'].' is Succefully', 'Payment Successfully', 200);
        } catch (Exception $e) {
            DB::rollback();
            return $this->sendErrorResponse($e->getMessage(), 'Something went wrong...', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
