<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RentalCar;
use App\Models\BookRentalCar;
use App\Models\BankDetail;
use App\Models\Payment;
use App\Models\Promocode;
use App\Models\User;
use App\Notifications\BookRentalCarNotification;
use App\Http\Controllers\API\Controllers\Classes\CheckPromocodeValidation;
use Illuminate\Support\Str;
use App\Helpers\Helper;
use Carbon\Carbon;
use Validator;
use Log;
use DB;
use Auth;
use Exception;

class RentalCarAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
	    $cars = RentalCar::with(['ratings.ratedUser'])->withCount(['bookings'])->withAvg('ratings', 'rating')->where('status', 'active')->orderBy('created_at', 'desc');
        if($request->has('status') && $request->status == 'new')
        {
            $cars = $cars->orderBy('created_at', 'desc');
        }
        if($request->has('status') && $request->status == 'popular')
        {
            $cars = $cars->orderBy('bookings_count', 'desc');
        }

        if($request->has('status') && $request->status == 'top-rated')
        {
            $cars = $cars->orderBy('ratings_avg_rating', 'desc');
        }

        $cars = $cars->get();

        return $this->sendResponse($cars, 'Cars Retrieved Successfully', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'rental_car_id' => 'required|exists:rental_cars,id',
            // 'select_date' => 'required_if:multiple_days,==,0|date',
            'date_from' => 'required',
            'date_to' => 'required|date',
            // 'payment_id' => 'required|exists:bank_details,id',
            // 'time_from' => 'required',
            'location' => 'required',
            'license_front_image' => 'nullable',
            'license_back_image' => 'nullable',
            'emirate_front_image' => 'nullable',
            'emirate_back_image' => 'nullable',
            // 'license' => 'required',
            // 'passport' => 'required',
            // 'multiple_days' => 'required|boolean',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Validation Error', 422);
        }
        DB::beginTransaction();
        try {
            $rental_car = RentalCar::find($request->rental_car_id);
            $booking_user = User::find(auth()->user()->id);
            $booking_data = $request->all();
            $start  = new Carbon($request->date_from);
            $end    = new Carbon($request->date_to);
            $total_days = $start->diffInDays($end);
            // dd($start,$end,$request->date_from,$request->date_to,$total_days);
            $booking_data['total_amount'] = $rental_car->price_per_day * ($total_days + 1);
            $bookCar = BookRentalCar::create(array_merge(['user_id' => auth()->user()->id], $booking_data));
            if($request->has('license_front_image'))
            {
                $license_front = Helper::file_upload($request, 'license_front_image', 'booking');
                $bookCar->attachments()->create(['file_path' => $license_front, 'type' => 'rent_booking_license_front']);

            }

            if($request->has('license_back_image'))
            {
                $license_back = Helper::file_upload($request, 'license_back_image', 'booking');
                $bookCar->attachments()->create(['file_path' => $license_back, 'type' => 'rent_booking_license_back']);
            }
            if($request->has('emirate_front_image'))
            {
                $emirate_front = Helper::file_upload($request, 'emirate_front_image', 'booking');
                $bookCar->attachments()->create(['file_path' => $emirate_front, 'type' => 'rent_booking_emirate_front']);

            }
            if($request->has('emirate_back_image'))
            {
                $emirate_back = Helper::file_upload($request, 'emirate_back_image', 'booking');
                $bookCar->attachments()->create(['file_path' => $emirate_back, 'type' => 'rent_booking_emirate_back']);
            }

            $admin = User::where('type', 'ADMIN')->first();
            try {
                $booking_user->notify(new BookRentalCarNotification($bookCar));
                $admin->notify(new BookRentalCarNotification($bookCar));

            } catch (Exception $e) {
                Log::debug('Email Failed User or Admin in Rental Car API store');
                Log::debug($e->getMessage());
            }
            DB::commit();
            return $this->sendResponse($bookCar, 'Car Booked Request Submitted', 200);
        } catch (Exception $e) {
            DB::rollback();
            return $this->sendErrorResponse($e->getMessage(), 'Sometging went Wrong..', 422);
            //throw $th;
        }
    }

    public function payForBooking(Request $request, $id)
    {
        $bookedCar = BookRentalCar::find($id);
        $price = $bookedCar->rental_car->price_per_day;

        $validator = Validator::make($request->all(), [
            'payment_id' => 'required_if:payment_from,!=,wallet|exists:bank_details,id',
            'payment_from' => 'required',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went wrong..', 400);
        }

        DB::beginTransaction();
        try {

            $payment_inputs = [
                'user_id' => auth()->user()->id,
                'book_rental_car_id' => $id,
                'payment_id' => $request->payment_id??null,
                'amount' => $price,
                'payment_intent' => Str::random(30),
                'transaction_id' => Str::random(10),
            ];

            if($request->has('promocode') && $request->promocode != false && $request->promocode != 'false')
            {
                $promocode = Promocode::where('promocode', $request->promocode)->first();
                if(!$promocode) throw new Exception("Invalid promocode", 1);

                $promo = new CheckPromocodeValidation($request->promocode, $request);
                $result = $promo->ApplyPromocode();

                if(!is_numeric($result)) throw new Exception($result, 1);

                array_merge($payment_inputs, ['promocode', $request->promocode, 'discount_type' => $promocode->discount_type, 'discount_value' => $promocode->discount_value]);
                $payment_inputs['amount'] = $result;
            }

            if($request->payment_from == 'wallet')
            {
                $user = auth()->user();
                if($user->total_balance < $payment_inputs['amount']) throw new Exception("Low Balance in wallet", 1);

                $amount = $user->total_balance - $payment_inputs['amount'];
                $user->total_balance = $amount;
                $user->save();

                $payment_inputs['payment_type'] = 'wallet';
            }
            if($request->payment_from != 'wallet')
            {
                $card_info = BankDetail::find($request->payment_id);
                $card_request = new Request();
                $card_request->request->add([
                    'card_no' => $card_info->account_number,
                    'ccExpiryMonth' => Carbon::parse($card_info->expiry_date)->month,
                    'ccExpiryYear' => Carbon::parse($card_info->expiry_date)->year,
                    'cvc' => $card_info->cvc,
                    'amount' => $payment_inputs['amount'],
                    'description' => 'Rental Car Booking Payment',
                ]);
                $result = Helper::Charge($card_request);
                if(isset($result['success']))
                {
                    return $this->sendErrorResponse($result['message'], 'Payment Error', 400);
                }else
                {
                    $payment_inputs['payment_intent'] = $result['id'];
                    $payment_inputs['transaction_id'] = $result['balance_transaction'];
                }
            }
            $payment_inputs['type'] = 'rental_car';


            $payment = Payment::create($payment_inputs);
            $bookedCar->payment_status = 'paid';
            $bookedCar->status = 'complete';
            $bookedCar->payment_id = $payment->id;
            $bookedCar->save();
            DB::commit();

            return $this->sendResponse($payment, 'Payment Successfully', 200);
        } catch (Exception $e) {
            DB::rollback();
            return $this->sendErrorResponse($e->getMessage(), 'Something went wrong...', 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cars = RentalCar::with(['attachments', 'ratings.ratedUser'])->where('status', 'active')->find($id);

        return $this->sendResponse($cars, 'Cars Retrieved Successfully', 200);
    }

    public function getBookings(Request $request)
    {
        $bookings = BookRentalCar::with(['rental_car' => function($q){
            $q->withTrashed()->with(['ratings.ratedUser']);
        }, 'payment'])->where('user_id', auth()->user()->id);
        $currentDate = Carbon::now();
        if($request->has('status') == 'active' || $request->has('status') == 'scheduled')
        {
            $bookings = $bookings->where('status', $request->status);
        }
        if($request->has('status') == 'past')
        {
            $bookings = $bookings->where('date_to','<', $currentDate);
        }

        return $this->sendResponse($bookings->get(), 'Your Bookings', 200);
    }

    public function getBookingsById($id)
    {
        $bookings = BookRentalCar::with(['rental_car' => function($q){
            $q->withTrashed()->with(['ratings.ratedUser']);
        }, 'payment'])->where('user_id', auth()->user()->id)->find($id);

        if(!$bookings) return $this->sendErrorResponse('Booking not found', 'Invalid Id', 404);

        return $this->sendResponse($bookings, 'Your Booking', 200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bookedCar = BookRentalCar::find($id);
        $price = $bookedCar->rental_car->price_per_day;

        $validator = Validator::make($request->all(), [
            'status' => 'required',
            'date_from' => 'required|date',
            'date_to' => 'required|date',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went wrong..', 400);
        }

        $bookedCar->update($request->all());

        return $this->sendResponse($bookedCar, 'Booking Updated Successfully', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
