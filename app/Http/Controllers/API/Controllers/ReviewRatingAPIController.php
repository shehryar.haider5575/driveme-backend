<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReviewsAndRating;
use Auth;
use Validator;

class ReviewRatingAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ratings = ReviewsAndRating::where('user_id', auth()->user()->id)->paginate();

        return $this->sendResponse($ratings, 'Ratting and Reviews Fetched Successfully', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->has('type')) $request['type'] = 'user';

        $validator = Validator::make($request->all(), [
            'rating' => 'required|numeric|gte:0|lte:5',
            'user_id' => 'required_if:type,==,CAPTAIN|exists:users,id',
            'rental_car_id' => 'required_if:type,==,rental_car|exists:rental_cars,id',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Invalid Rating', 400);
        }
        $already = null;
        if($request->type == 'user')
        {
            $already = ReviewsAndRating::where(['user_id' => $request->user_id, 'from_id' => auth()->user()->id])->first();
        }
        if($request->type == 'rental_car')
        {
            $already = ReviewsAndRating::where(['rental_car_id' => $request->rental_car_id, 'from_id' => auth()->user()->id])->first();
        }
        if($already)
        {
            return $this->sendErrorResponse('You rated this user Already', 'Invalid Rating', 400);
        }
        $request['from_id'] = auth()->user()->id;
        $rating = ReviewsAndRating::create($request->all());

        return $this->sendResponse($rating, 'Rating send successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rating = ReviewsAndRating::where(['user_id' => $id, 'from_id' => auth()->user()->id])->first();

        return $this->sendResponse($rating, 'Rating retrieved successfully', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->has('type')) $request['type'] = 'user';

        $validator = Validator::make($request->all(), [
            'rating' => 'required|numeric|gte:0|lte:5',
            'user_id' => 'required_if:type,==,user|exists:users,id',
            'rental_car_id' => 'required_if:type,==,rental_car|exists:rental_cars,id',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Invalid Rating', 400);
        }
        $already = ReviewsAndRating::find($id);
        if($already)
        {
            $already->rating = $request->rating??$already->rating;
            $already->review = $request->review??$already->review;
        }
        return $this->sendResponse($already, 'Rating Updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $already = ReviewsAndRating::find($id);
        if($already)
        {
            $already->delete();
            return $this->sendResponse('Your Rating Deleted', 'Rating Deleted', 200);
        }
        return $this->sendErrorResponse('Rating not Found', 'Invalid Rating', 409);
    }
}
