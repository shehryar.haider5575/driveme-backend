<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserComplaint;
use App\Models\User;
use App\Notifications\ComplaintUpdate;
use Validator;
use Auth;
use Log;

class UserComplaintAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaint = UserComplaint::with(['user', 'trips'])->where('user_id', auth()->user()->id)->get()->toArray();

        return $this->sendResponse($complaint, 'Your Complaints', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'user_trip_id' => 'required|exists:user_trips,id',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        $request['user_id'] = auth()->user()->id;
        $complaint = UserComplaint::create($request->all());
        try {
            $auth = auth()->user();
            $auth->notify(new ComplaintUpdate($complaint));

            $user = User::where('type', 'ADMIN')->first();
            if($user) $user->notify(new ComplaintUpdate($complaint));
        } catch (\Exception $e) {
            Log::debug('Email Failed In Complaint API Controller');
            Log::debug($e->getMessage());
        }
        return $this->sendResponse($complaint, 'Your Complaint Added Successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $complaint = UserComplaint::with(['user', 'trips'])->find($id);

        return $this->sendResponse($complaint, 'Complaint Retrieved Successfully', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'status' => 'required|in:pending,resolved',
        ])->validate();

        $complaint = UserComplaint::find($id);

        $complaint->update($request->only(['status', 'message']));

        return $this->sendResponse($complaint, 'Compalint '.$request->status, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
