<?php

namespace App\Http\Controllers\API\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserTrip;
use App\Models\User;
use App\Models\Disscount;
use App\Models\BankDetail;
use App\Models\Payment;
use App\Models\Promocode;
use App\Models\PromocodeUser;
use App\Notifications\Disscounts;
use App\Notifications\NewTripNotifications;
use App\Http\Controllers\API\Controllers\Classes\CheckPromocodeValidation;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
use Log;
use DB;
use Auth;
use Exception;
use App\Helpers\Helper;

class UserTripAPIController extends Controller
{
    public function __constructor()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        date_default_timezone_set('UTC');
        $trips = $request->user()->trips();

        if($request->has('trip_type')) $trips->where('trip_type', $request->trip_type);

        if($request->has('status')) $trips->where('status', $request->status);

        if($request->has('start_date')) $trips->where('created_at', '>=', $request->start_date);

        if($request->has('end_date')) $trips->where('created_at', '<=', $request->end_date);

        $trips->orderBy('id', 'desc');

        $trip_ids = $trips->pluck('id')->toArray();
        $trips = UserTrip::with(['user','ratings','avail_promocode','car','driver','payment','paymentDetails','topupDetails','tipDetails','rentCarsDetails'])->whereIn('id',$trip_ids)->orderBy('id', 'desc')->paginate();

        // dd($request->trip_type);
        return $this->sendResponse($trips, 'All Your Trips', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pickup_id' => 'required_if:pickup_address,=,false|exists:user_addresses,id',
            'dropoff_id' => 'required_if:dropoff_address,=,false|exists:user_addresses,id',
            'pickup_address' => 'required_if:pickup_id,=,false',
            'dropoff_address' => 'required_if:dropoff_id,=,false',
            'pickup_latitude' => 'required_if:pickup_address,!=,false',
            'pickup_longitude' => 'required_if:pickup_address,!=,false',
            'dropoff_latitude' => 'required_if:dropoff_address,!=,false',
            'dropoff_longitude' => 'required_if:dropoff_address,!=,false',
            'status' => 'required|in:pending,schedule,processing,cancel',
            'schedule_datetime' => 'required_if:status,=,schedule|date_format:Y-m-d H:i:s',
            'trip_type' => 'required|in:ride_now,garage_pickup,rta_inspection,inter_city',
            'booking_type' => 'required|in:pay_per_minute,pay_per_hour',
            'car_id' => 'required|exists:cars,id',
            'amount' => 'nullable',
            'promocode_id' => 'nullable',
            'hours' => 'nullable',
            'payment_id' => 'nullable'
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        DB::beginTransaction();
        try
        {
            $Promocode = Promocode::find($request->promocode_id);
            $user_promocode = PromocodeUser::where([['user_id',auth()->user()->id],['promocode_id',$request->promocode_id]])->first();
            $ppm = Helper::getGeneralSetting('pay_per_minute') ?? 0;
            $pph = Helper::getGeneralSetting('pay_per_hour') ?? 0;
            
            if(!empty($user_promocode) && $user_promocode->status == 'inactive'){
                return $this->sendErrorResponse('Failed, promocode is not valid.', 'Failed', 409 );
            }
            if(!empty($user_promocode)){
                $user_promocode->status = 'inactive';
                $user_promocode->save();
            }
            // check walllet amount
            if($request->pm_type == "Wallet"){
                if($request->user()->total_balance < $ppm){
                    return $this->sendErrorResponse('Failed, payment through wallet should not be allowed due to insufficient balance.', 'Failed', 409 );
                }
            }
            // Ride Condition
            if(empty($request->schedule_datetime)){
                $current = carbon::now()->addMinute(20)->format('Y-m-d H:i:s');
                $user_trip = UserTrip::where([['status','pending'],['driver_status','pending'],['user_id',auth()->user()->id], ['car_id',$request->car_id]])->whereNull('schedule_datetime')->first();
                if(!empty($user_trip)){
                    return $this->sendErrorResponse('Failed, You have already one pending request.', 'Failed', 409);
                }
            }

            // $user_schedule_trip = UserTrip::whereNotNull('schedule_datetime')->whereIn('driver_status',['accept','confirm_arrival','start_drive'])->where([['user_id',auth()->user()->id],['schedule_datetime','<',$current]])->first();
            // if(!empty($user_schedule_trip)){
            //     return $this->sendErrorResponse('Failed, You have already one pending schedule booking.', 'failed', 409);
            // }
            // Ride Condition

            if(!$request->pickup_id)
            {
                $address = ['user_id' => $request->user()->id, 'address'=> $request->pickup_address, 'latitude' => $request->pickup_latitude, 'longitude' => $request->pickup_longitude, 'city' => $request->city??null, 'type' => 'other'];
                $address = $request->user()->user_address()->create($address);
                $request['pickup_id'] = $address->id;
            }

            if(!$request->dropoff_id)
            {
                $address = ['user_id' => $request->user()->id, 'address'=> $request->dropoff_address, 'latitude' => $request->dropoff_latitude, 'longitude' => $request->dropoff_longitude, 'city' => $request->city??null, 'type' => 'other'];
                $address = $request->user()->user_address()->create($address);
                $request['dropoff_id'] = $address->id;
            }
            if($request->trip_type == "garage_pickup"){
                $hr = Helper::getGeneralSetting('garage_pickup') ?? 0;
                $amount = $hr;
                // $amount = $request->hours * $hr;
            }
            if ($request->trip_type == "rta_inspection"){
                $hr = Helper::getGeneralSetting('rta_charges') ?? 0;
                $amount = $hr;
                // $amount = $request->hours * $hr;
            }
            if ($request->trip_type == "ride_now" && $request->booking_type == "pay_per_hour"){
                $amount = $request->hours * $pph;
            }
            if ($request->trip_type == "ride_now" && $request->booking_type == "pay_per_minute"){
                $amount = $ppm;
            }

            $trip = new UserTrip;
            $trip->pickup_id = $request->pickup_id;
            $trip->dropoff_id = $request->dropoff_id;
            $trip->car_id = $request->car_id;
            $trip->amount = $amount ?? 0;
            $trip->user_id = $request->user()->id;
            $trip->trip_type = $request->trip_type;
            $trip->promocode_id = $request->promocode_id;
            $trip->payment_id = $request->payment_id ?? null;
            $trip->booking_type = $request->booking_type;
            $trip->status = $request->status == "schedule" ? "pending" : $request->status;
            $trip->instruction = $request->instruction??null;
            $trip->schedule_datetime = $request->schedule_datetime??null;
            $trip->pm_type = $request->pm_type??null;
            $trip->hours = $request->booking_type == "pay_per_hour" ? $request->hours : null;
            $trip->save();

            $user = $request->user();
            $refferal = $user->Myrefferals()->first();
            /*if(!is_null($refferal))
            {
                $disscount = new Disscount;
                $disscount->user_id = $refferal->refferal_user_id;
                $disscount->refferal_id = $refferal->id;
                $disscount->disscount = $this->setting['reward_on_reffer']??0;
                $disscount->disscount_type = 'fixed';
                $disscount->status = 'active';
                $disscount->save();

                $user_friend = User::find($refferal->refferal_user_id);
                $user_friend->notify(new Disscounts($disscount));
	    }*/
        
            $drivers = User::where('type','CAPTAIN')->whereNotNull(['device_token'])->get();
            foreach ($drivers as $key => $driver) {
                try{
                    $driver->notify(new NewTripNotifications($driver, $trip));
                }
                catch(\Exception $e)
                {
                    Log::warning("All Drivers User Trip Notification Issue");
                    Log::warning($e->getMessage());
                    Log::warning($e->getLine());
                }
            }

            DB::commit();

            return $this->sendResponse($trip->load(['user','avail_promocode','car','driver','payment','paymentDetails','topupDetails','tipDetails','rentCarsDetails']), 'Trip Booked Successfully', 200);

        }catch(\Exception $e)
        {
            DB::rollback();
            Log::error("Error in User Trip Controller Store Function");
            Log::error('Input From Mobile App Dev');
            Log::error($request->all());
            Log::error('Genrated Error');
            Log::error($e->getMessage());
            return $this->sendErrorResponse($e->getMessage(), 'Something Went Wrong.', 500);
        }

    }

    public function payForTrip(Request $request ,$trip_id)
    {
        $trip = UserTrip::find($trip_id);
        $price = $trip->amount;

        $validator = Validator::make($request->all(), [
            'payment_id' => 'required|exists:bank_details,id',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went wrong..', 400);
        }


        DB::beginTransaction();
        try {
            if(is_null($trip->driver_id)) throw new Exception("Trip has no Driver", 1);
            $payment_inputs = [
                'user_id' => auth()->user()->id,
                'trip_id' => $trip_id,
                'payment_id' => $request->payment_id,
                'amount' => $price,
                'payment_intent' => Str::random(30),
                'transaction_id' => Str::random(10),
            ];

            if($request->has('promocode') && $request->promocode != false && $request->promocode != 'false')
            {
                $promocode = Promocode::where('promocode', $request->promocode)->first();
                if(!$promocode) throw new Exception("Invalid promocode", 1);

                $promo = new CheckPromocodeValidation($request->promocode, $request);
                $result = $promo->ApplyPromocode();

                if(!is_numeric($result)) throw new Exception($result, 1);

                array_merge($payment_inputs, ['promocode', $request->promocode, 'discount_type' => $promocode->discount_type, 'discount_value' => $promocode->discount_value]);
                $payment_inputs['amount'] = $result;
            }

            $card_info = BankDetail::find($request->payment_id);
            $card_request = new Request();
            $card_request->request->add([
                'card_no' => $card_info->account_number,
                'ccExpiryMonth' => Carbon::parse($card_info->expiry_date)->month,
                'ccExpiryYear' => Carbon::parse($card_info->expiry_date)->year,
                'cvc' => $card_info->cvc,
                'amount' => $payment_inputs['amount'],
                'description' => 'Trip Payment '.$trip->id,
            ]);
            $result = Helper::Charge($card_request);
            if(isset($result['success']))
            {
                return $this->sendErrorResponse($result['message'], 'Payment Error', 400);
            }else
            {
                $payment_inputs['payment_intent'] = $result['id'];
                $payment_inputs['transaction_id'] = $result['balance_transaction'];
            }

            $payment = Payment::create($payment_inputs);
            $trip->payment_status = 'paid';
            $trip->status = 'complete';
            $trip->driver_status = 'complete';
            $trip->save();
            DB::commit();

            return $this->sendResponse('Payment for Trip is Succefully', 'Payment Successfully', 200);
        } catch (Exception $e) {
            DB::rollback();
            return $this->sendErrorResponse($e->getMessage(), 'Something went wrong...', 400);
        }


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = UserTrip::with(['user','ratings','car','driver','payment','paymentDetails','topupDetails','tipDetails','rentCarsDetails'])->find($id);

        return $this->sendResponse($trip, 'Trip Reterived', 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:pending,schedule,processing,complete,cancel',
            'schedule_datetime' => 'required_if:status,=,schedule|datetime',
            'reason' => 'required_if:status,=,cancel',
        ]);

        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }

        DB::beginTransaction();
        try
        {

            $trip = UserTrip::find($id);
            $trip->status = $request->status??$trip->status;
            $trip->schedule_datetime = $request->schedule_datetime??$trip->schedule_datetime;
            $trip->instruction = $request->instruction??$trip->instruction;
            $trip->car_id = $request->car_id??$trip->car_id;
            $trip->reason = $request->reason??$trip->reason;
            $trip->save();

            DB::commit();

            return $this->sendResponse($trip->load(['user','avail_promocode','car','driver','payment','paymentDetails','topupDetails','tipDetails','rentCarsDetails']), 'Trip Updated Successfully', 200);

        }catch(\Exception $e)
        {
            DB::rollback();
            Log::error("Error in User Trip Controller Update Function");
            Log::error('Input From Mobile App Dev');
            Log::error($request->all());
            Log::error('Genrated Error');
            Log::error($e->getMessage());
            return $this->sendErrorResponse($e->getMessage(), 'Something Went Wrong.', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getEstimationPrice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'distance' => 'required|gte:0',
            'time' => 'required',
        ]);
        if($validator->fails())
        {
            return $this->sendErrorResponse($validator->errors()->first(), 'Something went Wrong..', 409);
        }
        try {
            $drivers = User::where(['type' => 'CAPTAIN', 'status' => 1,'id'=>Auth::user()->id])->first();
            $ppm = Helper::getGeneralSetting('pay_per_minute') ;
            $pph = Helper::getGeneralSetting('pay_per_hour') ;
            $minutes = $request->time/3600;
            $hours = $request->time/60;
            $per_minute = $ppm;
            $per_hour = $pph;

            $garage_pickup  = Helper::getGeneralSetting('garage_pickup');
            $rta_pickup     = Helper::getGeneralSetting('rta_charges');

            $response = [
                'pay_per_minutes'   => (double) bcdiv($per_minute,1, 2),
                'pay_per_hours'     => (double) bcdiv($per_hour,1, 2),
                'garage_pickup'     => (double) bcdiv($garage_pickup,1, 2),
                'rta_pickup'        => (double) bcdiv($rta_pickup,1, 2)
            ];
            return $this->sendResponse($response, 'Estimation Calculation', 200);
            //code...
        } catch (\Exception $e) {
            return $this->sendErrorResponse($e->getMessage(), 'Unable to Calculate', 400);
        }

    }
}
