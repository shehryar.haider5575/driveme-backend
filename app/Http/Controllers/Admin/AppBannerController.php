<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\AppBanner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AppBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appBanners = AppBanner::latest()->paginate(5);
        return view('pages.appbanner.index',compact('appBanners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.appbanner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'file' => 'required'
        ]);
        $request['file_path'] = Helper::file_upload($request, "file", 'appbanners');
        $id = AppBanner::create($request->all());
        DB::table('app_banners')->where('id','!=',$id->id)->update(['status' => 0]);
        Session::flash('insert',"Faqs Add Successfully");
        return redirect()->route('appbanner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $appbanner = AppBanner::find($id);
        return view('pages.appbanner.edit',compact('appbanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $banner = AppBanner::find($id);
        $request->validate([
            'content' => 'required',
        ]);
        if ($request->has('file'))
        {
            if($banner->file_path)
            {
                Helper::file_delete($banner->file_path);
            }
            $request['file_path'] = Helper::file_upload($request, "file", 'appbanners');
        }
        if ($request->status)
        {
            AppBanner::where('id','!=',$id)->update(['status' => 0]);;
        }
        $banner->update($request->except('_method', '_token', 'file'));
        Session::flash('insert',"App Banner update Successfully");
        return redirect()->route('appbanner.index');
    }

    public function updateStatus(Request $request, $id)
    {
        $banner = AppBanner::find($id);

        if ($request->status)
        {
            AppBanner::where('id','!=',$id)->update(['status' => 0]);;
        }

        $banner->update($request->only('status'));

        Session::flash('insert',"App Banner update Successfully");
        return redirect()->route('appbanner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
