<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BookRentalCar;
use App\DataTables\BookRentalCarDataTable;

class BookedRentalCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BookRentalCarDataTable $bookeddatatable)
    {
        return $bookeddatatable->render('pages.book_rental_cars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = BookRentalCar::with(['rental_car' => function($q){
            $q->withTrashed()->with(['ratings']);
        }, 'user' => function($q){
            $q->withTrashed();
        }])->find($id);
        if(!$booking) abort(404, 'Invalid Booking Id');

        return view('pages.book_rental_cars.book_detail', compact('booking'));
    }

    public function updateStatus($status, $id)
    {
        $booking = BookRentalCar::find($id);
        if(!$booking) abort(404, 'Invalid Booking Id');

        $booking->request_status = $status;
        $booking->save();

        return redirect()->back()->with('insert', 'Booking Status Updated');
    }

    public function updatebookingStatus($status, $id)
    {
        $booking = BookRentalCar::find($id);
        if(!$booking) abort(404, 'Invalid Booking Id');

        $booking->status = $status;
        $booking->save();

        return redirect()->back()->with('insert', 'Booking Status Updated');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking = BookRentalCar::find($id);
        if(!$booking) abort(404, 'Invalid Booking Id');

        $booking->request_status = $request->request_status??$booking->request_status;
        if($request->request_status == 'reject') $booking->status = 'reject';
        $booking->status = $request->status??$booking->status;
        $booking->save();

        return redirect()->route('booking-cars.index')->with('insert', 'Booking Status Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
