<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarBrand;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Validator;

class CarBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = CarBrand::paginate();

        return view('pages.cars.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.cars.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        Validator::make($request->all(), [
            'name' => 'required|unique:car_brands,name',
            'logo_file' => 'required|image|mimes:jpg,png,jpeg|max:2048',
        ])->validate();

        $request['logo'] = Helper::file_upload($request, "logo_file", 'brands');

        $brand = CarBrand::create($request->except('logo_file'));

        return redirect()->route('brands.index')->with(['insert' => 'Brand Added Successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function show(CarBrand $carBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = CarBrand::find($id);
        if(!$brand) abort(409);

        return view('pages.cars.brand.create', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $carBrand = CarBrand::find($id);

        Validator::make($request->all(), [
            'name' => 'required|unique:car_brands,name,'.$carBrand->id,
            'logo_file' => 'nullable|image|mimes:jpg,png,jpeg|max:2048',
        ])->validate();

        if($request->has('logo_file'))
        {
            if(!empty($carBrand->logo))
            {
                Helper::file_delete($banner->file_path);
            }

            $request['logo'] = Helper::file_upload($request, "logo_file", 'brands');
        }

        $carBrand->update($request->except('logo_file'));

        return redirect()->route('brands.index')->with(['insert' => 'Brand Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CarBrand  $carBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carBrand = CarBrand::find($id);
        $carBrand->delete();

        return redirect()->route('brands.index')->with(['insert' => 'Brand Deleted']);
    }
}
