<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Http\Request;
use App\Helpers\Helper;
use Validator;

class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = CarModel::paginate();

        return view('pages.cars.model.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = CarBrand::where('status', 1)->get();

        return view('pages.cars.model.create', compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|unique:car_models,name',
            'logo_file' => 'required|image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=100,min_height=100,max_width=500,max_height=500',
        ])->validate();

        $request['logo'] = Helper::file_upload($request, "logo_file", 'models');

        $brand = CarModel::create($request->except('logo_file'));

        return redirect()->route('models.index')->with(['insert' => 'Model Added Successfully']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function show(CarModel $carModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = CarModel::find($id);
        if(!$model) abort(409);

        $brands = CarBrand::where('status', 1)->get();
        return view('pages.cars.model.create', compact('model', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarModel $carModel)
    {
        Validator::make($request->all(), [
            'name' => 'required|unique:car_models,name,'.$carModel->id,
            'brand_id' => 'required|exists:car_brands,id',
            'logo_file' => 'nullable|image|mimes:jpg,png,jpeg|max:2048|dimensions:min_width=100,min_height=100,max_width=500,max_height=500',
        ])->validate();

        if($request->has('logo_file'))
        {
            if(!empty($carModel->logo))
            {
                Helper::file_delete($banner->file_path);
            }

            $request['logo'] = Helper::file_upload($request, "logo_file", 'models');
        }

        $carModel->update($request->except('logo_file'));

        return redirect()->route('models.index')->with(['insert' => 'Model Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CarModel  $carModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carModel = CarModel::find($id);
        $carModel->delete();

        return redirect()->route('models.index')->with(['insert' => 'Model Deleted']);
    }
}
