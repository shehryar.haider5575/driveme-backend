<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserComplaint;
use App\DataTables\ComplaintsDataTable;
use App\Notifications\ComplaintUpdate;
use Exception;
use Log;
use Validator;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ComplaintsDataTable $complaintDatatable)
    {
        return $complaintDatatable->render('pages.complaints.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $complaint = UserComplaint::with(['user' => function($q){
            $q->withTrashed();
        }, 'trips' => function($q){
            $q->with(['driver']);
        }])->find($id);

        return view('pages.complaints.complaint_detail', compact('complaint'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|in:pending,resolved',
        ]);
        if($validator->fails())
        {
            return back()->with('error', $validator->errors()->first());
        }
        $complaint = UserComplaint::find($id);
        $complaint->status = $request->status??$complaint->status;
        $complaint->save();

        try {
            $complaint->user->notify(new ComplaintUpdate($complaint));
        } catch (Exception $th) {
            Log::debug("Complaint Mail Failed");
            Log::debug($th->getMessage());
        }

        return redirect()->route('user-complaints.index')->with('insert', 'Complaint updated to '.$request->status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
