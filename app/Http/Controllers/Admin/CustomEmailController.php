<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CustomNotification;
use App\Notifications\CustomEmail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Log;
use App\Jobs\CustomNotification as CustomNotiEmail;
use Carbon\Carbon;
use Validator;

class CustomEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_archive', null)->get();

        return view('pages.custom_email.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'subject' => 'required',
            'message' => 'required',
            'users.*' => 'required',
        ])->validate();

        $users = User::where('is_archive', null)->get();

        if(!in_array('all', $request->users))
        {
            $users = User::whereIn('id', $request->users)->get();
        }
	
        try
        {
            $mail = new CustomNotiEmail($request->subject, $request->message, $users);
            $mail->delay(Carbon::now()->addSeconds(2));
            dispatch($mail);

        }catch(\Exception $e)
        {
            Log::debug("Email issue in Custom EMail Controller Store");
            Log::debug($e->getMessage());
        }

        Session::flash('success', 'Promocode Added Successfully');
        return redirect()->route('custom-notification.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
