<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\DataTables\DriversDataTable;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Str;
use App\Notifications\CustomEmail;
use DB;
use Exception;
use Log;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DriversDataTable $dataTable)
    {
        return $dataTable->render('pages.driver.index');
        // $drivers = User::where('type',"CAPTAIN")->paginate(5);
        // return view('pages.driver.index',compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.driver.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'license_number' => 'required|unique:users,license_number',
            'emirates_id' => 'required|unique:users,emirates_id',
            'password' => 'required|min:8',
        ]);
        DB::beginTransaction();
        try
        {
        $request['type'] = "CAPTAIN";
        $request['password'] = Hash::make($request->password);
        $randomToken = (User::latest()->first())? User::latest()->first()->id + 1 : 1;
        $request['refferal_code'] = date('y').date('m').date('d').$randomToken;
        $user = User::create($request->all());

        if($request->has('license_front'))
        {
            $request->validate([
                'license_front' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $driver_license_front = Helper::file_upload($request, 'license_front', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_front, 'type' => 'driver_license_front']);
        }

        if($request->has('license_back'))
        {
            $request->validate([
                'license_back' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $driver_license_back = Helper::file_upload($request, 'license_back', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_back, 'type' => 'driver_license_back']);
        }

        if($request->has('pro_pic'))
        {
            $request->validate([
                'pro_pic' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $profile_image = Helper::file_upload($request, 'pro_pic', 'users');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
        }

        if($request->has('emirate_back'))
        {
            $request->validate([
                'emirate_back' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $profile_image = Helper::file_upload($request, 'emirate_back', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'emirate_back']);
        }

        if($request->has('emirate_front'))
        {
            $request->validate([
                'emirate_front' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $profile_image = Helper::file_upload($request, 'emirate_front', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'emirate_front']);
        }

        if($request->has('passport_pic'))
        {
            $request->validate([
                'passport_pic' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            $profile_image = Helper::file_upload($request, 'passport_pic', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'passport_pic']);
        }

        if($request->has('refferal_code'))
        {
            $user_refferal = User::where('refferal_code', $request->refferal_code)->first();
            $user->refferals()->create(['refferal_code' => $request->refferal_code, 'refferal_user_id' => $user_refferal->id]);
        }

        $user->refferal_code = $user->id.date('Y');
        $user->is_admin_approve = 1;
        $user->save();
        DB::commit();
        return redirect()->route('driver.index')->with('insert', "Driver Add Successfully");

        }catch(Exception $e)
        {
            DB::rollback();
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('pages.user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('pages.driver.create',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request['password']) {
            $request['password'] = Hash::make($request->password);
        }else {
            unset($request['password']);
        }
        // dd($request->all());
        $user = User::find($id);
        $user->update($request->all());
        if($request->has('license_front'))
        {
            $request->validate([
                'license_front' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->license_front_image))
            {
                Helper::file_delete($user->license_front_image);
            }
            $driver_license_front = Helper::file_upload($request, 'license_front', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_front, 'type' => 'driver_license_front']);
        }

        if($request->has('license_back'))
        {
            $request->validate([
                'license_back' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->license_back_image))
            {
                Helper::file_delete($user->license_back_image);
            }
            $driver_license_back = Helper::file_upload($request, 'license_back', 'captains');
            $user->attachments()->create(['file_path' => $driver_license_back, 'type' => 'driver_license_back']);
        }

        if($request->has('pro_pic'))
        {
            $request->validate([
                'pro_pic' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->profile_image))
            {
                Helper::file_delete($user->profile_image);
            }
            $profile_image = Helper::file_upload($request, 'pro_pic', 'users');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
        }

        if($request->has('emirate_back'))
        {
            $request->validate([
                'emirate_back' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->emirate_back))
            {
                Helper::file_delete($user->emirate_back);
            }
            $profile_image = Helper::file_upload($request, 'emirate_back', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'emirate_back']);
        }

        if($request->has('emirate_front'))
        {
            $request->validate([
                'emirate_front' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->emirate_front))
            {
                Helper::file_delete($user->emirate_front);
            }
            $profile_image = Helper::file_upload($request, 'emirate_front', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'emirate_front']);
        }

        if($request->has('passport_pic'))
        {
            $request->validate([
                'passport_pic' => 'required|image|mimes:jpg,png,jpeg|max:1024'
            ]);
            if(!is_null($user->passportsize_pic))
            {
                Helper::file_delete($user->passportsize_pic);
            }
            $profile_image = Helper::file_upload($request, 'passport_pic', 'captains');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'passport_pic']);
        }

        return redirect()->route('driver.index')->with('insert', "Driver Update Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Driverrequests()
    {
        $drivers = User::where(['type' => 'CAPTAIN', 'is_admin_approve' => '0'])->orderBy('id', 'desc')->paginate();

        return view('pages.driver.requested', compact('drivers'));
    }

    public function DriverrequestsUser($id)
    {
        $user = User::find($id);
        $driver = true;
        return view('pages.user.view', compact('user', 'driver'));
    }

    public function DriverAccept($id)
    {
        $password = Str::random(6).time();
        $user = User::find($id);
        if(!$user) abort(404, 'User Not Found');
        $user->is_admin_approve = '1';
        $user->is_archive = NULL;
        $user->password = Hash::make($password);
        $user->save();

        try
        {
            $user->notify(new CustomEmail('Your Account Request is Accepted', "Request Accepted".env('APP_NAME'), $password));

        }catch(Exception $e)
        {
            Log::debug("Email failed to Driver On accept Request Driver Controller");
            Log::debug($e->getMessage());
        }

        // Session::flash('insert', "Driver Accepted Successfully");
        return redirect()->route('driver.index')->with('insert', "Driver Accepted Successfully");
    }

    public function DriverReject($id)
    {
        $user = User::find($id);
        $user->is_admin_approve = 3;
        $user->save();
        //$user->delete();

        return redirect()->route('driver.index')->with('insert', "Driver Request Rejected Successfully");
    }
}
