<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\GeneralSetting;
use App\Models\Notification;
use App\Models\Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DataTables\NotificationDataTable;
use App\Helpers\Helper;
use Carbon\Carbon;
use Auth;
use Validator;
use Hash;

class GeneralSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(NotificationDataTable $datatable)
    {
        return $datatable->render('pages.notifications.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $generalSetting = GeneralSetting::all()->pluck('value', 'key');

        return view('pages.generalsetting.create',compact('generalSetting'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $general = GeneralSetting::all()->pluck('value', 'key')->toArray();

        foreach($request->key as $key => $value)
        {
            if(!array_key_exists($key ,$general))
            {
                GeneralSetting::create(['key' => $key, 'value' => $value]);
            }else
            {
                $generalUpdate = GeneralSetting::where('key', $key)->first();
                $generalUpdate->value = $value;
                $generalUpdate->save();
            }
        }
        // dd($request->all());
        Session::flash('insert', 'General Setting Update sucessfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notification = Notification::with(['user' => function($q){
            $q->withTrashed();
        }])->find($id);
        $notification->read_at = Carbon::now();

        return view('pages.notifications.show', compact('notification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = Notification::find($id);
        $notification->read_at = Carbon::now();
        $notification->save();

        return redirect()->back()->with('insert', 'Notification Marked as Read');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateProfile(Request $request)
    {
        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
        ])->validate();

        $user = Auth::user();

        if($request->phone != $user->phone)
        {
            Validator::make($request->all(), [
                'phone' => 'required|unique:users,phone',
            ])->validate();
        }

        if($request->email != $user->email)
        {
            Validator::make($request->all(), [
                'email' => 'required|unique:users,email',
            ])->validate();
        }

        if ($request->hasFile('imageFile')) {
            // dd($request->all());
            if(!empty($user->profile_image)){

                Helper::file_delete($user->profile_image);

                $user->profile_image()->delete();
            }

            $profile_image = Helper::file_upload($request, 'imageFile', 'users');
            $user->attachments()->create(['file_path' => $profile_image, 'type' => 'profile_image']);
        }

        $user->update($request->all());
        $request->session()->flash('success', 'Profile Updated Successfully');
        return back();
    }

    public function updatePassword(Request $request)
    {
        Validator::make($request->all(), [
            'current_password' => 'required',
            'password' => 'required|confirmed',
        ])->validate();

        $user = Auth::user();
        if(!Hash::check($request->current_password, $user->password))
        {
            $request->session()->flash('anyError', 'Current Password is Wrong');
            return back()->withErrors('Current Password is Wrong');
        }
        $user->password = Hash::make($request->password);
        $user->save();

        $request->session()->flash('success', 'Password Updated Successfully');
        return back();
    }

    public function delete_attachment($id)
    {
        $attachment = Attachment::find($id);
        if(!$attachment) abort(404, 'Attachment not Found');

        try {
            Helper::file_delete($attachment->file_path);

            $attachment->delete();

            return back()->with('insert', 'Attachment Deleted');
        } catch (\Exception $e) {
            return back()->with('error', 'Attachement Delete Failed');
        }
    }
}
