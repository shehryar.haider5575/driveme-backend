<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promocode;
use App\DataTables\PromocodesDataTable;
use App\Models\PromocodeUser;
use App\Rules\DiscountValidation;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Promocode as PromocodeNotifications;
use Illuminate\Support\Facades\Log;
use App\Models\User;
use App\Jobs\SendPromoEmailQueue;
use Carbon\Carbon;
use Validator;

class PromocodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PromocodesDatatable $promocodeDatatable)
    {
        return $promocodeDatatable->render('pages.promocodes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('type', 'USER')->get();

        return view('pages.promocodes.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'promocode' => 'required|string|unique:promocodes,promocode',
            'discount_type' => 'required|in:fixed,percent',
            'discount_value' => ['required', new DiscountValidation($request->discount_type)],
            'started_at' => 'required|date|after_or_equal:today',
            'expired_at' => 'required|date|after:started_at',
        ])->validate();
            // dd($request->all());
        $promos = Promocode::create($request->all());
        $users = [];
        if(is_array($request->users))
        {
            if(in_array('all', $request->users))
            {
                PromocodeUser::create(['user_id' => NULL, 'promocode_id' => $promos->id]);
                $users = User::where('type', 'USER')->where('is_archive', 0)->get();
            }else
            {
                $users = User::whereIn('id', $request->users)->get();
                foreach($request->users as $user)
                {
                    PromocodeUser::create(['user_id' => $user, 'promocode_id' => $promos->id]);
                }
            }
        }else
        {
            $users = User::where('type', 'USER')->where('is_archive', 0)->get();
            PromocodeUser::create(['user_id' => NULL, 'promocode_id' => $promos->id]);
        }
        try
        {
            $mail = new SendPromoEmailQueue($promos, $users);
            $mail->delay(Carbon::now()->addSeconds(2));
            dispatch($mail);

        }catch(\Exception $e)
        {
            Log::debug("Email issue in Promocode Controller Store");
            Log::debug($e->getMessage());
        }
        Session::flash('success', 'Promocode Added Successfully');
        return redirect()->route('promocodes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promo = Promocode::find($id);

        return view('pages.promocodes.edit', compact('promo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'discount_value' => ['required', new DiscountValidation($request->discount_type)],
            'started_at' => 'required|date',
            'expired_at' => 'required|date|after:started_at',
        ])->validate();

        $promos = Promocode::find($id)->update($request->only('title', 'description', 'discount_value', 'started_at', 'expired_at', 'status'));
        if($request->has('users'))
        {
            $promos->users()->delete();
            if(is_array($request->users))
            {
                if(in_array('all', $request->users))
                {
                    PromocodeUser::create(['user_id' => NULL, 'promocode_id' => $promos->id]);
                }else
                {
                    foreach($request->users as $user)
                    {
                        PromocodeUser::create(['user_id' => $user, 'promocode_id' => $promos->id]);
                    }
                }
            }else
            {
                $users = User::where('type', 'USER')->where('is_archive', 0)->get();
                PromocodeUser::create(['user_id' => NULL, 'promocode_id' => $promos->id]);
            }
        }
        Session::flash('insert', 'Promo Updated Successfully');

        return redirect()->route('promocodes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
