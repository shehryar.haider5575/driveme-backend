<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RentalCar;
use App\DataTables\CarRentalDataTable;
use App\Helpers\Helper;

class RentalCarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CarRentalDataTable $datatable)
    {
        return $datatable->render('pages.rental_cars.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.rental_cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'model' => 'required',
            'year' => 'required|date_format:Y',
            'transmission_type' => 'required',
            'color_code' => 'required',
            'price_per_day' => 'required|numeric',
            'passengers' => 'required|numeric',
            'doors' => 'required|numeric',
            'horse_power' => 'required|numeric',
            'speed' => 'required|numeric',
            'fuel_kit' => 'required',
            'description' => 'nullable',
        ]);

        try {
            $car = RentalCar::create($request->except('files'));
            if($request->has('files'))
            {
                $request->validate([
                    'files.*' => 'image'
                ]);
                $files = $request->file('files');
                foreach($files as $file)
                {
                    $request['image'] = $file;
                // dd($request->all());
                    $file_path = Helper::file_upload_with_array($request->all(), 'image', 'Rental_Car');
                    $attachment['file_path'] = $file_path;
                    $attachment['type'] = 'rental_cars';
                    $car->attachments()->create($attachment);
                }
            }
        } catch (Exception $e) {
            return back()->with('error', 'Something went Wrong...! '.$e->getMessage());
            // dd($e->getMessage());
        }

        return back()->with('insert', 'Car Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $car = RentalCar::with(['attachments'])->find($id);

        if(!$car) abort(404);

        return view('pages.rental_cars.show', compact('car'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $car = RentalCar::with(['attachments'])->find($id);

        if(!$car) abort(404);

        return view('pages.rental_cars.create', compact('car'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $car = RentalCar::find($id);
        if(!$car) abort(404);
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'model' => 'required',
            'year' => 'required|date_format:Y',
            'transmission_type' => 'required',
            'color_code' => 'required',
            'price_per_day' => 'required|numeric',
            'passengers' => 'required|numeric',
            'doors' => 'required|numeric',
            'horse_power' => 'required|numeric',
            'speed' => 'required|numeric',
            'fuel_kit' => 'required',
            'description' => 'nullable',

        ]);

        $car->update($request->all());
        // dd($car);
        if($request->has('files'))
        {
            $request->validate([
                'files.*' => 'image'
            ]);
         // $attachments = $car->attachments;
            // foreach($attachments as $attachment)
            // {
            //     $attch = Helper::file_delete($attachment->file_path);
            //     $attachment->delete();
            // }
            $files = $request->file('files');
            foreach($files as $index => $file)
            {
                $request['image'] = $file;
                // dd($request->all());
                $file_path = Helper::file_upload_with_array($request->all(), 'image', 'Rental_Car');
                $attachment['file_path'] = $file_path;
                $attachment['type'] = 'rental_cars';
                $car->attachments()->create($attachment);
            }
        }

        return back()->with('insert', 'Car Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $car = RentalCar::find($id);

        if(!$car) abort(404);

        $car->delete();

        return redirect()->route('rental-cars.index')->with('insert', 'Car Deleted Successfully');
    }
}
