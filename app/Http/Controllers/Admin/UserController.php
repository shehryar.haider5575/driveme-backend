<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\DataTables\UsersDataTable;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use Session;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        // $users = User::where('type','USER')->paginate();
        return $dataTable->render('pages.user.index');
        // return view('pages.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|confirmed',
        ])->validate();
        $request['type'] = "USER";
        $request['password'] = Hash::make($request->password);
        $randomToken = (User::latest()->first())? User::latest()->first()->id + 1 : 1;
        $request['refferal_code'] = date('y').date('m').date('d').$randomToken;
        $user = User::create($request->all());

        if($request->has('refferal_code'))
        {
            $user_refferal = User::where('refferal_code', $request->refferal_code)->first();
            $user->refferals()->create(['refferal_code' => $request->refferal_code, 'refferal_user_id' => $user_refferal->id]);
        }

        $user->refferal_code = $user->id.date('Y');
        $user->save();
        Session::flash('insert', "User Add Successfully");

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with(['trips' => function($q){
            $q->with(['driver' => function($t){
                $t->withTrashed();
            }, 'car' => function($t){
                $t->withTrashed();
            }]);
        }])->find($id);

        return view('pages.user.view', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('pages.user.create', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|unique:users,email,'.$id,
            'phone' => 'required|unique:users,phone,'.$id,
            'password' => 'nullable|confirmed',
        ])->validate();

        $user = User::find($id);
        $user->update($request->all());

        // Session::flash('insert', "User Updated Successfully");

        return redirect()->route('users.index')->with('insert', "User Updated Successfully");
    }

    public function updateStatus(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->only('status'));

        // Session::flash('insert', "User Status Updated Successfully");

        return back()->with('insert', "User Status Updated Successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->route('users.index')->with('insert', "User Deleted Successfully");
    }
}
