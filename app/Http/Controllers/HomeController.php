<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserTrip;


class HomeController extends AuthenticatedController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $addmonth = Carbon::now()->subMonth()->format('Y-m-d H:i:s');
        $reult['total_users'] = User::where('type', '<>', 'ADMIN')->count();
        $reult['total_chauffers'] = User::where(['type' => 'CAPTAIN'])->count();
        $reult['total_clients'] = User::where(['type' => 'USER'])->count();
        $reult['total_requests'] = User::where([['type', 'CAPTAIN'],['is_admin_approve', 0]])->count();
        $reult['total_trips'] = UserTrip::where('status', '<>', 'cancel')->count();
        $reult['total_earning'] = Payment::all()->sum('amount').'AED';

        $reult['today_registrations'] = User::where(['is_admin_approve' => 0, 'created_at' => Carbon::today()])->count();
        $reult['today_earning'] = Payment::where(['created_at' => Carbon::today()])->sum('amount').'AED';
        $reult['month_earning'] = Payment::whereMonth('created_at', Carbon::now()->month)->sum('amount').'AED';
        $reult['new_request'] = User::where([['type', 'USER']])->whereDate('created_at','>=',$addmonth)->take(10)->get();
        $reult['new_request_users'] = User::where([['type', 'USER']])->whereDate('created_at','>=',$addmonth)->count();

        return view('pages.dashboard.dashboard')->with($reult);

    }

    public function usersList()
    {
        $users = User::where('type','USER')->paginate(5);
        return view('pages.user.index',compact('users'));
    }

    public function userDetail($id)
    {
        $user = User::find($id);
        return view('pages.user.view',compact('user'));
    }

}
