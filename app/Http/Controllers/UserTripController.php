<?php

namespace App\Http\Controllers;

use App\Models\UserTrip;
use Illuminate\Http\Request;

class UserTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserTrip  $userTrip
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $trip_id)
    {
        $trip = UserTrip::where('user_id', $user_id)->find($trip_id);

        return view('pages.trips.trip_detail')->with('trip', $trip);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserTrip  $userTrip
     * @return \Illuminate\Http\Response
     */
    public function edit(UserTrip $userTrip)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserTrip  $userTrip
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserTrip $userTrip)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserTrip  $userTrip
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserTrip $userTrip)
    {
        //
    }
}
