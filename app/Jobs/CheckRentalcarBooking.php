<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\BookRentalCar;
use Carbon\Carbon;


class CheckRentalcarBooking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $booking = BookRentalCar::where('select_date', '<', Carbon::now())
                    ->where('request_status', 'pending')
                    ->where('status', '<>', 'complete')
                    ->where('status', '<>', 'cancel')
                    ->where('status', '<>', 'scheduled')
                    ->update(['status', 'cancel']);

        $scheduled_booking = BookRentalCar::where('date_from', '<', Carbon::now())
        ->where('request_status', 'pending')
        ->where('status', 'scheduled')
        ->update(['status', 'cancel']);
    }
}
