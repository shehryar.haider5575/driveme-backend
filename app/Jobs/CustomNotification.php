<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use App\Notifications\CustomEmail;
use Illuminate\Support\Facades\Log;
use App\Models\User;

class CustomNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 7200;

    protected $subject, $message, $users;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subject, $message, $users)
    {
        $this->subject = $subject;
        $this->message = $message;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $users = $this->users;

            foreach($users as $user)
            {
                Notification::send($user, new CustomEmail($this->subject, $this->message));
            }
            Log::debug("Cron Job for Custom Notification");
        } catch (\Exception $e) {
            Log::debug("Cron Job Error for Custom Notification");
            Log::debug($e->getMessage());
            //throw $th;
        }
    }
}
