<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Promocode as PromocodeNotifications;
use Illuminate\Support\Facades\Log;
use App\Models\User;

class SendPromoEmailQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 7200;

    protected $promos, $users;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($promos, $users)
    {
        $this->promos = $promos;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $users = $this->users;

            foreach($users as $user)
            {
                Notification::send($user, new PromocodeNotifications($this->promos));
            }
            Log::debug("Cron Job for Promocode");
        } catch (\Exception $e) {
            Log::debug("Cron Job Error for Promocode");
            Log::debug($e->getMessage());
            //throw $th;
        }

    }
}
