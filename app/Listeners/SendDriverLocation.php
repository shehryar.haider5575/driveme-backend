<?php

namespace App\Listeners;

use App\Events\DriverLocation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\EventData;
use Illuminate\Support\Facades\Redis;

class SendDriverLocation implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\DriverLocation  $event
     * @return void
     */
    public function handle(DriverLocation $event)
    {
        // Redis::publish('driverLocation11', json_encode($event->message));
        EventData::insert(['name' => 'DriverLocation', 'value' => json_encode($event->message)]);
        return $event->message;
    }
}
