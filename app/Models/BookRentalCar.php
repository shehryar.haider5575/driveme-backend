<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookRentalCar extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id', 'rental_car_id', 'payment_id', 'select_date', 'date_from', 'date_to', 'time_from', 'time_to', 'multiple_days','total_amount', 'status', 'request_status', 'payment_status',
    ];

    protected $appends = ['license_front_image', 'license_back_image', 'emirate_front_image', 'emirate_back_image'];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable')->latest();
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function rental_car()
    {
        return $this->belongsTo(RentalCar::class, 'rental_car_id');
    }

    public function payment()
    {
        return $this->belongsTo(Payment::class, 'payment_id');
    }

    public function getLicenseFrontImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%rent_booking_license_front%')->latest()->first();
        return $img->file_path??null;
    }

    public function getLicenseBackImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%rent_booking_license_back%')->latest()->first();
        return $img->file_path??null;
    }

    public function getEmirateFrontImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%rent_booking_emirate_front%')->latest()->first();
        return $img->file_path??null;
    }

    public function getEmirateBackImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%rent_booking_emirate_back%')->latest()->first();
        return $img->file_path??null;
    }
}
