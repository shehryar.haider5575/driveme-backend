<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Disscount extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'refferal_id', 'disscount', 'disscount_type', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function refferal()
    {
        return $this->belongsTo(FriendReffer::class, 'refferal_id');
    }
}
