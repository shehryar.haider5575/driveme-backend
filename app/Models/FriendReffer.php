<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FriendReffer extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * Get the user that owns the BankDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the user that owns the BankDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firend()
    {
        return $this->belongsTo(User::class, 'refferal_user_id');
    }
}
