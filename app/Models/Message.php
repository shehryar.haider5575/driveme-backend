<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $fillable = ['sender','receiver','type','message'];
    protected $appends = ['receiver_name', 'profile_image'];

    public function getReceiverNameAttribute()
    {
        return $this->belongsTo(User::class, 'receiver')->withTrashed()->first()->first_name;
    }

    public function getProfileImageAttribute()
    {
        return Attachment::where('type', 'profile_image')->where('attachable_id', $this->receiver)->first('file_path');
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver');
    }

    public function recentMessage()
    {
        return $this->belongsTo(User::class, 'receiver');
    }


}
