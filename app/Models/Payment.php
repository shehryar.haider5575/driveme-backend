<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function trip()
    {
        return $this->belongsTo(UserTrip::class, 'trip_id')->withTrashed();
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }
    /**
     * Get the bankDetail associated with the Payment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bankDetail()
    {
        return $this->hasOne(BankDetail::class,'id', 'payment_id');
    }
}
