<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocode extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['promocode', 'discount_type', 'discount_value', 'started_at', 'expired_at', 'status', 'title', 'description'];

    protected $casts = ['started_at' => 'datetime', 'expired_at' => 'datetime', 'discount_value' => 'float'];

    // public function users()
    // {
    //     return $this->hasManyThrough(User::class, PromocodeUser::class, 'user_id', 'id', 'id', 'id');
    // }
}
