<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromocodeUser extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'promocode_id', 'status'];

    public function promocode()
    {
        return $this->belongsTo(Promocode::class, 'promocode_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
