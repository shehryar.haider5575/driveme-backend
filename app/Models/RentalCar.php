<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RentalCar extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'model',
        'year',
        'color',
        'color_code',
        'description',
        'price_per_day',
        'price_per_hour',
        'passengers',
        'doors',
        'horse_power',
        'speed',
        'fuel_kit',
        'transmission_type',
        'status',
    ];
    protected $appends = ['avg_rating','attachments'];
    public function bookings()
    {
        return $this->hasMany(BookRentalCar::class)->latest();
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable')->latest();
    }
    public function getAttachmentsAttribute()
    {
        return $this->attachments()->get();
    }
    public function ratings()
    {
        return $this->hasMany(ReviewsAndRating::class, 'rental_car_id','id');
    }

    public function avg_rating()
    {
        return $this->ratings()->avg('rating');
    }

    public function getAvgRatingAttribute()
    {
        return $this->avg_rating();
    }
    public function getRatingsAttribute()
    {
        return $this->ratings();
    }
}
