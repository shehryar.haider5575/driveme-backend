<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReviewsAndRating extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function ratedUser()
    {
        return $this->belongsTo(User::class, 'from_id');
    }

    public function rental_car()
    {
        return $this->belongsTo(RentalCar::class, 'rental_car_id');
    }

    
}
