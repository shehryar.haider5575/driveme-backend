<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $appends = ['address', 'profile_image', 'passportsize_pic', 'emirate_front', 'emirate_back', 'license', 'avg_rating', 'license_front_image', 'license_back_image', 'full_name', 'current_month_earning', 'top_earn_required', 'completed_rides', 'top_ride_required', 'emirate_front_image', 'emirate_back_image'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'refferal_code',
        'emirates_id',
        'bio',
        'type',
        'phone',
        'is_archive',
        'status',
        'pay_per_minute',
        'pay_per_hour',
        'gender',
        'is_admin_approve',
        'total_balance',
        'current_status',
        'emirate',
        'city',
        'dob',
        'similar_app',
        'vendor_app',
        'disability',
        'driver_license_front_image_website',
        'driver_license_back_image_website',
        'driver_emirate_front_image_website',
        'driver_emirate_back_image_website',
        'license_number',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAddressAttribute()
    {
        return $this->hasOne(UserAddress::class, 'user_id')->where('type', 'home')->latest()->first();
    }
    public function promo_codes()
    {
        return $this->hasMany(PromocodeUser::class, 'user_id');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getProfileImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'profile_image')->first();
        return $img->file_path??null;
    }

    public function getPassportsizePicAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'passport_pic')->first();
        return $img->file_path??null;
    }

    public function getEmirateBackAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'emirate_back')->first();
        return $img->file_path??null;
    }

    public function getEmirateFrontAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'emirate_front')->first();
        return $img->file_path??null;
    }

    public function getLicenseAttribute()
    {
        return $this->morphMany(Attachment::class, 'attachable')->where('type', 'like' , '%driver_license%')->get();
    }

    public function getLicenseFrontImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%driver_license_front%')->latest()->first();
        return $img->file_path??null;
    }

    public function getLicenseBackImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%driver_license_back%')->latest()->first();
        return $img->file_path??null;
    }

    public function getEmirateFrontImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%driver_emirate_front%')->latest()->first();
        return $img->file_path??null;
    }

    public function getEmirateBackImageAttribute()
    {
        $img = $this->morphOne(Attachment::class, 'attachable')->where('type', 'like' , '%driver_emirate_back%')->latest()->first();
        return $img->file_path??null;
    }

    public function user_address()
    {
        return $this->hasMany(UserAddress::class, 'user_id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachable');
    }

    public function profile_image()
    {
        return $this->morphOne(Attachment::class, 'attachable')->where('type', 'profile_image');
    }

    public function refferals()
    {
        return $this->hasOne(FriendReffer::class, 'user_id');
    }

    public function Myrefferals()
    {
        return $this->hasMany(FriendReffer::class, 'refferal_user_id');
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'user_id');
    }

    public function trips()
    {
        if($this->type == 'USER') return $this->hasMany(UserTrip::class, 'user_id');

        return $this->hasMany(UserTrip::class, 'driver_id');
    }

    public function comnplaints()
    {
        return $this->hasMany(UserComplaint::class, 'user_id');
    }

    /**
     * Get all of the bank_details for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bank_details()
    {
        return $this->hasMany(BankDetail::class, 'user_id');
    }

    public function ratings()
    {
        return $this->hasMany(ReviewsAndRating::class, 'user_id');
    }

    public function avg_rating()
    {
        return $this->ratings()->avg('rating');
    }

    public function getAvgRatingAttribute()
    {
        return $this->avg_rating();
    }
    /**
     * Get all of the Disscount for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function disscount()
    {
        return $this->hasMany(Disscount::class, 'user_id');
    }

    public function earnings($month = 0)
    {
        return $this->trips()->whereMonth('created_at', Carbon::now()->month)->sum('amount');
    }

    public function getCurrentMonthEarningAttribute()
    {
        return $this->earnings();
    }

    public function ride()
    {
        return $this->hasMany(UserTrip::class, 'driver_id')->where('driver_status', 'complete')->whereMonth('created_at', Carbon::now()->month)->count();
    }
    public function getCompletedRidesAttribute()
    {
        return $this->ride();
    }

    public function getTopRideRequiredAttribute()
    {
        $earn = UserTrip::where('driver_id', '<>', NULL)->whereMonth('created_at', Carbon::now()->month)->groupBy('driver_id')->selectRaw('driver_id, count(driver_id) as rides')->orderBy('rides', 'desc')->first();
        $topearn = $this->ride();
        $earn = $earn->rides??0 - $topearn;
        $earn = number_format($earn,2);
        return $earn<0?0:$earn;
    }

    public function getTopEarnRequiredAttribute()
    {
        $earn = UserTrip::where('driver_id', '<>', NULL)->whereMonth('created_at', Carbon::now()->month)->groupBy('driver_id')->selectRaw('driver_id, SUM(amount) as earning')->orderBy('earning', 'desc')->first();
        $topearn = $this->earnings();
        $earn = $earn->earning??0 - $topearn;
        $earn = number_format($earn,2);
        return $earn<0?0:$earn;
    }

    public function routeNotificationForFcm($notification)
    {
        return $this->device_token;
    }
}
