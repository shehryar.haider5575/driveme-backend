<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserComplaint extends Model
{
    use HasFactory;

    protected $fillable = ['message', 'user_id', 'user_trip_id', 'status'];

    /**
     * Get the user that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the user that owns the Trips
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trips()
    {
        return $this->belongsTo(UserTrip::class, 'user_trip_id');
    }
}
