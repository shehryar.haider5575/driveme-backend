<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class UserTrip extends Model
{
    use HasFactory, SoftDeletes;

    protected $appends = ['pickup_address','avail_promocode', 'dropoff_address'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function avail_promocode()
    {
        return $this->belongsTo(Promocode::class, 'promocode_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function payment()
    {
        return $this->belongsToMany(BankDetail::class, 'payments', 'trip_id', 'payment_id');
    }

    public function paymentDetails()
    {
        return $this->hasOne(Payment::class, 'trip_id')->where('type','trip');
    }

    public function topupDetails()
    {
        return $this->hasOne(Payment::class, 'trip_id')->where('type','topup');
    }

    public function tipDetails()
    {
        return $this->hasOne(Payment::class, 'trip_id')->where('type','tip');
    }

    public function rentCarsDetails()
    {
        return $this->hasOne(Payment::class, 'trip_id')->where('type','rental_car');
    }

    public function getPickupAddressAttribute()
    {
        return UserAddress::withTrashed()->find($this->pickup_id)->address??null;
    }

    public function getAvailPromocodeAttribute()
    {
        return $this->avail_promocode()->first();
    }

    public function getDropoffAddressAttribute()
    {
        return UserAddress::withTrashed()->find($this->dropoff_id)->address??null;
    }

    public function pickup_address()
    {
        return $this->belongsTo(UserAddress::class, 'pickup_id');
    }

    public function dropoff_address()
    {
        return $this->belongsTo(UserAddress::class, 'dropoff_id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id');
    }

    public function scopeNearBy($query, $latitude, $longitude)
    {
        // dd($latitude);
        return $query->join('user_addresses', 'user_addresses.id', '=', 'user_trips.pickup_id')->select('user_trips.*',
         'user_addresses.id AS AID', DB::raw("6371 * acos(cos(radians(" . $latitude . "))
         * cos(radians(user_addresses.latitude))
         * cos(radians(user_addresses.longitude) - radians(" . $longitude . "))
         + sin(radians(" .$latitude. "))
         * sin(radians(user_addresses.latitude))) AS distance"))->having("distance", "<", 6371)->orderBy("distance",'asc');
        //  ->groupBy("user_trips.id");
    }

    public function complaint()
    {
        return $this->hasOne(UserComplaint::class, 'user_trip_id');
    }

    public function ratings()
    {
        return $this->hasMany(ReviewsAndRating::class, 'user_id','driver_id')->where([['rental_car_id','=',null],['type','user']]);
    }
}
