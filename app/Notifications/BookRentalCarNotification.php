<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class BookRentalCarNotification extends Notification
{
    use Queueable;
    private $bookedCar;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bookedCar)
    {
        $this->bookedCar = $bookedCar;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if($notifiable->type=='ADMIN') return ['database', 'mail'];
        else return ['database', 'mail', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->type == 'ADMIN')
        {
            return (new MailMessage)
                        ->line('Dear Admin,')
                        ->line('You have Recieved new Request for Car Booking')
                        ->line('Reuest Details:')
                        ->line('Car Name: '.$this->bookedCar->rental_car->name)
                        ->line('User Name: '.$this->bookedCar->user->full_name)
                        ->line('User Email: '.$this->bookedCar->user->email)
                        ->line('User Phone: '.$this->bookedCar->user->phone)
                        ->line('From: '.$this->bookedCar->time_from)
                        ->line('To: '.$this->bookedCar->time_to)
                        ->line('Date: '.$this->bookedCar->select_date)
                        ->line('Thank you for using our application!');
        }else
        {
            return (new MailMessage)
                        ->line('Dear '.$notifiable->full_name)
                        ->line('We have Recieved your Request for Car Booking')
                        ->line('Reuest Details:')
                        ->line('Car Name: '.$this->bookedCar->rental_car->name)
                        ->line('User Name: '.$this->bookedCar->user->full_name)
                        ->line('User Email: '.$this->bookedCar->user->email)
                        ->line('User Phone: '.$this->bookedCar->user->phone)
                        ->line('From: '.$this->bookedCar->time_from)
                        ->line('To: '.$this->bookedCar->time_to)
                        ->line('Date: '.$this->bookedCar->select_date)
                        ->line('Thank you for using our application!');
        }
    }

    public function toFcm($notifiable)
    {
	    // Log::debug('In FCM Function');
	    // Log::debug($this->subject);
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Request for Booking a Car',
            'body'         => 'Your request for '.$this->bookedCar->rental_car->name.' has been send',
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        // Log::debug('Return From FCM');
        //dd($message);
        // Log::debug(json_encode($message));
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->bookedCar->user_id,
            'rental_car_id' => $this->bookedCar->rental_car_id,
            'booking_id' => $this->bookedCar->id,
            'subject' => 'Rental Car Booking Submitted',
            'message' => 'Rental Car Booking recieved',
        ];
    }
}
