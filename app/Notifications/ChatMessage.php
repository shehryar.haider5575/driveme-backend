<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class ChatMessage extends Notification
{
    use Queueable;
    private $reciever, $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reciever, $message)
    {
        $this->reciever = $reciever;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'New Message from '.$this->reciever->first_name,
            'body'         => $this->message,
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'reciever_id' => $this->reciever->id,
            'message' => $this->message,
        ];
    }
}
