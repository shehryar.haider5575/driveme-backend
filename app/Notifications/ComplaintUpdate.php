<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class ComplaintUpdate extends Notification
{
    use Queueable;
    private $complaint;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($complaint)
    {
        $this->complaint = $complaint;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("Hey ".$notifiable->full_name??'')
                    ->line("Your Complaint Number #00".$this->complaint->id)
                    ->line("Has updated with status ".ucfirst($this->complaint->status))
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Your Complaint #00'.$this->complaint->id,
            'body'         => "Has updated with status ".ucfirst($this->complaint->status),
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->complaint->user_id,
            'message' => 'Your Complaint has Been'.ucfirst($this->complaint->status),
            'compaint_id' => $this->complaint->id,
            'compaint_status' => $this->complaint->status,
            'trip_id' => $this->complaint->user_trip_id??null,
        ];
    }
}
