<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class CustomEmail extends Notification
{
    use Queueable;

    private $subject, $message, $password;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($subject, $message, $password = null)
    {
        $this->subject = $subject;
        $this->message = $message;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database','mail','fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $content['message'] = $this->message;
        $content['type'] = $notifiable->type;
        if(!is_null($this->password))
        {
            $content['email'] = $notifiable->email;
            $content['password'] = $this->password;
            return (new MailMessage)
                    ->view('partials.captain_email', ['content' => $content])
                    ->subject($this->subject)
                    ->line('Thank you for using our application!');
        }
        return (new MailMessage)
                    ->view('partials.email_template', ['content' => $content])
                    ->subject($this->subject)
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
	    // Log::debug('In FCM Function');
	    // Log::debug($this->subject);
        $message = new FcmMessage();
        $message->content([
            'title'        => $this->subject,
            'body'         => $this->message,
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        // Log::debug('Return From FCM');
        //dd($message);
        // Log::debug(json_encode($message));
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $notifiable->id??'',
            'subject' => $this->subject,
            'message' => $this->message,
        ];
    }
}
