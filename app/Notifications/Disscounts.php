<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class Disscounts extends Notification
{
    use Queueable;
    private $disscounts;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($disscounts)
    {
        $this->disscounts = $disscounts;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("Dear $notifiable->name")
                    ->line("Congratulation You earn ".$this->dissconts->disscount.($this->dissconts->disscount_type == 'percent')?'%':'AED')
                    ->line("Your Friend has booked first ride since you invite")
                    ->line('Thank you for using our application!');
    }


    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Congratulations You Earn '.$this->dissconts->disscount.($this->dissconts->disscount_type == 'percent')?'%':'AED',
            'body'         => "Your Friend has booked first ride since you invite",
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'disscount_id' => $this->disscounts->id,
            'refferal_id' => $this->disscounts->refferal_id,
            'disscount' => $this->disscounts->disscount,
            'disscount_type' => $this->disscounts->type,
            'message' => 'Congratulation you earned new Disscount',
        ];
    }
}
