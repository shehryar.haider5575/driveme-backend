<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class NewTripNotifications extends Notification
{
    use Queueable;
    private $driver, $ride;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($driver, $ride)
    {
        $this->driver = $driver;
        $this->ride = $ride;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'fcm'];
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'New Trip is added',
            'body'         => "check the detials of new trip",
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
    
        return [
            'driver_id' => $this->driver->id,
            'ride_id' => $this->ride->id,
            'message' => "New Trip is added",
        ];
    }
}
