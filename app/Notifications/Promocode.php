<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Benwilkins\FCM\FcmMessage;

class Promocode extends Notification
{
    use Queueable;

    private $promo;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($promo)
    {
        $this->promo = $promo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $html = "Book a trip and enjoy disscount upto {$this->promo->discount_value}";
        $html .= ($this->promo->discount_type=='percent')?'%':'AED';
        Log::debug("Mail Function");
        Log::debug($html);

        return (new MailMessage)
                    ->line('Dear '.$notifiable->first_name)
                    ->line($html)
                    ->line('Use Promocode:')
                    ->line($this->promo->promocode)
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Congratulations You Earn '.$this->dissconts->disscount.($this->dissconts->disscount_type == 'percent')?'%':'AED',
            'body'         => "Your Friend has booked first ride since you invite",
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
