<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Benwilkins\FCM\FcmMessage;
use Log;

class RideStatusUpdate extends Notification
{
    use Queueable;
    private $driver, $ride;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($driver, $ride)
    {
        $this->driver = $driver;
        $this->ride = $ride;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $status = str_replace('_',' ',strtoupper($this->ride->driver_status));
        return (new MailMessage)
                    ->line("Dear $notifiable->last_name,")
                    ->line("Your Ride is ".$status." By ".$this->driver->first_name)
                    ->line("Your Driver Details:")
                    ->line("Phone: ".$this->driver->phone)
                    ->line("Email: ".$this->driver->email)
                    ->line("Gender: ".$this->driver->gender)
                    ->line('Thank you for using our application!');
    }

    public function toFcm($notifiable)
    {
        $status = str_replace('_',' ',strtoupper($this->ride->driver_status));
        $message = new FcmMessage();
        $message->content([
            'title'        => 'Your Ride is Updated',
            'body'         => "Your Ride is ".$status." By ".$this->driver->first_name,
        ])->data([
            'param1' => 'baz' // Optional
        ])->priority(FcmMessage::PRIORITY_HIGH); // Optional - Default is 'normal'.

        //dd($message);
        return $message;

    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $status = str_replace('_',' ',strtoupper($this->ride->driver_status));

        return [
            'driver_id' => $this->driver->id,
            'driver_status' => $this->ride->driver_status,
            'ride_id' => $this->ride->id,
            'message' => "Your Ride is ".$status." By ".$this->driver->first_name.' '.$this->driver->last_name,
        ];
    }
}
