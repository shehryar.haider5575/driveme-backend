<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use App\Models\UserComplaint;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('notifications', function () {
            $notifications = Auth::user()->unreadNotifications()->orderBy('created_at', 'desc')->get()->toArray()??[];
            $html = '';
            // dd($notifications);
            foreach($notifications as $notification)
            {
                $link = route('notification.show', $notification['id']);
                $user = $this->getUser($notification['data']['user_id']);
                $user_name = $user->full_name??null;
                $subject = $notification['data']['subject']??null;
                $message = $notification['data']['message']??null;
                $href = '';
                if($notification['type'] == 'App\Notifications\AdminNewRegistration')
                {
                    $href = route('users.show', $notification['data']['user_id']);
                }

                if($notification['type'] == 'App\Notifications\BookRentalCarNotification')
                {
                    if(!isset($notification['data']['booking_id'])) $href = '#';
                    else $href = route('booking-cars.show', $notification['data']['booking_id']);
                }

                if($notification['type'] == 'App\Notifications\ComplaintUpdate')
                {
                    $href = route('user-complaints.show', $data['data']['complaint_id']);
                }

                if($message) $message = substr($message, 0, 100);
                $img_path = $user->profile_image??' ';
                $html .= "<a class='dropdown-item preview-item' href={$href}>
                            <div class='preview-thumbnail'>
                                <img src=".asset($img_path)." alt='image' class='img-sm profile-pic' />
                            </div>
                            <div class='preview-item-content flex-grow py-2'>
                                <span class='preview-subject ellipsis font-weight-medium text-dark'>
                                {$subject}
                                </span>
                                <p class='fw-light small-text mt-1 mb-0'>
                                    {$message}
                                </p>
                            </div>
                        </a>";
            }
            return $html;
        });

        Blade::directive('complaints', function () {
            $complaints = UserComplaint::where('status', 'pending')->get();
            $all_link = route('user-complaints.index');
            $html = '<div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">
                        <a class="dropdown-item py-3 border-bottom" href="'.$all_link.'">
                            <p class="mb-0 font-weight-medium float-left">You have '.count($complaints).' unread Complaint </p>
                                <span class="badge badge-pill badge-primary float-right">View all</span>
                        </a>';

            foreach($complaints as $key => $complaint)
            {
                $link = route('user-complaints.show', $complaint->id);
                $html .= "<a class='dropdown-item preview-item py-3' href={$link}>
                            <div class='preview-thumbnail'>
                                <i class='mdi mdi-alert m-auto text-primary'></i>
                            </div>
                            <div class='preview-item-content'>
                                <h6 class='preview-subject fw-normal text-dark mb-1'>New Complaint From {$complaint->user->full_name}</h6>
                                <p class='fw-light small-text mb-0'> {$complaint->created_at->diffForHumans()} </p>
                            </div>
                        </a>";
            }
            if(count($complaints) <= 0)
            {
                $html .= '<a class="dropdown-item preview-item py-3">
                            <div class="preview-thumbnail">
                                <i class="mdi mdi-emoticon m-auto text-primary"></i>
                            </div>
                            <div class="preview-item-content">
                                <h6 class="preview-subject fw-normal text-dark mb-1">No Complaints</h6>
                                <p class="fw-light small-text mb-0">By Now</p>
                            </div>
                        </a>';
            }
            $html .= '</div>';

            return $html;
        });

        Paginator::useBootstrap();
        JsonResource::withoutWrapping();
    }

    public function getUser($userId)
    {
        $user = User::find($userId);
        if(!$user)
        {
            return null;
        }

        return $user;
    }
}
