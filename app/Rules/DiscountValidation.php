<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DiscountValidation implements Rule
{
    private $disscountType;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($disscountType)
    {
        $this->disscountType = $disscountType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->disscountType == 'percent')
        {
            if($value > 100) return false;

            return true;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute value should be less than or equals to 100 if disscount type is percent.';
    }
}
