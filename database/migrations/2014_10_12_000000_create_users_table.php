<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('bio')->nullable();
            $table->string('phone')->nullable();
            $table->bigInteger('emirates_id')->unique()->nullable();
            $table->string('refferal_code')->unique()->nullable();
            $table->tinyInteger('is_archive')->nullable();
            $table->enum('is_admin_approve', ['0', '1'])->default('1');
            $table->tinyInteger('status')->default(1);
            $table->string('driver_license_front_image_website');
            $table->string('driver_license_back_image_website');
            $table->string('driver_emirate_front_image_website');
            $table->string('driver_emirate_back_image_website');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
