<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_trips', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('promocode_id')->constrained('promocodes')->onDelete('cascade');
            $table->foreignId('driver_id')->nullable()->default(null)->constrained('users')->onDelete('cascade');
            $table->foreignId('car_id')->nullable()->default(null)->constrained('cars')->onDelete('cascade');
            $table->foreignId('pickup_id')->constrained('user_addresses')->onDelete('cascade');
            $table->foreignId('dropoff_id')->constrained('user_addresses')->onDelete('cascade');
            $table->enum('trip_type', ['ride_now', 'garage_pickup', 'rta_inspection', 'inter_city'])->default('ride_now');
            $table->double('amount')->default(0.00);
            $table->enum('status', ['pending', 'schedule', 'processing', 'complete', 'cancel'])->default('pending');
            $table->enum('driver_status', ['pending', 'accept', 'complete', 'reject', 'timeout','confirm_arrival','start_drive','end_drive'])->default('pending');
            $table->enum('payment_status', ['pending', 'paid', 'refunded'])->default('pending');
            $table->enum('booking_type', ['pay_per_minute', 'pay_per_hour'])->default('pay_per_minute');
            $table->string('total_ride_time', 100)->nullable();
            $table->string('pm_type', 100)->nullable();
            $table->integer('hours', 100)->nullable();
            $table->timestamp('schedule_datetime')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->timestamp('start_at')->nullable();
            $table->text('instruction')->nullable();
            $table->mediumText('reason')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('user_trips');
    }
};
