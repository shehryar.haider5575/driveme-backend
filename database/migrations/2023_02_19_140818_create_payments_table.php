<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('trip_id')->nullable()->constrained('user_trips')->onDelete('cascade');
            $table->foreignId('payment_id')->nullable()->constrained('bank_details')->onDelete('cascade');
            $table->double('amount')->nullable()->default(0.00);
            $table->string('promocode')->nullable();
            $table->string('discount_type')->nullable();
            $table->string('discount_value')->nullable();
            $table->string('payment_intent')->nullable();
            $table->text('transaction_id')->nullable();
            $table->enum('payment_type', ['stripe', 'paypal', 'bank', 'other','payment'])->default('stripe');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
