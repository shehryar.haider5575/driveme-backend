<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rental_cars', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('model')->nullable();
            $table->string('year')->nullable();
            $table->string('color')->nullable();
            $table->string('color_code')->nullable();
            $table->text('description')->nullable();
            $table->double('price_per_day');
            $table->double('price_per_hour')->nullable();
            $table->double('passengers')->default(1);
            $table->double('doors')->default(2);
            $table->double('horse_power')->nullable(2);
            $table->double('speed')->nullable();
            $table->string('fuel_kit')->default('petrol');
            $table->enum('transmission_type', ['automatic', 'manual', 'other'])->default('manual');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rental_cars');
    }
};
