<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_rental_cars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('rental_car_id')->constrained('rental_cars')->onDelete('cascade');
            $table->foreignId('payment_id')->nullable()->constrained('bank_details')->onDelete('cascade');
            $table->timestamp('select_date')->nullable();
            $table->timestamp('date_from')->nullable();
            $table->timestamp('date_to')->nullable();
            $table->string('time_from');
            $table->string('time_to');
            $table->enum('multiple_days', ['1', '0'])->default('0');
            $table->enum('status', ['pending', 'complete', 'active', 'scheduled', 'cancel', 'reject'])->default('pending');
            $table->enum('request_status', ['pending', 'accept', 'reject'])->default('pending');
            $table->enum('payment_status', ['pending', 'processing', 'paid'])->default('pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_rental_cars');
    }
};
