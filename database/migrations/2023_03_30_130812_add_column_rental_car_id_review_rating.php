<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reviews_and_ratings', function (Blueprint $table) {
            $table->foreignId('rental_car_id')->nullable()->constrained('rental_cars')->onDelete('cascade')->after('user_id');
            $table->enum('type', ['user', 'rental_car'])->nullable()->default('user')->after('review');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reviews_and_ratings', function (Blueprint $table) {
            $table->dropForeign('rental_car_id');
            $table->dropColumn('type');
        });
    }
};
