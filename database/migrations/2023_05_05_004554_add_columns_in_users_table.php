<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('emirate', 100)->nullable()->default('text');
            $table->string('city', 100)->nullable()->default('text');
            $table->date('dob')->nullable();
            $table->tinyInteger('similar_app')->nullable()->default(0);
            $table->tinyInteger('vendor_app')->nullable()->default(0);
            $table->tinyInteger('disability')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['device_token',
            'emirate',
            'city',
            'dob',
            'similar_app',
            'vendor_app',
            'disability'
        ]);
        });
    }
};
