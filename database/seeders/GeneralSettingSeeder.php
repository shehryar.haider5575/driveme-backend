<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeneralSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('general_settings')->insert(['key' => 'about', 'value' => null]);
        DB::table('general_settings')->insert(['key' => 'terms&condition', 'value' => null]);
        DB::table('general_settings')->insert(['key' => 'reward_on_reffer', 'value' => 0]);
    }
}
