@extends('layouts.default')
@section('title', 'Add App Banner')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add App Banner</h4>
                        <form class="forms-sample" method="post" action="{{route('appbanner.store')}}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="content" class="col-sm-3 col-form-label">Content</label>
                                <div class="col-sm-9">
                                    <input type="text" name="content" class="form-control" id="content" placeholder="Enter COntent"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-sm-3 col-form-label">Attach Banner</label>
                                <div class="col-sm-9">
                                    <input type="file" name="file" class="form-control" id="file">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
