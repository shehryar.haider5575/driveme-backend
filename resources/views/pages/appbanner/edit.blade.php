@extends('layouts.default')
@section('title', 'Edit App Banner')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit App Banner</h4>
                        <form class="forms-sample" method="post" action="{{route('appbanner.update',$appbanner->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="content" class="col-sm-3 col-form-label">Content</label>
                                <div class="col-sm-9">
                                    <input type="text" name="content" class="form-control" id="content" placeholder="Enter COntent" value="{{$appbanner->content}}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="file" class="col-sm-3 col-form-label">Attach Banner</label>
                                <div class="col-sm-9">
                                    <img src="{{asset($appbanner->file_path)}}" alt="{{$appbanner->content}}" width="50">
                                    <input type="file" name="file" class="form-control" id="file">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                  <label class="form-check-label" for="status1">
                                    <input type="radio" class="form-check-input" name="status" id="status1" {{($appbanner->status)? 'checked' : ''}} value="1">
                                    Active
                                  <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check">
                                  <label class="form-check-label" for="status2">
                                    <input type="radio" class="form-check-input" name="status" id="status2" {{(!$appbanner->status)? 'checked' : ''}} value="0">
                                    Deactive
                                  <i class="input-helper"></i></label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary me-2">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
