@extends('layouts.default')
@section('title', 'App Banner List')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title card-title-btn">
                            App Banner List
                            <div style="float: right;">
                                <a href="{{ route('appbanner.create') }}" class="btn btn-primary btn-sm">Create App Banner</a>
                            </div>
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.NO#</th>
                                        <th>Content</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($appBanners as $key => $appbanner)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $appbanner->content }}</td>
                                            <td><img src="{{ asset($appbanner->file_path) }}" alt="{{ $appbanner->content }}"></td>
                                            <td>
                                                <form action="{{route('banner.status',$appbanner->id)}}" class="d-flex" method="post">
                                                    @csrf
                                                    @method('PUT')
                                                    <input type="hidden" name="status" value="{{$appbanner->status ? 0 : 1 }}" />
                                                    <button type="submit" class="btn badge badge-{{ $appbanner->status ? 'success' : 'danger' }}">
                                                        {{ $appbanner->status ? 'Active' : 'Deactive' }}
                                                    </button>
                                                </form>
                                            </td>
                                            <td><a href="{{ route('appbanner.edit', $appbanner->id) }}" class="btn btn-warning btn-xs">EDIT</a></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div>Showing {{($appBanners->currentpage()-1)*$appBanners->perpage()+1}} to {{$appBanners->currentpage()*$appBanners->perpage()}}
                                of  {{$appBanners->total()}} entries
                                @if ($appBanners->hasPages())
                                    <div class="pagination-wrapper" style="float: right;">
                                        {{ $appBanners->links() }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
