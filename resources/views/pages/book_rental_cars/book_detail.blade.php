@extends('layouts.default')
@section('title', "View Booking Detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Booking Detail</h4>
                        <div class="row mt-4">
                            <h5>User Info:</h5>
                            <div class="col-4">
                                @if (is_null($booking->user->profile_image))
                                <div class="img-thumb">
                                    <i class="mdi mdai-user"></i>
                                </div>
                                @else
                                <img src="{{ asset($booking->user->profile_image?? '') }}" />
                                @endif
                                <p><a href="{{ route('users.show', $booking->user->id) }}">{{ $booking->user->full_name }}</a></p>
                            </div>
                            <div class="col-4">
                                <p>{{ $booking->user->email }}</p>
                            </div>
                            <div class="col-4">
                                <p>{{ $booking->user->phone }}</p>
                            </div>
                        </div>
                        <div class="w-100 border"></div>
                        <div class="row mt-4">
                            <h5>Booking Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Booking Id: #000{{$booking->id}}
                                    </div>
                                    <div class="col-4">
                                        @php
                                        $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $booking->date_to);
                                        $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $booking->date_from);
                                        $days = $to->diffInDays($from);
                                        @endphp
                                        Days: {{ $days>1?$days.' Days':'1 Day' }}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Date From: {{ $booking->date_from }}
                                    </div>
                                    <div class="col-4">
                                        To: {{ $booking->date_to }}
                                    </div>
                                    <div class="col-4">
                                        Current Status:
                                        <select name="status" class="form-control" onchange="changebookingStatus(this, '{{ $booking->id }}')">
                                            <option value="pending" @selected($booking->status=='pending')>Pending</option>
                                            <option value="complete" @selected($booking->status=='complete')>Complete</option>
                                            <option value="cancel" @selected($booking->status=='cancel')>Cancel</option>
                                        </select>
                                        <br>
                                        <br>
                                        <label>Booking Request Status:</label>
                                        <select class="form-control" name='request_status' onchange='changeStatus(this, {{$booking->id}})'>
                                            <option value='pending' @selected($booking->request_status=='pending'?true:false)>Pending</option>
                                            <option value='accept' @selected($booking->request_status=='accept'?true:false)>Accept</option>
                                            <option value='reject' @selected($booking->request_status=='reject'?true:false)>Reject</option>
                                        </select>
                                        {{-- Booking Request Status: {{ $booking->request_status }} --}}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Car Name: <a href="{{ route('rental-cars.show', $booking->rental_car->id) }}">{{ $booking->rental_car->name }}</a>
                                    </div>
                                    <div class="col-4">
                                        Car Model: {{ $booking->rental_car->model }}
                                    </div>
                                    <div class="col-4">
                                        Car Description: {!! $booking->rental_car->description !!}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        @php
                                            $day = $booking->multiple_days<= 0?1:$booking->multiple_days;
                                            $price = $day * $booking->rental_car->price_per_day;
                                        @endphp
                                        Payment: {{ number_format($price, 2) }} AED
                                    </div>
                                    <div class="col-4">
                                        Payment Status: {{ $booking->payment_status }}
                                    </div>
                                </div>
                                @foreach ($booking->rental_car->ratings as $ratings)
                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Rating: {{ $ratings->rating }}/5
                                            Avg. Rating: {{ $booking->rental_car->avg_rating??0.00 }}/5
                                        </div>
                                        <div class="col-8">
                                            Review: {{ $ratings->review }}
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <div class="card">
                                <div class="card-title">
                                    <h5>Attachments</h5>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6 p-4 border">
                                            <h4>Driver Licence (Front)</h4>
                                            @empty($booking->license_front_image)
                                            <a href="{{ asset($booking->license_front_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->license_front_image) }}" /></a>
                                            @else
                                            <a href="{{ asset($booking->license_front_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->license_front_image) }}" /></a>
                                            @endempty
                                        </div>
                                        <div class="col-6 p-4 border">
                                            <h4>Driver License (Back)</h4>
                                            @empty($booking->license_back_image)
                                            <a href="{{ asset($booking->license_back_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->license_back_image) }}" /></a>
                                            @else
                                            <a href="{{ asset($booking->license_back_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->license_back_image) }}" /></a>
                                            @endempty
                                        </div>

                                        <div class="col-6 p-4 border">
                                            <h4>Emirate Id (Front)</h4>
                                            @empty($booking->emirate_front_image)
                                            <a href="{{ asset($booking->emirate_front_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->emirate_front_image) }}" /></a>
                                            @else
                                            <a href="{{ asset($booking->emirate_front_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->emirate_front_image) }}" /></a>
                                            @endempty
                                        </div>

                                        <div class="col-6 p-4 border">
                                            <h4>Emirate Id (Back)</h4>
                                            @empty($booking->emirate_back_image)
                                            <a href="{{ asset($booking->emirate_back_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->emirate_back_image) }}" /></a>
                                            @else
                                            <a href="{{ asset($booking->emirate_back_image) }}"><img style="max-height: 120px;" src="{{ asset($booking->emirate_back_image) }}" /></a>
                                            @endempty
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
  <script>
    function changeStatus(data, id)
    {
        let status = $(data).val();
        // console.log('Stataus');
        window.location = `${status}/${id}`;
    }
  </script>
  <script>
    function changebookingStatus(data, id)
    {
        let status = $(data).val();
        // console.log('Stataus');
        window.location = `status/${status}/${id}`;
    }
  </script>
@endpush
