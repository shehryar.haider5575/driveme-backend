@extends('layouts.default')
@section('title', 'Booking List')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Booking List</h4>
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
@push('scripts')
  <script>
    function changeStatus(data, id)
    {
        let status = $(data).val();
        // console.log('Stataus');
        window.location = `booking-cars/${status}/${id}`;
    }
  </script>
  <script>
    function changebookingStatus(data, id)
    {
        let status = $(data).val();
        // console.log('Stataus');
        window.location = `booking-cars/status/${status}/${id}`;
    }
  </script>
@endpush
@endsection
