@extends('layouts.default')
@section('title', !isset($brand)?'Add Brand':'Edit Brand')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ !isset($brand)?'Add Brand':'Edit Brand' }}</h4>
                        <form class="forms-sample" action="{{ !isset($brand)?route('brands.store'):route('brands.update', $brand->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @isset($brand)
                                @method('PUT')
                            @endisset
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{old('name', $brand->name??'')}}" name="name" id="name" placeholder="name">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="logo">Logo</label>
                                <input type="file" accept="image/*" name="logo_file" id="logo" class="form-control" />
                                @isset($brand->logo)
                                    <img src="{{asset($brand->logo)}}" alt="{{ $brand->name }}" class="thumbnail-img"/>
                                @endisset
                                @error('logo_file')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            @isset($brand)
                                <div class="form-group">
                                    <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
                                        <option value="active" @selected(old('status', $brand->status) == 'active')>Active</option>
                                        <option value="inactive" @selected(old('status', $brand->status) == 'inactive')>In Active</option>
                                    </select>
                                        @error('status')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endisset

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
