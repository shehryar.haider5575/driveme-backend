@extends('layouts.default')
@section('title', 'Car Brands List')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Brands List</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.NO#</th>
                    <th>Name</th>
                    <th>Logo</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($brands as $key => $brand)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$brand->name}}</td>
                        <td>
                            @empty($brand->logo)
                                N/A
                            @endempty
                            @if (!empty($brand->logo))
                                <img src="{{asset($brand->logo)}}" alt="{{$brand->name}}">
                            @endif
                        </td>
                        <td>{{$brand->status}}</td>
                        <td>
                            <form action="{{route('brands.destroy', $brand->id)}}" method="post" class="d-flex">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('brands.edit', $brand->id) }}" class="btn btn-warning btn-xs mr-1">EDIT</a>
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <th colspan="5" class="text-center text-danger">Record not found</th>
                    </tr>
                  @endforelse
                </tbody>
              </table>
              <div>Showing {{($brands->currentpage()-1)*$brands->perpage()+1}} to {{$brands->currentpage()*$brands->perpage()}}
                  of  {{$brands->total()}} entries
                  @if ($brands->hasPages())
                      <div class="pagination-wrapper" style="float: right;">
                          {{ $brands->links() }}
                      </div>
                  @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
