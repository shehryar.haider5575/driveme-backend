@extends('layouts.default')
@section('title', !isset($model)?'Add Model':'Edit Model')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ !isset($model)?'Add Model':'Edit Model' }}</h4>
                        <form class="forms-sample" action="{{ !isset($model)?route('models.store'):route('models.update', $model->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @isset($model)
                                @method('PUT')
                            @endisset
                            <div class="form-group">
                                <label for="brand_id">Brands</label>
                                <select class="form-control @error('brand_id') is-invalid @enderror" name="brand_id" id="brand_id">
                                    <option></option>
                                    @foreach ($brands as $brand)
                                        <option value="{{ $brand->id }}" @selected(old('brand_id', $model->brand_id??null ) == $brand->id )>{{ $brand->name }}</option>
                                    @endforeach
                                </select>
                                @error('brand_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control @error('name') is-invalid @enderror" value="{{old('name', $model->name??'')}}" name="name" id="name" placeholder="name">
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="logo">Logo</label>
                                <input type="file" accept="image/*" name="logo_file" id="logo" class="form-control" />
                                @isset($model->logo)
                                    <img src="{{asset($model->logo)}}" alt="{{ $model->name }}" class="thumbnail-img"/>
                                @endisset
                                @error('logo_file')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            @isset($model)
                                <div class="form-group">
                                    <select class="form-control @error('status') is-invalid @enderror" name="status" id="status">
                                        <option value="active" @selected(old('status', $model->status) == 'active')>Active</option>
                                        <option value="inactive" @selected(old('status', $model->status) == 'inactive')>In Active</option>
                                    </select>
                                        @error('status')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            @endisset

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
