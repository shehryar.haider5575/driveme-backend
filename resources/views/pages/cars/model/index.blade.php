@extends('layouts.default')
@section('title', 'Car Models List')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Models List</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.NO#</th>
                    <th>Name</th>
                    <th>Logo</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($models as $key => $model)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$model->name}}</td>
                        <td>
                            @empty($model->logo)
                                N/A
                            @endempty
                            @if (!empty($model->logo))
                                <img src="{{asset($model->logo)}}" alt="{{$model->name}}">
                            @endif
                        </td>
                        <td>{{$model->status}}</td>
                        <td>
                            <form action="{{route('models.destroy', $model->id)}}" method="post" class="d-flex justify-content-between">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('models.edit', $model->id) }}" class="btn btn-warning btn-xs">EDIT</a>
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <th colspan="5" class="text-center text-danger">Record not found</th>
                    </tr>
                  @endforelse
                </tbody>
              </table>
              <div>Showing {{($models->currentpage()-1)*$models->perpage()+1}} to {{$models->currentpage()*$models->perpage()}}
                  of  {{$models->total()}} entries
                  @if ($models->hasPages())
                      <div class="pagination-wrapper" style="float: right;">
                          {{ $models->links() }}
                      </div>
                  @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
