@extends('layouts.default')
@section('title', "View Payment Detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Trip Detail</h4>
                        <div class="row mt-4">
                            <h5>User Info:</h5>
                            <div class="col-4">
                                @if (is_null($complaint->user->profile_image))
                                <div class="img-thumb">
                                    <i class="mdi mdai-user"></i>
                                </div>
                                @else
                                <img src="{{ asset($complaint->user->profile_image?? '') }}" />
                                @endif
                                <p><a href="{{ route('users.show', $complaint->user->id) }}">{{ $complaint->user->full_name }}</a></p>
                            </div>
                            <div class="col-4">
                                <p>{{ $complaint->user->email }}</p>
                            </div>
                            <div class="col-4">
                                <p>{{ $complaint->user->phone }}</p>
                            </div>
                        </div>
                        <div class="w-100 border"></div>
                            <div class="row mt-4">
                                <h5>Trip Details</h5>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-4">
                                            Trip Id: <a href="{{ route('user-trips.show', $complaint->trips->id) }}">#000{{$complaint->trips->id}}</a>
                                        </div>
                                        <div class="col-4">
                                            Trip Type: {{ ucfirst(str_ireplace('_', ' ', $complaint->trips->trip_type)) }}
                                        </div>
                                        <div class="col-4">
                                            Trip Date: {{$complaint->trips->schedule_datetime??$complaint->trips->created_at }}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Pickup Address: {{$complaint->trips->pickup_address}}
                                        </div>
                                        <div class="col-4">
                                            Dropoff Address: {{$complaint->trips->dropoff_address}}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Driver: @if ($complaint->trips->driver)
                                                <a href="{{ route('driver.requested.id', $complaint->trips->driver->id) }}">{{ $complaint->trips->driver->full_name }}</a>
                                            @endif
                                        </div>
                                        <div class="col-4">
                                            Driver Status: {{$complaint->trips->driver_status}}
                                        </div>
                                        <div class="col-4">
                                            Booking Type: {{$complaint->trips->booking_type == 'pay_per_minute'?'Pay Per Minute':'Pay per Hour' }}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Payment: {{ $complaint->trips->amount }} AED
                                        </div>
                                        <div class="col-4">
                                            Payment Status: {{$complaint->trips->payment_status}}
                                        </div>
                                        <div class="col-12 mt-2">
                                            Instruction:
                                            <p>{!! $complaint->trips->instruction !!}</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        <div class="row mt-4">
                            <h5>Complaint Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Message:
                                        <p> {!! $complaint->message !!} </p>
                                    </div>
                                    <div class="col-4">
                                        Complaint Status: {{ $complaint->status }}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Date:
                                        <p>{!! $complaint->created_at !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
