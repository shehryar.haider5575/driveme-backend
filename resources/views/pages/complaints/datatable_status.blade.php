<form action="{{route('user-complaints.update', $id)}}" class="d-flex" method="post" id="status">
    @csrf
    @method('PUT')

    <select class="form-control" name="status" onchange="$('#status').submit()">
        <option value="pending" @selected($status=='pending'?true:false)>Pending</option>
        <option value="resolved" @selected($status=='resolved'?true:false)>Resolved</option>
    </select>
</form>
