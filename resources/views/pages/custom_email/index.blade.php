@extends('layouts.default')
@section('title', 'Custom Email')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Custom Email</h4>
                        <form class="forms-sample" method="post" action="{{route('custom-notification.store')}}">
                            @csrf
                            {{-- <input type="hidden" name="exist" value="{{($generalSetting)? 1 : 0}}"> --}}
                            <div class="form-group">
                                <label for="users">Users</label>
                                <select id="users" name="users[]" class="form-control select2 @error('users') is-invalid @enderror" multiple>
                                    <option value="all" @selected(old('users')=='all')>All Users</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @selected(old('users')==$user->id)>{{ $user->full_name }}</option>
                                    @endforeach
                                </select>
                                @error('users')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <textarea id="subject" name="subject" class="form-control textarea @error('subject') is-invalid @enderror" cols="12" rows="12"></textarea>
                                @error('subject')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea id="message" name="message" class="form-control textarea @error('message') is-invalid @enderror" cols="12" rows="12"></textarea>
                                @error('message')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
