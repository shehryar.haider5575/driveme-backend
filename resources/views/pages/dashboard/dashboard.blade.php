@extends('layouts.default')
@section('title','Dashboard')
@section('content')
@push('heads')
<meta name="total_clients" content="{{ $total_clients }}"/>
<meta name="total_chauffers" content="{{ $total_chauffers }}"/>

<meta name="total_chauffers" content="{{ $total_chauffers }}"/>
<meta name="total_chauffers" content="{{ $total_chauffers }}"/>

@endpush
    <div class="content-wrapper homepage ">
      <div class="row">
        <div class="col-sm-12">
          <div class="home-tab">
            {{-- <div class="d-sm-flex align-items-center justify-content-between border-bottom">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">Overview</a>
                </li>
              </ul>
            </div> --}}
            <div class="tab-content tab-content-basic">
              <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
               {{--  <div class="row">
                  <div class="col-sm-12">
                    <div class="statistics-details d-flex align-items-center justify-content-between">
                      <div>
                        <p class="statistics-title">Total Users</p>
                        <h3 class="rate-percentage"></h3>
                      </div>
                      <div>
                        <p class="statistics-title">Total Clients</p>
                        <h3 class="rate-percentage">{{ $total_clients }}</h3>
                      </div>
                      <div>
                        <p class="statistics-title">Total Chauffers</p>
                        <h3 class="rate-percentage">{{ $total_chauffers }}</h3>
                      </div>
                      <div>
                        <p class="statistics-title">New Requests</p>
                        <h3 class="rate-percentage">{{ $total_requests }}</h3>
                      </div>
                      <div class="d-none d-md-block">
                        <p class="statistics-title">Total Earning</p>
                        <h3 class="rate-percentage">{{ $total_earning }}</h3>
                      </div>
                      <div class="d-none d-md-block">
                        <p class="statistics-title">Total Trips</p>
                        <h3 class="rate-percentage">{{ $total_trips }}</h3>
                      </div>
                    </div>
                  </div>
                </div> --}}
                <div class="row">

                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                    <a href="{{ route('users.index') }}" style="text-decoration:none">
                        <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                            <div class="card bg-primary card-rounded">
                            <div class="card-body pb-0">
                                <h4 class="card-title card-title-dash text-white mb-4">Total Users</h4>
                                <div class="row">
                                <div class="col-sm-4">
                                    <h2 class="text-info">{{ $total_users }}</h2>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </a>
                    </div>
                  </div>
                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                    <a href="{{ route('users.index') }}" style="text-decoration:none">
                        <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                          <div class="card bg-primary card-rounded">
                            <div class="card-body pb-0">
                              <h4 class="card-title card-title-dash text-white mb-4">Total Clients</h4>
                              <div class="row">
                                <div class="col-sm-4">

                                  <h2 class="text-info">{{ $total_clients }}</h2>
                                </div>

                              </div>
                            </div>
                          </div>
                        </div>
                    </a>

                    </div>
                  </div>
                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                    <a href="{{ route('driver.index') }}" style="text-decoration:none">
                        <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                          <div class="card bg-primary card-rounded">
                            <div class="card-body pb-0">
                              <h4 class="card-title card-title-dash text-white mb-4">Total Chauffers</h4>
                              <div class="row">
                                <div class="col-sm-4">
                                  <h2 class="text-info">{{ $total_chauffers }}</h2>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </a>

                    </div>
                  </div>
                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                        <a href="{{ route('driver.requested') }}" style="text-decoration:none">
                            <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                              <div class="card bg-primary card-rounded">
                                <div class="card-body pb-0">
                                  <h4 class="card-title card-title-dash text-white mb-4">New Requests</h4>
                                  <div class="row">
                                    <div class="col-sm-4">

                                      <h2 class="text-info">{{ $total_requests }}</h2>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                        </a>

                    </div>
                  </div>
                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                    <a href="{{ route('payments.index') }}" style="text-decoration:none">
                        <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                          <div class="card bg-primary card-rounded">
                            <div class="card-body pb-0">
                              <h4 class="card-title card-title-dash text-white mb-4">Total Earning</h4>
                              <div class="row">
                                <div class="col-sm-4">
                                  <h2 class="text-info">{{ $total_earning }}</h2>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </a>

                    </div>
                  </div>
                  <div class="col-lg-4 d-flex flex-column">
                    <div class="row flex-grow">
                        <a href="{{ route('user-trips.index') }}" style="text-decoration:none">
                            <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                              <div class="card bg-primary card-rounded">
                                <div class="card-body pb-0">
                                  <h4 class="card-title card-title-dash text-white mb-4">Total Trips</h4>
                                  <div class="row">
                                    <div class="col-sm-4">

                                      <h2 class="text-info">{{ $total_trips }}</h2>
                                    </div>

                                  </div>
                                </div>
                              </div>
                            </div>
                        </a>

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 d-flex flex-column">
                    {{-- <div class="row flex-grow">
                      <div class="col-12 grid-margin stretch-card">
                        <div class="card card-rounded">
                          <div class="card-body">
                            <div class="d-sm-flex justify-content-between align-items-start">
                              <div>
                                <h4 class="card-title card-title-dash">Total Earning</h4>
                               <p class="card-subtitle card-subtitle-dash">Chauffers Earning</p>
                              </div>
                              <div>
                                <div class="dropdown">
                                  <button class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> This Year </button>
                                </div>
                              </div>
                            </div>
                            <div class="d-sm-flex align-items-center mt-1 justify-content-between">
                              <div class="d-sm-flex align-items-center mt-4 justify-content-between"><h2 class="me-2 fw-bold">{{ $month_earning }}</h2><h4 class="me-2">AED</h4><h4 class="text-success">(+1.37%)</h4></div>
                              <div class="me-3"><div id="marketing-overview-legend"></div></div>
                            </div>
                            <div class="chartjs-bar-wrapper mt-3">
                              <canvas id="marketingOverview"></canvas>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> --}}
                    <div class="row flex-grow">
                      <div class="col-12 grid-margin stretch-card">
                        <div class="card card-rounded">
                          <div class="card-body">
                            <div class="d-sm-flex justify-content-between align-items-start">
                              <div>
                                <h4 class="card-title card-title-dash">New Users</h4> <a href="{{url('/users')}}"> View all</a>
                               <p class="card-subtitle card-subtitle-dash">{{ $new_request_users }} new users in a month</p>
                              </div>
                              {{-- <div>
                                <a class="btn btn-primary btn-lg text-white mb-0 me-0" href="{{ route('users.create') }}">
                                    <i class="mdi mdi-account-plus"></i>
                                    New 
                                </a>
                              </div> --}}
                            </div>
                            <div class="table-responsive  mt-1">
                              <table class="table select-table">
                                <thead>
                                  <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($new_request as $request)
                                  <tr>
                                  <td>
                                    <div class="d-flex ">
                                      <img src="{{ asset($request->profile_image??'admin/images/faces/face1.jpg') }}" alt="">
                                      <div>
                                        <h6>{{ $request->first_name.' '.$request->last_name }}</h6>
                                      </div>
                                    </div>
                                  </td>
                                <td>
                                    <h6>{{ $request->email }}</h6>
                                </td>
                                <td>
                                    <h6>{{ $request->phone }}</h6>
                                </td>
                                <td>
                                    <h6>{{ $request->status }}</h6>
                                </td>
                                <td>
                                    @if ($request->type == 'USER')
                                        <a href="{{route('users.edit', $request->id)}}">Edit</a>
                                    @else
                                        <a href="{{route('driver.edit', $request->id)}}">Edit</a>

                                    @endif
                                </td>
                                </tr>
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
