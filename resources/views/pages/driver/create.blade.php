@extends('layouts.default')
@section('title', (isset($user)?'Update Driver':'Add Driver'))
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ isset($user)?'Update':'Add' }} Driver Form</h4>
                        <form class="forms-sample" action="{{ isset($user)?route('driver.update', $user->id):route('driver.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @isset($user)
                                @method('PUT')
                            @endisset
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" id="first_name" value="{{ old('first_name', $user->first_name??'') }}" placeholder="First Name">
                                @error('first_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" id="last_name" value="{{ old('last_name', $user->last_name??'') }}" placeholder="Last Name">
                                @error('last_name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" id="phone" value="{{ old('phone', $user->phone??'') }}" placeholder="Phone">
                                @error('phone')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email', $user->email??'') }}" placeholder="Email">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="emirates_id">Emirate Id</label>
                                <input type="number" class="form-control @error('emirates_id') is-invalid @enderror" name="emirates_id" id="emirates_id" value="{{ old('emirates_id', $user->emirates_id??'') }}" placeholder="Emirate Id">
                                @error('emirates_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="license_number">License Number</label>
                                <input type="number" class="form-control @error('license_number') is-invalid @enderror" name="license_number" id="license_number" value="{{ old('license_number', $user->license_number??'') }}" placeholder="Licese Number">
                                @error('license_number')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" id="password" value="{{ old('password') }}" placeholder="Password">
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="passport_pic">Passportsize Image</label>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control @error('passport_pic') is-invalid @enderror" name="passport_pic" id="passport_pic" placeholder="License Front">
                                @error('passport_pic')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="emirate_front">Emirate Id Front Image</label>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control @error('emirate_front') is-invalid @enderror" name="emirate_front" id="emirate_front" placeholder="License Front">
                                @error('emirate_front')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="emirate_back">Eimrate Id Back Image</label>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control @error('emirate_back') is-invalid @enderror" name="emirate_back" id="emirate_back" placeholder="License Front">
                                @error('emirate_back')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="license_front">License Front</label>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control @error('license_front') is-invalid @enderror" name="license_front" id="license_front" placeholder="License Front">
                                @error('license_front')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="license_back">License Back</label>
                                <input type="file" accept="image/png, image/jpg, image/jpeg" class="form-control @error('license_back') is-invalid @enderror" name="license_back" id="license_back" placeholder="License Front">
                                @error('license_back')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            {{-- <div class="form-group">
                                <label>Profile Picture</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="pro_pic" class="form-control file-upload-info"
                                        placeholder="Upload Image">
                                </div>
                            </div> --}}
                            {{-- <div class="form-group">
                                <label>License Front</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="license_front" class="form-control file-upload-info"
                                        placeholder="Upload Image">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>License Back</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="license_back" class="form-control file-upload-info"
                                        placeholder="Upload Image">
                                </div>
                            </div> --}}

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
