@extends('layouts.default')
@section('title', 'Driver List')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Driver List</h4>
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
@endsection
