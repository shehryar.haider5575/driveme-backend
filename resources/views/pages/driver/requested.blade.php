@extends('layouts.default')
@section('title', 'Driver List')
@section('content')
<div class="content-wrapper">
    <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Driver List</h4>
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>S.NO#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Refferal Code</th>
                    <th>Emirates Id</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse ($drivers as $key => $driver)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$driver->first_name.' '.$driver->last_name}}</td>
                        <td>{{$driver->email}}</td>
                        <td>{{$driver->phone}}</td>
                        <td>{{$driver->refferal_code}}</td>
                        <td>{{$driver->emirates_id}}</td>
                        <td>
                           <div class="d-flex">
                              <div class="dropdown">
                                <button class="btn {{$driver->is_admin_approve == 0? "btn-warning":"btn-danger"}} dropdown-toggle" type="button"
                                    id="dropdownMenuButton2" data-bs-toggle="dropdown"
                                    aria-expanded="false">
                                    {{$driver->is_admin_approve == 0? "Waiting":"Rejected"}}
                                </button>
                                <ul class="dropdown-menu dropdown-menu-dark"
                                    aria-labelledby="dropdownMenuButton2">
                                    <li><a href="{{ route('driver.accept', $driver->id) }}" class="dropdown-item">Accept</a></li>
                                    <li><a href="{{ route('driver.reject', $driver->id) }}" class="dropdown-item">Reject</a></li>
                                </ul>
                            </div>
                            <a href="{{ route('driver.requested.id', $driver->id) }}" class="btn btn-secondary"><i class="fa fa-eye"></i></a>
                           </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                      <th colspan="7" class="text-center text-danger">Record not found</th>
                    </tr>
                  @endforelse
                </tbody>
              </table>
              <div class="showing-entry">Showing {{($drivers->currentpage()-1)*$drivers->perpage()+1}} to {{$drivers->currentpage()*$drivers->perpage()}}
                  of  {{$drivers->total()}} entries
                  @if ($drivers->hasPages())
                      <div class="pagination-wrapper" style="float: right;">
                          {{ $drivers->links() }}
                      </div>
                  @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
