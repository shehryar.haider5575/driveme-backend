@extends('layouts.default')
@section('title', 'Add Faqs')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Faqs</h4>
                        <form class="forms-sample" method="post" action="{{route('faqs.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label for="question" class="col-sm-3 col-form-label">Question</label>
                                <div class="col-sm-9">
                                    <textarea cols="5" name="question" class="form-control" id="question"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="answer" class="col-sm-3 col-form-label">Answer</label>
                                <div class="col-sm-9">
                                    <textarea cols="5" name="answer" class="form-control" id="answer"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="answer" class="col-sm-3 col-form-label">Type</label>
                                <select name="type" class="form-control">
                                    <option value="USER">User</option>
                                    <option value="CAPTAIN">Captain</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
