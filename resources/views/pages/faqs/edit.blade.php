@extends('layouts.default')
@section('title', 'Edit Faqs')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Edit Faqs</h4>
                        <form class="forms-sample" method="post" action="{{route('faqs.update',$faq->id)}}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label for="question" class="col-sm-3 col-form-label">Question</label>
                                <div class="col-sm-9">
                                    <textarea cols="5" name="question" class="form-control" id="question">{{$faq->question}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="answer" class="col-sm-3 col-form-label">Answer</label>
                                <div class="col-sm-9">
                                    <textarea cols="5" name="answer" class="form-control" id="answer">{{$faq->answer}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="answer" class="col-sm-3 col-form-label">Type</label>
                                <select name="type" class="form-control">
                                    <option {{$faq->type == 'USER' ? 'selected' : null}} value="USER">User</option>
                                    <option {{$faq->type == 'CAPTAIN' ? 'selected' : null}} value="CAPTAIN">Captain</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                  <label class="form-check-label" for="status1">
                                    <input type="radio" class="form-check-input" name="status" id="status1" {{($faq->status)? 'checked' : ''}} value="1">
                                    Active
                                  <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check">
                                  <label class="form-check-label" for="status2">
                                    <input type="radio" class="form-check-input" name="status" id="status2" {{(!$faq->status)? 'checked' : ''}} value="0">
                                    Deactive
                                  <i class="input-helper"></i></label>
                                </div>
                              </div>
                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
