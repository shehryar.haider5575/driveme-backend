@extends('layouts.default')
@section('title', 'Faq List')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title card-title-btn">
                            Faq List
                            <div style="float: right;">
                                <a href="{{ route('faqs.create') }}" class="btn btn-primary btn-sm">Create Faqs</a>
                            </div>
                        </h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.NO#</th>
                                        <th>Question</th>
                                        <th>Answer</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($faqs as $key => $faq)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $faq->question }}</td>
                                            <td>{{ $faq->answer }}</td>
                                            <td>
                                                <form action="{{route('faqs.update', $faq->id)}}" class="d-flex" method="post" id="status">
                                                    @csrf
                                                    @method('PUT')

                                                    <select class="form-control" name="status" onchange="$('#status').submit()">
                                                        <option value="1" @selected($faq->status=='1'?true:false)>Active</option>
                                                        <option value="0" @selected($faq->status=='0'?true:false)>In-active</option>
                                                    </select>
                                                </form>
                                            </td>
                                            <td><a href="{{ route('faqs.edit', $faq->id) }}" class="btn btn-warning btn-xs">EDIT</a></td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <th colspan="5" class="text-center text-danger">Record not found</th>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            <div>Showing {{($faqs->currentpage()-1)*$faqs->perpage()+1}} to {{$faqs->currentpage()*$faqs->perpage()}}
                                of  {{$faqs->total()}} entries
                                @if ($faqs->hasPages())
                                    <div class="pagination-wrapper" style="float: right;">
                                        {{ $faqs->links() }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
