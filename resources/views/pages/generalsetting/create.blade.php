@extends('layouts.default')
@section('title', 'General Setting')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">General Setting</h4>
                        <form class="forms-sample" method="post" action="{{route('generalsetting.store')}}">
                            @csrf
                            {{-- <input type="hidden" name="exist" value="{{($generalSetting)? 1 : 0}}"> --}}
                            <div class="form-group row">
                                <label for="about" class="col-sm-3 col-form-label">About</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" name="key[about]" class="form-control" id="about">{{ $generalSetting['about']??'' }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="privacy-policy" class="col-sm-3 col-form-label">Privacy Policy</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" name="key[privacy-policy]" class="form-control" id="privacy-policy">{{ $generalSetting['privacy-policy']??'' }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="terms&condition" class="col-sm-3 col-form-label">Terms & Condition</label>
                                <div class="col-sm-9">
                                    <textarea cols="5" name="key[terms-condition]" class="form-control" id="terms&condition">{{ $generalSetting['terms&condition']??'' }}</textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price_per_point" class="col-sm-3 col-form-label">Price Per Point</label>
                                <div class="col-sm-9">
                                    <input type="number" step="any" min="0" name="key[price_per_point]" class="form-control" id="price_per_point" value="{{ $generalSetting['price_per_point']??0 }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pay_per_minute" class="col-sm-3 col-form-label">Pay Per Minute</label>
                                <div class="col-sm-9">
                                    <input type="number" step="any" min="0" name="key[pay_per_minute]" class="form-control" id="pay_per_minute" value="{{ $generalSetting['pay_per_minute']??0 }}" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pay_per_hour" class="col-sm-3 col-form-label">Pay Per Hour</label>
                                <div class="col-sm-9">
                                    <input type="number" step="any" min="0" name="key[pay_per_hour]" class="form-control" id="pay_per_hour" value="{{ $generalSetting['pay_per_hour']??0 }}" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="reward_on_reffer" class="col-sm-3 col-form-label">Reward Point on User Reffer</label>
                                <div class="col-sm-9">
                                    <input type="number" min="0" name="key[reward_on_reffer]" class="form-control" id="reward_on_reffer" value="{{ $generalSetting['reward_on_reffer']??0 }}" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rta_charges" class="col-sm-3 col-form-label">RTA Inspection Charges</label>
                                <div class="col-sm-9">
                                    <input type="number" min="0" name="key[rta_charges]" class="form-control" id="rta_charges" value="{{ $generalSetting['rta_charges']??0 }}" />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="garage_pickup" class="col-sm-3 col-form-label">Garage Pickup Charges</label>
                                <div class="col-sm-9">
                                    <input type="number" min="0" name="key[garage_pickup]" class="form-control" id="garage_pickup" value="{{ $generalSetting['garage_pickup']??0 }}" />
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
