<a href="{{ route('notification.read', $id) }}" class="btn btn-warning p-2 btn-xs m-1 rounded-top">Mark as Read</a>

<a href="{{ route('notification.show', $id) }}" class="btn btn-group-vertical btn-info btn-xs">Show</a>
