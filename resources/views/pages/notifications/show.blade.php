@extends('layouts.default')
@section('title', "View Detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Notification Detail</h4>
                        <div class="row mt-4">
                            <h5>User Info:</h5>
                            <div class="col-4">
                                @if (!isset($notification->user->profile_image))
                                <div class="img-thumb">
                                    <i class="mdi mdai-user"></i>
                                </div>
                                @else
                                <img src="{{ asset($notification->user->profile_image?? '') }}" />
                                @endif
                                <p>Name: {{ $notification->user->full_name??'' }}</p>
                            </div>
                        </div>
                        <div class="w-100 border"></div>
                            <div class="row mt-4">
                                <h5>Details:</h5>
                                <div class="col-12">
                                    <h5>Subject</h5>
                                    <p> {!! $notification->data['subject']??'' !!} </p>
                                </div>
                                <div class="col-12">
                                    <h5>Message</h5>
                                    <p> {!! $notification->data['message']??'' !!} </p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
