@extends('layouts.default')
@section('title', 'Payment History')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Filters:</h4>
                    <form action="{{ route('payments.index') }}" method="GET">
                        <div class="row">
                            @csrf
                            <div class="col m-2">
                                <label>Type</label>
                                <select class="form-control" name="type">
                                    <option></option>
                                    <option value="tip" @selected(app('request')->type == 'tip')>Tip</option>
                                    <option value="trip" @selected(app('request')->type == 'trip')>Trip</option>
                                    <option value="topup" @selected(app('request')->type == 'topup')>Topup</option>
                                    <option value="rental_car" @selected(app('request')->type == 'rental_car')>Rental Car</option>
                                </select>
                            </div>
                            <div class="col m-2">
                                <label>Payment Method</label>
                                <select class="form-control" name="pm">
                                    <option></option>
                                    <option value="stripe" @selected(app('request')->pm == 'stripe')>Card</option>
                                    <option value="paypal" @selected(app('request')->pm == 'paypal')>Paypal</option>
                                    <option value="wallet" @selected(app('request')->pm == 'wallet')>Wallet</option>
                                    <option value="bank" @selected(app('request')->pm == 'bank')>Bank</option>
                                    <option value="other" @selected(app('request')->pm == 'other')>Other</option>
                                </select>
                            </div>
                            <div class="col m-2">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Payment History</h4>
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
@endsection
