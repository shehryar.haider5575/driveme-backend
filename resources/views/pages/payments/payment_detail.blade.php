@extends('layouts.default')
@section('title', "View Payment Detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Payment Detail</h4>
                        <div class="row mt-4">
                            <h5>User Info:</h5>
                            <div class="col-4">
                                <div class="user-img">
                                    @if (is_null($payment->user->profile_image))
                                    <div class="img-thumb">
                                        <i class="mdi mdai-user"></i>
                                    </div>
                                    @else
                                    <img src="{{ asset($payment->user->profile_image?? '') }}" />
                                    @endif
                                </div>
                                <p><a href="{{ route('users.show', $payment->user->id) }}">{{ $payment->user->full_name??'' }}</a></p>
                            </div>
                            <div class="col-4">
                                <p>{{ $payment->user->email }}</p>
                            </div>
                            <div class="col-4">
                                <p>{{ $payment->user->phone }}</p>
                            </div>
                        </div>
                        <div class="w-100 border"></div>
                        @if ($payment->type == 'trip')
                            <div class="row mt-4">
                                <h5>Trip Details</h5>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-4">
                                            Trip Id: <a href="{{ route('user-trips.show', $payment->trip->id) }}">#000{{$payment->trip->id}}</a>
                                        </div>
                                        <div class="col-4">
                                            Trip Type: {{ ucfirst(str_ireplace('_', ' ', $payment->trip->trip_type)) }}
                                        </div>
                                        <div class="col-4">
                                            Trip Date: {{$payment->trip->schedule_datetime??$payment->trip->created_at }}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Pickup Address: {{$payment->trip->pickup_address}}
                                        </div>
                                        <div class="col-4">
                                            Dropoff Address: {{$payment->trip->dropoff_address}}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Driver: <a href="{{ route('driver.requested.id', $payment->trip->driver->id) }}">{{$payment->trip->driver->full_name}}</a>
                                        </div>
                                        <div class="col-4">
                                            Driver Status: {{$payment->trip->driver_status}}
                                        </div>
                                        <div class="col-4">
                                            Booking Type: {{$payment->trip->booking_type == 'pay_per_minute'?'Pay Per Minute':'Pay per Hour' }}
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-4">
                                            Payment: {{ $payment->trip->amount }} AED
                                        </div>
                                        <div class="col-4">
                                            Payment Status: {{$payment->trip->payment_status}}
                                        </div>
                                        <div class="col-12 mt-2">
                                            Instruction:
                                            <p>{!! $payment->trip->instruction !!}</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endif

                        @if ($payment->type == 'topup')
                        <div class="row mt-4">
                            <h5>Topups Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Amount: {{ $payment->amount }}
                                    </div>
                                    <div class="col-4">
                                        @php
                                            $old_amount = (float)$payment->user->total_balance - (float)$payment->amount;
                                        @endphp
                                        Old Balance: {{ $old_amount < 0?0:$old_amount }}
                                    </div>
                                    <div class="col-4">
                                        Total Balance: {{ $payment->user->total_balance }}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Payment: {{ $payment->amount }} AED
                                    </div>
                                    <div class="col-4">
                                        Payment By: {{ $payment->payment_type }}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Date:
                                        <p>{!! $payment->created_at !!}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif

                        @if ($payment->type == 'tip')
                        <div class="row mt-4">
                            <h5>Tip Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Amount: {{ $payment->amount }}
                                    </div>
                                    <div class="col-4">
                                        Chauffer Name: <a href="{{ route('driver.requested.id', $payment->driver->id) }}">{{$payment->driver->full_name}}</a>
                                    </div>
                                    <div class="col-4">
                                        Total Balance: {{ $payment->driver->total_balance }}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Payment: {{ $payment->amount }} AED
                                    </div>
                                    <div class="col-4">
                                        Payment By: {{ $payment->payment_type }}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Date:
                                        <p>{!! $payment->created_at !!}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endif

                        @if ($payment->type == 'other')
                        <div class="row mt-4">
                            <h5>Other Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Amount: {{ $payment->amount }}
                                    </div>
                                    <div class="col-4">
                                        Payment By: {{ $payment->payment_type }}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Date:
                                        <p>{!! $payment->created_at !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <div class="row mt-4">
                            <h5>Discount Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Promocode: {{ $payment->promocode??'N/A' }}
                                    </div>
                                    <div class="col-4">
                                        Discount Type: {{ $payment->discount_type??'' }}
                                    </div>
                                    <div class="col-4">
                                        Discount: {{ $payment->discount_value??0.00 }}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Date:
                                        <p>{!! $payment->created_at !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
