@extends('layouts.default')
@section('title', 'Add Promocodes')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Promocodes</h4>
                        <form class="forms-sample" action="{{route('promocodes.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}" name="title" id="title" placeholder="Title">
                                @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="Description">{!! old('description') !!}</textarea>
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="promocode">Promocode</label>
                                <input type="text" class="form-control @error('promocode') is-invalid @enderror" value="{{old('promocode')}}" name="promocode" id="promocode" placeholder="Promocode">
                                @error('promocode')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="discount_type">Disscount Type</label>
                                <select name="discount_type" class="form-control @error('discount_type') is-invalid @enderror" id="discount_type">
                                    <option value="percent" @selected(old('discount_type') == 'percent')>Percent</option>
                                    <option value="fixed" @selected(old('discount_type') == 'fixed')>Fixed</option>
                                </select>
                                @error('discount_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="discount_value">Disscount Value</label>
                                <input type="number" class="form-control @error('discount_value') is-invalid @enderror" value="{{old('discount_value')}}" name="discount_value" id="discount_value" placeholder="Disscount Value">
                                @error('discount_value')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="users">Users</label>
                                <select id="users" name="users[]" class="form-control select2 @error('users') is-invalid @enderror" multiple>
                                    <option value="all" @selected(old('users', $promocode->user_id??'')=='all')>All Users</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @selected(old('users', $promocode->user_id??'')==$user->id)>{{ $user->first_name.' '.$user->last_name }}</option>
                                    @endforeach
                                </select>
                                @error('users')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="started_at">Started At</label>
                                <input type="datetime-local" class="form-control @error('started_at') is-invalid @enderror" value="{{old('started_at')}}" name="started_at" id="started_at" placeholder="Started At">
                                @error('started_at')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="expired_at">Expired At</label>
                                <input type="datetime-local" class="form-control @error('expired_at') is-invalid @enderror" value="{{old('expired_at')}}" name="expired_at" id="expired_at" placeholder="Expired At">
                                @error('expired_at')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
