<a href="{{ route('promocodes.edit', $id) }}" class="btn btn-warning btn-xs">edit</a>
<form action="{{ route('promocodes.destroy', $id) }}">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-warning btn-xs"><i class="fa fa-trash"></i> Delete</button>
</form>
