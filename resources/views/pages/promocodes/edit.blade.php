@extends('layouts.default')
@section('title', 'Add Promocodes')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Promocodes</h4>
                        <form class="forms-sample" action="{{route('promocodes.update', $promo->id)}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" value="{{old('title', $promo->title)}}" name="title" id="title" placeholder="Title">
                                @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control @error('description') is-invalid @enderror" name="description" id="description" placeholder="Description">
                                    {!! old('description', $promo->description) !!}
                                </textarea>
                                @error('description')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="promocode">Promocode</label>
                                <input type="hidden" value="{{old('promocode', $promo->promocode)}}" name="promocode" id="promocode" placeholder="Promocode" readonly>
                                <p>{{ $promo->promocode }}</p>
                                @error('promocode')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="discount_type">Disscount Type</label>
                                <input type="hidden" value="{{ $promo->discount_type }}" name="discount_type" />
                                <p>{{ $promo->discount_type }}</p>

                                @error('discount_type')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="discount_value">Disscount Value</label>
                                <input type="number" class="form-control @error('discount_value') is-invalid @enderror" value="{{old('discount_value', $promo->discount_value)}}" name="discount_value" id="discount_value" placeholder="Disscount Value">
                                @error('discount_value')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="started_at">Started At</label>
                                <input type="datetime-local" class="form-control @error('started_at') is-invalid @enderror" value="{{old('started_at', $promo->started_at)}}" name="started_at" id="started_at" placeholder="Started At">
                                @error('started_at')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="expired_at">Expired At</label>
                                <input type="datetime-local" class="form-control @error('expired_at') is-invalid @enderror" value="{{old('expired_at', $promo->expired_at)}}" name="expired_at" id="expired_at" placeholder="Expired At">
                                @error('expired_at')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <input type="radio" value="active" @checked(old('status', $promo->status) == 'active') name="status" id="active" />
                                <label for="active">Active</label>

                                <input type="radio" value="inactive" @checked(old('status', $promo->status) == 'inactive') name="status" id="in-active" />
                                <label for="in-active">In-Active</label>

                                @error('status')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
