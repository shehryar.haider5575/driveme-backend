<a href="{{ route('rental-cars.show', $id) }}" class="btn btn-warning"><i class="fa fa-eye"></i></a>
<a href="{{ route('rental-cars.edit', $id) }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
<form action="{{ route('rental-cars.destroy', $id) }}" method="post">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
</form>
