@extends('layouts.default')
@section('title', isset($car)?'Edit ':'Add '.'Car')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ isset($car)?'Edit ':'Add ' }}Car</h4>
                        <form class="forms-sample" action="{{ isset($car)?route('rental-cars.update', $car->id):route('rental-cars.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @isset($car)
                                @method('PUT')
                            @endisset
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name', $car->name??'') }}">
                            </div>
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="model">Model</label>
                                <input type="text" class="form-control" name="model" id="model" placeholder="Model" value="{{ old('model', $car->model??'') }}">
                            </div>
                            @error('model')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="year">Year</label>
                                <input type="number" class="form-control" name="year" id="year" placeholder="Year" value="{{ old('year', $car->year??'') }}">
                            </div>
                            @error('year')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="color_code">Color</label>
                                <input type="color" class="form-control" name="color_code" id="color_code" placeholder="Color" value="{{ old('color_code', $car->color_code??'') }}">
                            </div>
                            @error('color_code')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="price_per_day">Price</label>
                                <input type="number" class="form-control" name="price_per_day" step="any" id="price_per_day" placeholder="Price per Day" value="{{ old('price_per_day', $car->price_per_day??'') }}">
                            </div>
                            @error('price_per_day')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="passengers">No. Passengers</label>
                                <input type="number" class="form-control" id="passengers" name="passengers" placeholder="Number of Passengers" value="{{ old('passengers', $car->passengers??'') }}">
                            </div>
                            @error('passengers')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="doors">No. Doors</label>
                                <input type="number" class="form-control" id="doors" name="doors" placeholder="Number of Doors" value="{{ old('doors', $car->doors??'') }}">
                            </div>
                            @error('doors')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="horse_power">Horse Power</label>
                                <input type="number" class="form-control" id="horse_power" name="horse_power" placeholder="Horse Power" value="{{ old('horse_power', $car->horse_power??'') }}">
                            </div>
                            @error('horse_power')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="speed">Speed KM/H</label>
                                <input type="number" class="form-control" id="speed" name="speed" placeholder="Speed KM/H" value="{{ old('speed', $car->speed??'') }}">
                            </div>
                            @error('speed')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                            <div class="form-group">
                                <label for="fuel_kit">Fuel KIT</label>
                                <select class="form-control" name="fuel_kit" id="fuel_kit" required>
                                    <option value=""></option>
                                    <option value="petrol" @selected(old('fuel_kit', $car->fuel_kit??'')=='petrol')>Petrol</option>
                                    <option value="gas" @selected(old('fuel_kit', $car->fuel_kit??'')=='gas')>GAS</option>
                                </select>
                            </div>
                            @error('fuel_kit')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="transmission_type">Transmission Type</label>
                                <select class="form-control" name="transmission_type" id="transmission_type" required>
                                    <option value=""></option>
                                    <option value="automatic" @selected(old('transmission_type', $car->transmission_type??'')=='automatic')>Automatic</option>
                                    <option value="manual" @selected(old('transmission_type', $car->transmission_type??'')=='manual')>Manual</option>
                                </select>
                            </div>
                            @error('transmission_type')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label>Car Images</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="files[]" class="form-control file-upload-info" placeholder="Upload Image" multiple>
                                </div>
                            </div>
                            @error('files')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" cols="12" rows="12" id="description" class="form-control">{{ old('description', $car->description??'') }}</textarea>
                            </div>
                            @error('description')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                            {{-- <div class="form-group">
                                <label>License Front</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="license_front" class="form-control file-upload-info"
                                        placeholder="Upload Image">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>License Back</label>
                                <div class="input-group col-xs-12">
                                    <input type="file" name="license_back" class="form-control file-upload-info"
                                        placeholder="Upload Image">
                                </div>
                            </div> --}}

                            <button type="submit" class="btn btn-primary me-2">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
