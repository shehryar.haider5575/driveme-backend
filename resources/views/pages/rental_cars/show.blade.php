@extends('layouts.default')
@section('title', "View Rental Car Detail")
@section('content')
@push('css_lib')
<style>
    table td{
        background: white;
    }
    .carousel-item img {
        height: 450px !important;
        object-fit: cover;
        object-position: center;
    }
    .carousel-item:after {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        background: rgba(0,0,0,0.3);
    }
    .carousel-caption {
        z-index: 1;
    }
</style>
@endpush
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ $car->name }}</h4>
                        <div class="card">
                            <div class="card-body">
                                <div id="carouselExampleIndicators" class="carousel slide carousel-fade carousel-top hoverable" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        @foreach ($car->attachments as $index => $attachment)
                                        <div class="carousel-item {{ $index==0?'active':'' }}">
                                          <img class="d-block w-100" src="{{ asset($attachment->file_path) }}" alt="{{$index}} slide">
                                          <div class="carousel-caption d-none d-md-block">
                                            <h5>#{{$index+1}} Picture</h5>
                                            {{-- <p>Hello</p> --}}
                                            <form action="{{ route('delete_attachment', $attachment->id) }}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger">Delete Image</button>
                                            </form>
                                          </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @if (count($car->attachments) > 1)
                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                        </a>
                                    @endif
                                  </div>
                            </div>
                        </div>
                        <table class="mt-3 table table-bordered table-secondary">
                            <thead>
                                <tr>
                                    <th>Card Id</th>
                                    <td>{{ $car->id }}</td>
                                </tr>
                                <tr>
                                    <th>Card Name</th>
                                    <td>{{ $car->name }}</td>
                                </tr>
                                <tr>
                                    <th>Card Model</th>
                                    <td>{{ $car->model }}</td>
                                </tr>
                                <tr>
                                    <th>Year</th>
                                    <td>{{ $car->year }}</td>
                                </tr>
                                <tr>
                                    <th>Color</th>
                                    <td>{{ $car->color.' '.$car->color_code }}</td>
                                </tr>
                                <tr>
                                    <th>Price per Day</th>
                                    <td>{{ $car->price_per_day }}</td>
                                </tr>
                                <tr>
                                    <th>No. Passengers</th>
                                    <td>{{ $car->name }}</td>
                                </tr>
                                <tr>
                                    <th>No. Doors</th>
                                    <td>{{ $car->doors }}</td>
                                </tr>
                                <tr>
                                    <th>Horse Power</th>
                                    <td>{{ $car->horse_power }}</td>
                                </tr>
                                <tr>
                                    <th>Speed</th>
                                    <td>{{ $car->speed }} KM/H</td>
                                </tr>
                                <tr>
                                    <th>Fuel Kit</th>
                                    <td>{{ $car->fuel_kit }}</td>
                                </tr>
                                <tr>
                                    <th>Transmission Type</th>
                                    <td>{{ $car->transmission_type }}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{!! $car->description !!}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@push('scripts')
<script>
    $('.carousel').carousel()
</script>
@endpush
@endsection
