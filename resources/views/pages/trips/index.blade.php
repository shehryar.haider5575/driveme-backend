@extends('layouts.default')
@section('title', 'Booking List')
@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Filters:</h4>
                <form action="{{ route('user-trips.index') }}" method="GET">
                    @csrf
                    <div class="row">
                        <div class="col m-2">
                            <label>Trip Type</label>
                            <select class="form-control" name="trip_type">
                                <option></option>
                                <option value="ride_now" @selected(app('request')->trip_type == 'ride_now')>Ride Now</option>
                                <option value="garage_pickup" @selected(app('request')->trip_type == 'garage_pickup')>Garage Pickup</option>
                                <option value="rta_inspection" @selected(app('request')->trip_type == 'rta_inspection')>Rta Inspection</option>
                                <option value="inter_city" @selected(app('request')->trip_type == 'inter_city')>Inter City</option>
                            </select>
                        </div>
                        <!--<div class="col m-2">
                            <label>Booking Status</label>
                            <select class="form-control" name="booking_type">
                                <option></option>
                                <option value="one_time" @selected(app('request')->booking_type == 'one_time')>One Time</option>
                                <option value="pay_per_hour" @selected(app('request')->booking_type == 'pay_per_hour')>Pay Per Hour</option>
                                <option value="pay_per_minute" @selected(app('request')->booking_type == 'pay_per_minute')>Pay Per Minute</option>
                            </select>
                        </div>
                        <div class="col m-2">
                            <label>Chauffer Status</label>
                            <select class="form-control" name="ds">
                                <option></option>
                                <option value="accept" @selected(app('request')->ds == 'accept')>Accept</option>
                                <option value="complete" @selected(app('request')->ds == 'complete')>Complete</option>
                                <option value="confirm_arrival" @selected(app('request')->ds == 'confirm_arrival')>Confirm Arival</option>
                                <option value="pending" @selected(app('request')->ds == 'pending')>Pending</option>
                                <option value="reject" @selected(app('request')->ds == 'reject')>Reject</option>
                                <option value="start_drive" @selected(app('request')->ds == 'start_drive')>Start Drive</option>
                                <option value="end_drive" @selected(app('request')->ds == 'end_drive')>End Drive</option>
                            </select>
                        </div> -->
                        <div class="col m-4">
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </form>
              </div>
            </div>
        </div>

      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Booking List</h4>
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @push('scripts')
    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}
@endpush
@endsection
