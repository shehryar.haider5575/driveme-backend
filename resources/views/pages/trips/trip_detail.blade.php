@extends('layouts.default')
@section('title', "View Trip Detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Trip Detail</h4>
                        <div class="row mt-4">
                            <h5>User Info:</h5>
                            <div class="col-4">
                                <div class="user-img">
                                    @if (is_null($trip->user->profile_image))
                                    <div class="img-thumb">
                                        <i class="mdi mdai-user"></i>
                                    </div>
                                    @else
                                    <img src="{{ asset($trip->user->profile_image?? '') }}" />
                                    @endif
                                </div>
                                <p><a href="{{ route('users.show', $trip->user->id) }}">{{ $trip->user->full_name }}</a></p>
                            </div>
                            <div class="col-4">
                                <p>{{ $trip->user->email }}</p>
                            </div>
                            <div class="col-4">
                                <p>{{ $trip->user->phone }}</p>
                            </div>
                        </div>
                        <div class="w-100 border"></div>
                        <div class="row mt-4">
                            <h5>Trip Details</h5>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        Trip Id: #000{{$trip->id}}
                                    </div>
                                    <div class="col-4">
                                        Trip Type: {{ ucfirst(str_ireplace('_', ' ', $trip->trip_type)) }}
                                    </div>
                                    <div class="col-4">
                                        Trip Date: {{$trip->schedule_datetime??$trip->created_at }}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Pickup Address: {{$trip->pickup_address}}
                                    </div>
                                    <div class="col-4">
                                        Dropoff Address: {{$trip->dropoff_address}}
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Driver: {!! !is_null($trip->driver)?'<a href="'.route("driver.requested.id", $trip->driver->id).'">'.$trip->driver->full_name.'</a>':'' !!}
                                    </div>
                                    <div class="col-4">
                                        Trip Status: {{$trip->driver_status}}
                                    </div>
                                    <div class="col-4">
                                        Booking Type:
                                        @if ($trip->trip_type == 'garage_pickup' || $trip->trip_type == 'rta_inspection')
                                            One Time
                                        @else
                                            {{$trip->booking_type == 'pay_per_minute'?'Pay Per Minute':'Pay per Hour' }}
                                        @endif
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-4">
                                        Payment: {{ $trip->amount }} AED
                                    </div>
                                    <div class="col-4">
                                        Payment Status: {{$trip->payment_status}}
                                    </div>
                                    <div class="col-12 mt-2">
                                        Instruction:
                                        <p>{!! $trip->instruction !!}</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
