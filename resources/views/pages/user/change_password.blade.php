@extends('layouts.default')
@section('title', "Update Password")
@section('content')
    <div class="content-wrapper">
        <section style="background-color: #eee;">
            <div class="container py-5">
                @if (Session::has('success'))
                    <div class="alert alert-success" style="margin-bottom: 1%" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif
                @if (Session::has('anyError'))
                    <div class="alert alert-danger" style="margin-bottom: 1%" role="alert">
                        {{ Session::get('anyError') }}
                    </div>
                @endif
              <form accept="{{ route('update-change-password') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="col-lg-4">
                  <div class="card mb-4">
                    <div class="card-body text-center">
                      <img src="{{ asset(Auth::user()->profile_image??'admin/images/faces/face8.jpg') }}" alt="avatar" class="rounded-circle img-fluid" style="width: 150px; cursor: pointer;">
                      <h5 class="my-3">{{ Auth::user()->first_name??'' .' '. Auth::user()->last_name??'' }}</h5>
                      <p class="text-muted mb-1">Role: {{ Auth::user()->type }}</p>
                      <p class="text-muted mb-4">Address: {{Auth::user()->address??'N/A'}}</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="card mb-4">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Current Password</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="current_password" type="password" class="text-muted mb-0 form-control" >
                            @error('current_password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">New Password</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="password" type="password" class="text-muted mb-0 form-control" >
                            @error('password')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Confirm Password</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="password_confirmation" type="password" class="text-muted mb-0 form-control" >
                            @error('password_confirmation')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>

                      <div class="row mt-3" style="float: right;">
                        <div class="col-sm-12">
                            <a class="btn btn-danger btn-sm" href="{{ route('dashboard') }}">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">Save Changes</button>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                </div>
              </form>
            </div>
          </section>
    </div>
@endsection
