<form id="status" action="{{route('users.status',$id)}}" class="d-flex" method="post">
    @csrf
    @method('PUT')
    <select class="form-control" name="status" onchange="$('#status').submit()">
        <option value="1" @selected($status=='1'?true:false)>Active</option>
        <option value="0" @selected($status=='0'?true:false)>Inactive</option>
    </select>
</form>
