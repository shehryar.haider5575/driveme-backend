@extends('layouts.default')
@section('title', "View Profile")
@section('content')
<style>
.overlay {
    position: absolute;
    left: 35%;
    top: 40%;
    background-color: #46464647;
    width: 122px;
    height: 40px;
    border-bottom-left-radius: 65px;
    border-bottom-right-radius: 65px;
    color: white;
    text-align: center;
    align-items: center;
    display: flex;
    /* align-content: center; */
    justify-content: center;
    transition: ease 1s;
    font-size: 35px;
    font-weight: bolder;
}
.overlay button{
    font-weight: bolder;
    font-size: 15px;
    cursor: pointer;
    background-color: #46464647;
    border: none;
    color: white;
    border-bottom-left-radius: 65px;
    border-bottom-right-radius: 65px;
}
</style>
    <div class="content-wrapper">
        <section style="background-color: #eee;">
            <div class="container py-5">
                @if (Session::has('success'))
                    <div class="alert alert-success" style="margin-bottom: 1%" role="alert">
                        {{ Session::get('success') }}
                    </div>
                @endif
              <form accept="{{ route('update-profile') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="col-lg-4">
                  <div class="card mb-4">
                    <div class="card-body text-center">
                      <img src="{{ asset(Auth::user()->profile_image??'admin/images/faces/face8.jpg') }}" alt="avatar" class="rounded-circle img-fluid" style="width: 150px; cursor: pointer;">
                      <div class="overlay" >
                          <button id="profileImage">Upload Image</button>
                        <div>
                            <input type="file" name="imageFile" class="d-none" id="imgFile" accept="image/*"/>
                        </div>
                      </div>
                      <h5 class="my-3">{{ Auth::user()->first_name??'' .' '. Auth::user()->last_name??'' }}</h5>
                      <p class="text-muted mb-1">Role: {{ Auth::user()->type }}</p>
                      <p class="text-muted mb-4">Address: {{Auth::user()->address??'N/A'}}</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="card mb-4">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">First Name</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="first_name" type="text" class="text-muted mb-0 form-control" value="{{ old('first_name', Auth::user()->first_name) }}">
                            @error('first_name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Last Name</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="last_name" type="text" class="text-muted mb-0 form-control" value="{{ old('last_name', Auth::user()->last_name) }}">
                            @error('last_name')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Email</p>
                        </div>
                        <div class="col-sm-10">
                            <input name="email" type="email" class="text-muted mb-0 form-control" value="{{ old('email', Auth::user()->email) }}">
                            @error('email')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Phone</p>
                        </div>
                        <div class="col-sm-10">
                            <input type="number" name="phone" class="text-muted mb-0 form-control" value="{{ old('phone', Auth::user()->phone) }}">
                            @error('phone')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">Gender</p>
                        </div>
                        <div class="col-sm-10">
                            <select name="gender" class="form-control">
                                <option value=""></option>
                                <option value="male" @selected(old('gender', Auth::user()->gender)=='male')>Male</option>
                                <option value="female" @selected(old('gender', Auth::user()->gender)=='female')>Fe-male</option>
                                <option value="others" @selected(old('gender', Auth::user()->gender)=='others')>Other</option>
                            </select>
                            @error('gender')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-sm-2">
                          <p class="mb-0">BIO</p>
                        </div>
                        <div class="col-sm-10">
                            <textarea name="bio" cols="12" rows="15" class="form-control">{!! Auth::user()->bio??'' !!}</textarea>
                            @error('bio')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                      </div>

                      <div class="row mt-3" style="float: right;">
                        <div class="col-sm-12">
                            <a class="btn btn-danger btn-sm" href="{{ route('dashboard') }}">Cancel</a>
                            <button type="submit" class="btn btn-primary btn-sm">Save Changes</button>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                </div>
              </form>
            </div>
          </section>
    </div>
@push('scripts')
<script>
    $('#profileImage').click((e)=>{
        e.preventDefault();
        $('#imgFile').click();
    })
</script>
@endpush
@endsection
