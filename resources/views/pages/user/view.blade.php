@extends('layouts.default')
@section('title', "View $user->first_name detail")
@section('content')
    <div class="content-wrapper">
        <div class="row">
            {{-- <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">User Bank Detail</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.NO#</th>
                                        <th>Bank name</th>
                                        <th>Account Holder Name</th>
                                        <th>Account NO</th>
                                        <th>IBAN NO</th>
                                        <th>Bank Country</th>
                                        <th>Address</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user->bank_details as $key => $bank_detail)
                                        @if ($bank_detail->type != 'card')
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $bank_detail->bank_name }}</td>
                                                <td>{{ $bank_detail->account_holder_name }}</td>
                                                <td>{{ $bank_detail->account_number }}</td>
                                                <td>{{ $bank_detail->iban_number }}</td>
                                                <td>{{ $bank_detail->bank_country }}</td>
                                                <td>{{ $bank_detail->address }}</td>
                                            </tr>
                                        @endif
                                    @if(!$user->bank_details()->where('type', '<>', 'card')->count())
                                        <tr>
                                            <th colspan="7" class="text-center text-danger">Record not found</th>
                                        </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> --}}
            @if ($user->type == 'CAPTAIN')
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered table-secondary">
                            <thead>
                                <tr>
                                    <th>First Name:</th>
                                    <td>{{ $user->first_name??'' }}</td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td>{{ $user->last_name??'' }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                                <tr>
                                    <th>DOB</th>
                                    <td>{{ $user->dob }}</td>
                                </tr>
                                <tr>
                                    <th>Emirate ID</th>
                                    <td>{{ $user->emirates_id }}</td>
                                </tr>
                                <tr>
                                    <th>License Number</th>
                                    <td>{{ $user->license_number }}</td>
                                </tr>
                                <tr>
                                    <th>Current Status</th>
                                    <td>{{ ucfirst($user->current_status) }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                    @if (isset($driver))
                                    <div class="dropdown">
                                        <button class="btn {{$user->is_admin_approve == 0? "btn-warning":"btn-danger"}} dropdown-toggle" type="button"
                                            id="dropdownMenuButton2" data-bs-toggle="dropdown"
                                            aria-expanded="false">
                                            {{ (($user->is_admin_approve == 0)? "Waiting":(($user->is_admin_approve==3)?"Rejected":"Accepted")) }}
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-dark"
                                            aria-labelledby="dropdownMenuButton2">
                                            <li><a href="{{ route('driver.accept', $user->id) }}" class="dropdown-item">Accept</a></li>
                                            <li><a href="{{ route('driver.reject', $user->id) }}" class="dropdown-item">Reject</a></li>
                                        </ul>
                                    </div>
                                    @else
                                    <form action="{{route('users.status', $user->id)}}" class="d-flex" method="post" id="status">
                                        @csrf
                                        @method('PUT')
                                        <select class="form-control" name="status" onchange="$('#status').submit()">
                                            <option value="1" @selected($user->status=='1'?true:false)>Active</option>
                                            <option value="0" @selected($user->status=='0'?true:false)>Inactive</option>
                                        </select>
                                    </form>
                                    @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Gender</th>
                                    <td>{{ $user->gender }}</td>
                                </tr>
                                <tr>
                                    <th>City</th>
                                    <td>{{ $user->city }}</td>
                                </tr>
                                <tr>
                                    <th>Disability</th>
                                    <td>{{ $user->disability?'Yes':'Not Any' }}</td>
                                </tr>
                                <tr>
                                    <th>Similar App</th>
                                    <td>{{ $user->similar_app?'Yes':'No' }}</td>
                                </tr>
                                <tr>
                                    <th>Vendor App</th>
                                    <td>{{ $user->vendor_app?'Yes':'No' }}</td>
                                </tr>
                                <tr>
                                    <th>BIO</th>
                                    <td>{!! $user->bio !!}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            @endif
            @if ($user->type == 'USER')
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">User Card Detail</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>S.NO#</th>
                                            <th>Card Holder Name</th>
                                            <th>Card NO</th>
                                            <th>Address</th>
                                            <th>Country</th>
                                            <th>Expiry Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($user->bank_details as $key => $bank_detail)
                                            @if ($bank_detail->type == 'card')
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td>{{ $bank_detail->account_holder_name }}</td>
                                                    <td>{{ $bank_detail->account_number }}</td>
                                                    <td>{{ $bank_detail->address }}</td>
                                                    <td>{{ $bank_detail->bank_country }}</td>
                                                    <td>{{ $bank_detail->expiry_date }}</td>
                                                </tr>
                                            @endif
                                        @if(!$user->bank_details()->where('type', 'card')->count())
                                            <tr>
                                                <th colspan="7" class="text-center text-danger">Record not found</th>
                                            </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">User Trip Detail</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>S.NO#</th>
                                            <th>Driver</th>
                                            <th>Car</th>
                                            <th>Pickup</th>
                                            <th>Dropoff</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($user->trips as $key => $trip)
                                            <tr>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $trip->driver->first_name??'' }}</td>
                                                <td>{{ $trip->car->name }}</td>
                                                <td>{{ $trip->pickup_address }}</td>
                                                <td>{{ $trip->dropoff_address }}</td>
                                                <td>{{ $trip->amount }}</td>
                                                <td>{{ $trip->status }}</td>
                                                <td>
                                                    <a class="text-center text-decoration-none d-grid" href="{{ route('user_trip_detail', [$user->id, $trip->id]) }}">
                                                        <i class="mdi mdi-eye-outline mdi-24px"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <th colspan="7" class="text-center text-danger">Record not found</th>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if ($user->type == 'CAPTAIN')
                <div class="card">
                    <div class="card-title">
                        <h5>Attachments</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6 p-4 border">
                                <h4>Driver Profile Picture</h4>
                                @empty($user->profile_image)
                                <p>No Image Provided</p>
                                @else
                                <a href="{{ asset($user->profile_image) }}"><img style="max-height: 120px;" src="{{ asset($user->profile_image) }}" /></a>
                                @endempty
                            </div>
                            {{-- <div class="col-6 p-4 border">
                                <h4>Driver Passportsize Picture</h4>
                                @empty($user->passportsize_pic)
                                <p>No Image Provided</p>
                                @else
                                <a href="{{ asset($user->passportsize_pic) }}"><img style="max-height: 120px;" src="{{ asset($user->passportsize_pic) }}" /></a>
                                @endempty
                            </div> --}}

                            <div class="col-6 p-4 border">
                                <h4>Driver Licence (Front)</h4>
                                @empty($user->driver_license_front_image_website)
                                <a href="{{ asset($user->license_front_image) }}"><img style="max-height: 120px;" src="{{ asset($user->license_front_image) }}" /></a>
                                @else
                                <a href="{{ asset($user->driver_license_front_image_website) }}"><img style="max-height: 120px;" src="{{ asset($user->driver_license_front_image_website) }}" /></a>
                                @endempty
                            </div>
                            <div class="col-6 p-4 border">
                                <h4>Driver License (Back)</h4>
                                @empty($user->driver_license_back_image_website)
                                <a href="{{ asset($user->license_back_image) }}"><img style="max-height: 120px;" src="{{ asset($user->license_back_image) }}" /></a>
                                @else
                                <a href="{{ asset($user->driver_license_back_image_website) }}"><img style="max-height: 120px;" src="{{ asset($user->driver_license_back_image_website) }}" /></a>
                                @endempty
                            </div>

                            <div class="col-6 p-4 border">
                                <h4>Emirate Id (Front)</h4>
                                @empty($user->driver_emirate_front_image_website)
                                <a href="{{ asset($user->emirate_front) }}"><img style="max-height: 120px;" src="{{ asset($user->emirate_front) }}" /></a>
                                @else
                                <a href="{{ asset($user->driver_emirate_front_image_website) }}"><img style="max-height: 120px;" src="{{ asset($user->driver_emirate_front_image_website) }}" /></a>
                                @endempty
                            </div>

                            <div class="col-6 p-4 border">
                                <h4>Emirate Id (Back)</h4>
                                @empty($user->driver_emirate_back_image_website)
                                <a href="{{ asset($user->emirate_back) }}"><img style="max-height: 120px;" src="{{ asset($user->emirate_back) }}" /></a>
                                @else
                                <a href="{{ asset($user->driver_emirate_back_image_website) }}"><img style="max-height: 120px;" src="{{ asset($user->driver_emirate_back_image_website) }}" /></a>
                                @endempty
                            </div>

                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
