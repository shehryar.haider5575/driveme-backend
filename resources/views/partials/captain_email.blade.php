<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"><head>
    <!--[if gte mso 9]>
  <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
  </xml>
  <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title>Login Credentials </title>

    <style type="text/css">
      @media only screen and (min-width: 620px) {
        .u-row {
          width: 600px !important;
        }
        .u-row .u-col {
          vertical-align: top;
        }
        .u-row .u-col-100 {
          width: 600px !important;
        }
      }

      @media (max-width: 620px) {
        .u-row-container {
          max-width: 100% !important;
          padding-left: 0px !important;
          padding-right: 0px !important;
        }
        .u-row .u-col {
          min-width: 320px !important;
          max-width: 100% !important;
          display: block !important;
        }
        .u-row {
          width: 100% !important;
        }
        .u-col {
          width: 100% !important;
        }
        .u-col>div {
          margin: 0 auto;
        }
      }

      body {
        margin: 0;
        padding: 0;
      }

      table,
      tr,
      td {
        vertical-align: top;
        border-collapse: collapse;
      }

      p {
      }

      .ie-container table,
      .mso-container table {
        table-layout: fixed;
      }

      * {
        line-height: inherit;
      }

      a[x-apple-data-detectors='true'] {
        color: inherit !important;
        text-decoration: none !important;
      }

      table,
      td {
        color: #fff;
      }

      #u_body a {
        color: #0000ee;
        text-decoration: underline;
      }
    </style>



    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Manrope:300,400,500,600,700,800" rel="stylesheet" type="text/css">
    <!--<![endif]-->

  </head>

  <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #f5f5f5;color: #000000">
    <!--[if IE]><div class="ie-container"><![endif]-->
    <!--[if mso]><div class="mso-container"><![endif]-->
    <table id="u_body" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #f5f5f5;width:100%" cellpadding="0" cellspacing="0">
      <tbody>
        <tr style="vertical-align: top">
          <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #f5f5f5;"><![endif]-->


            <div class="u-row-container" style="padding: 0px;background-image: url('{{asset("images/bg.jpg")}}');background-repeat: no-repeat;background-position: center top;background-color: transparent;Margin: 0 auto;min-width: 320px;max-width: 600px;background-size: cover;">
              <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;">
                <div style="border-collapse: collapse;display: table;width: 100%;height: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-image: url('https://assets.unlayer.com/projects/140088/1678135950728-email-bg.jpg');background-repeat: no-repeat;background-position: center top;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:600px;"><tr style="background-color: transparent;"><![endif]-->

                  <!--[if (mso)|(IE)]><td align="center" width="600" style="width: 600px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100" style="max-width: 320px;min-width: 600px;display: table-cell;vertical-align: top;">
                    <div style="height: 100%;width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div style="box-sizing: border-box; height: 100%; padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->

                        <table style="font-family:'Manrope',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td style="overflow-wrap:break-word;word-break:break-word;padding: 60px 10px 40px;font-family:'Manrope',sans-serif;" align="left">

                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                  <tbody><tr>
                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">

                                      <img align="center" border="0" src="{{asset('images/logo.png')}}" alt="" title="" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 100%;max-width: 185px;" width="313">

                                    </td>
                                  </tr>
                                </tbody></table>

                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <table style="font-family:'Manrope',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td style="overflow-wrap:break-word;word-break:break-word;padding:30px 10px 10px;font-family:'Manrope',sans-serif;" align="left">

                                <h2 style="margin: 0px;color: #fff;text-align: center;word-wrap: break-word;font-family: inherit;font-size: 30px;font-weight: 600;">
                                  Hi <strong>Captain,</strong>
                                </h2>

                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <table style="font-family:'Manrope',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td style="overflow-wrap:break-word;word-break:break-word;padding:25px 10px 45px;font-family:'Manrope',sans-serif;font-weight: 600;" align="left">

                                <h3 style="margin: 0px; color: #fff;text-align: center; word-wrap: break-word; font-size: 30px; font-weight: 700; ">Login Credentials </h3>

                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <table style="font-family:'Manrope',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td style="overflow-wrap:break-word;word-break:break-word;padding: 0 34px 50px;font-family:'Manrope',sans-serif;" align="left">

                                <div style="color: #fff;text-align: center;word-wrap: break-word;font-size: 18px;line-height: 25px;font-weight: 500;">
                                  <p style="color: #fff;">We are pleased to inform you that your account has been successfully created on Drive me Captain. As a part of our security protocol, we have generated a unique one-time password for your first-time login.</p>
                                  <p style="color: #fff;">Please use the following credentials to access your account:</p>
                                  <p style="color: #fff;">Email: {!! $content['email'] !!}</p>
                                  <p style="color: #fff; margin-bottom: 0; ">Password: {!! $content['password'] !!}</p>
                                  <p style=" color: #fff;margin-top: 7px; ">Copyright © {{ date('Y') }}</p>
                                  <p style="color: #fff;font-weight: 600;font-size: 24px;line-height: 33px;"><strong>{{ env('APP_NAME') }}</strong></p>
                                  <h4 style="color: #fff;font-weight: 500;font-size: 24px;line-height: 33px;margin: 10px 0 10px;">{{ env('APP_NAME') }} Assistance</h4>
                                  <p style="color: #fff; font-size: 16px;line-height: 22px;text-align: center;">If you have any question, feel free message us at {{ strtolower(env('support_email')) }}. All rights reserved. Update email preference or unsubscribe.</p>
                                  {{-- <p style="color: #fff;font-size: 16px;line-height: 22px;text-align: center;padding: 0 35px;">1539 Charack Road, Washington, Indiana, Zip code  47501 United States</p> --}}
                                  <p style=" font-weight: 500; font-size: 16px; line-height: 22px; text-align: center; color: #fff; ">
                                    <a rel="noopener" href="https://drivemeapp.ae/terms-of-use/" target="_blank" style="font-weight: 500; font-size: 16px; line-height: 22px; text-align: center; color: #fff; text-decoration: none; margin: 0 20px; display: inline-block;">Terms of Use</a> | <a rel="noopener" href="https://drivemeapp.ae/privacy-policy/" target="_blank" style="font-weight: 500; font-size: 16px; line-height: 22px; text-align: center; color: #fff; text-decoration: none; margin: 0 20px; display: inline-block;">Privacy Policy</a></p>
                                </div>

                              </td>
                            </tr>
                          </tbody>
                        </table>

                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
  </body>
  </html>
