<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <div class="me-3">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
                <span class="icon-menu"></span>
            </button>
        </div>
        <div>
            <a class="navbar-brand brand-logo" href="{{ route('dashboard') }}">
                {{-- <img src="{{ asset('admin/images/logo.svg') }}" alt="logo" /> --}}
                <img src="{{asset('admin/images/logo.png')}}" alt="">
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{ route('dashboard') }}">
                {{-- <img src="{{ asset('admin/images/logo-mini.svg') }}" alt="logo" /> --}}
                <img src="{{asset('admin/images/logo.png')}}" alt="">
            </a>
        </div>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-top">
        <ul class="navbar-nav">
            <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
                <h1 class="welcome-text">Good Morning, <span
                        class="text-black fw-bold">{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</span>
                </h1>
                {{-- <h3 class="welcome-sub-text">Your performance summary this week </h3> --}}
            </li>
        </ul>
        <ul class="navbar-nav ms-auto">
            <li class="nav-item dropdown">
                <a class="nav-link count-indicator" id="notificationDropdown" href="#" data-bs-toggle="dropdown">
                    <i class="icon-mail icon-lg"></i>
                </a>
                @complaints()
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link count-indicator" id="countDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="icon-bell"></i>
                    <span class="count"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="countDropdown">
                    <a href="{{ route('notification-all') }}" class="dropdown-item py-3">
                        <p class="mb-0 font-weight-medium float-left">
                            You have {{ count(Auth::user()->unreadNotifications()->get()->toArray()) }} new Notification
                        </p>
                        <span class="badge badge-pill badge-primary float-right">View all</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    @notifications()
                </div>
            </li>
            <li class="nav-item dropdown d-none d-lg-block user-dropdown">
                <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                    <img class="img-xs rounded-circle" src="{{ asset(Auth::user()->profile_image??'admin/images/faces/face8.jpg') }}" alt="Profile image"> </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown" data-bs-popper="none">
                    <div class="dropdown-header">
                        <img class="img-md rounded-circle" src="{{ asset(Auth::user()->profile_image??'admin/images/faces/face8.jpg') }}" alt="Profile image">
                        <div class="prof-det">
                            <p class="mb-1 font-weight-semibold">{{ Auth::user()->full_name }}</p>
                            <p class="fw-light text-muted mb-0">{{ Auth::user()->email }}</p>
                        </div>
                    </div>
                    <a class="dropdown-item" href="{{route('profile')}}"><i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> My Profile</a>
                    <a class="dropdown-item" href="{{route('change-password')}}"><i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> Change Password</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Sign Out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
    </div>
</nav>
