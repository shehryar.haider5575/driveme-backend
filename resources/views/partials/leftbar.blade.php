<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{route('dashboard')}}">
          <i class="mdi mdi-grid-large menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#driver" aria-expanded="false" aria-controls="driver">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Driver</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="driver">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('driver.create')}}">Add Driver</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('driver.index')}}">Driver List</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('driver.requested')}}">Driver Requests</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#trips" aria-expanded="false" aria-controls="trips">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Bookings</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="trips">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('user-trips.index') }}">Booking History</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#payments" aria-expanded="false" aria-controls="payments">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Payments</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="payments">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('payments.index') }}">Payments History</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#promocodes" aria-expanded="false" aria-controls="promocodes">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Promocodes</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="promocodes">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('promocodes.index') }}">Promocodes</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('promocodes.create') }}">Add Promocode</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#cars" aria-expanded="false" aria-controls="cars">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Cars</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="cars">
          <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="{{ route('brands.index') }}">Brands</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('models.index') }}">Models</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('brands.create') }}">Add Brand</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('models.create') }}">Add Model</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#user" aria-expanded="false" aria-controls="user">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">User Management</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="user">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('users.index')}}">User List</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('users.create')}}">Add User</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#user-complaints" aria-expanded="false" aria-controls="user-complaints">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Customer Complaints</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="user-complaints">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('user-complaints.index')}}">Complaint List</a></li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#rental-cars" aria-expanded="false" aria-controls="rental-cars">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Rental Cars</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="rental-cars">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('rental-cars.index')}}">Rental Car List</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('booking-cars.index')}}">Booking Car List</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('rental-cars.create')}}">Add Rental Car</a></li>
          </ul>
        </div>
      </li>


      <li class="nav-item">
        <a class="nav-link" href="https://web.whatsapp.com/">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Chat</span>
          <i class="menu-arrow"></i>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#contentManagement" aria-expanded="false" aria-controls="contentManagement">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Content Management</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="contentManagement">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{route('generalsetting.create')}}">General Setting</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('faqs.index')}}">FAQs</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{route('appbanner.index')}}">App Banner</a></li>
          </ul>
        </div>
      </li>

    <li class="nav-item nav-category">Custom Notifications</li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('custom-notification.index') }}">
          <i class="menu-icon mdi mdi-floor-plan"></i>
          <span class="menu-title">Send Notification</span>
          <i class="menu-arrow"></i>
        </a>
      </li>
      {{--
      <li class="nav-item nav-category">Forms and Datas</li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
          <i class="menu-icon mdi mdi-card-text-outline"></i>
          <span class="menu-title">Form elements</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="form-elements">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
          <i class="menu-icon mdi mdi-chart-line"></i>
          <span class="menu-title">Charts</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="charts">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/charts/chartjs.html">ChartJs</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
          <i class="menu-icon mdi mdi-table"></i>
          <span class="menu-title">Tables</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="tables">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/tables/basic-table.html">Basic table</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
          <i class="menu-icon mdi mdi-layers-outline"></i>
          <span class="menu-title">Icons</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="icons">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/icons/mdi.html">Mdi icons</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item nav-category">pages</li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
          <i class="menu-icon mdi mdi-account-circle-outline"></i>
          <span class="menu-title">User Pages</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="auth">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item nav-category">help</li>
      <li class="nav-item">
        <a class="nav-link" href="http://bootstrapdash.com/demo/star-admin2-free/docs/documentation.html">
          <i class="menu-icon mdi mdi-file-document"></i>
          <span class="menu-title">Documentation</span>
        </a>
      </li> --}}
    </ul>
  </nav>
