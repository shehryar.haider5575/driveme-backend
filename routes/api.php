<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Auth\LoginController;
use App\Http\Controllers\API\Auth\RegisterController;
use App\Http\Controllers\API\Auth\UserController;
use App\Http\Controllers\API\Auth\ForgotPasswordController;
use App\Http\Controllers\API\Controllers\CarController;
use App\Http\Controllers\API\Controllers\UserTripAPIController;
use App\Http\Controllers\API\Controllers\BankDetailsController;
use App\Http\Controllers\API\Controllers\UserComplaintAPIController;
use App\Http\Controllers\API\Controllers\DeliveryAddressAPIController;
use App\Http\Controllers\API\Controllers\ReviewRatingAPIController;
use App\Http\Controllers\API\Controllers\GeneralSettingApiController;
use App\Http\Controllers\API\Controllers\DriverTripController;
use App\Http\Controllers\API\Controllers\PaymentAPIController;
use App\Http\Controllers\API\Controllers\RentalCarAPIController;
use App\Http\Controllers\API\Controllers\MessageController;
use Workerman\Worker;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('user/login', [LoginController::class, 'login']);
Route::post('user/verified', [LoginController::class, 'userVerified']);
Route::post('user/register', [RegisterController::class, 'register'])->name('user');
Route::post('vendor/register', [RegisterController::class, 'register'])->name('captain');
Route::post('forgot/password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('forgot-password');
Route::post('confirm/otp', [ForgotPasswordController::class, 'validateOTP'])->name('verify-otp');
Route::post('reset/password', [ForgotPasswordController::class, 'resetPassword'])->name('reset-password');
Route::get('settings/{key}', [GeneralSettingApiController::class, 'getGeneralSetting']);
Route::get('banner', [GeneralSettingApiController::class, 'Banner']);

Route::middleware(['auth:api', 'checkuser'])->group(function () {
    Route::get('faqs', [GeneralSettingApiController::class, 'Faqs']);
    Route::post('verify/otp', [UserController::class, 'verify_otp']);
    Route::get('user', [UserController::class, 'user']);
    Route::get('notifications', [UserController::class, 'getNotifications']);
    Route::get('notifications/{id}', [UserController::class, 'getNotificationRead']);
    Route::post('notifications/all', [UserController::class, 'readAllNotification']);
    Route::get('car/brands', [CarController::class, 'getBrands']);
    Route::post('trip/estimation', [UserTripAPIController::class, 'getEstimationPrice']);
    Route::post('verified', [UserController::class, 'verified']);
    Route::prefix('user')->group(function(){
        Route::get('/', [UserController::class, 'user']);
        Route::post('/updateprofile', [UserController::class, 'updateprofile']);
        Route::post('/changePassword', [UserController::class, 'changePassword']);
        Route::delete('/delete', [UserController::class, 'DeleteUser']);
        Route::resource('bank/details', BankDetailsController::class);
        Route::get('payments', [PaymentAPIController::class, 'index']);
        Route::post('topup', [PaymentAPIController::class, 'store'])->name('topup');
        Route::post('driver/tip', [PaymentAPIController::class, 'store'])->name('tip');
    });

    Route::get('promocodes', [UserController::class, 'promocodes']);
    Route::resource('address', DeliveryAddressAPIController::class);
    Route::resource('complaints', UserComplaintAPIController::class);
    Route::resource('ratings', ReviewRatingAPIController::class);

    Route::middleware(['is_user'])->group(function () {
        Route::post('trips/pay/{trip_id}', 'API\Controllers\UserTripAPIController@payForTrip');
        Route::resource('cars', CarController::class);
        Route::resource('trips', UserTripAPIController::class);
    });

    Route::middleware('is_captain')->group(function () {
        Route::get('driver/rides', [DriverTripController::class, 'DriverRides']);
        Route::get('driver/rides/{id}', [DriverTripController::class, 'DriverRidesById']);
        Route::get('driver/ride', [DriverTripController::class, 'searchRide']);
        Route::post('driver/ride/{id}', [DriverTripController::class, 'RideStatus']);
    });

    Route::resource('rentalcars', RentalCarAPIController::class);
    Route::post('pay/rental-cars/{id}', [RentalCarAPIController::class, 'payForBooking']);
    Route::get('booking/rental-cars', [RentalCarAPIController::class, 'getBookings']);
    Route::get('booking/rental-cars/{id}', [RentalCarAPIController::class, 'getBookingsById']);
    Route::get('/driver/location', [DriverTripController::class, 'driverBroadcast']);
    // Route::get('/locate/driver', function () {
    //     // print_r('hh');

    // });

    Route::controller(MessageController::class)->group(function(){
        Route::get('getUserWiseMessageList','getUserWiseMessageList');
        Route::get('getUserWiseMessages/{receiverId}','getUserWiseMessages');
        Route::post('message','message');
    });
});

