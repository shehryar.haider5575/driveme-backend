<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('dashboard');
});*/

Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('userDetail/{id}', 'HomeController@userDetail')->name('userDetail');
    Route::get('userDetail/{user_id}/trip/{trip_id}', 'UserTripController@show')->name('user_trip_detail');
    Route::get('/logout', 'HomeController@logout');
    Route::get('/profile', function ()
    {
        return view('pages.user.profile');

    })->name('profile');

    Route::get('/change-password', function ()
    {
        return view('pages.user.change_password');

    })->name('change-password');
    Route::delete('attachment/{id}', 'Admin\GeneralSettingController@delete_attachment')->name('delete_attachment');
    Route::post('/profile', 'Admin\GeneralSettingController@updateProfile')->name('update-profile');
    Route::post('/change-password', 'Admin\GeneralSettingController@updatePassword')->name('update-change-password');

    Route::get('notifications', 'Admin\GeneralSettingController@index')->name('notification-all');
    Route::get('notifications/{id}', 'Admin\GeneralSettingController@show')->name('notification.show');
    Route::get('notifications/read/{id}', 'Admin\GeneralSettingController@edit')->name('notification.read');

    Route::resource('users', 'Admin\UserController');
    Route::put('users/status/{id}', 'Admin\UserController@updateStatus')->name('users.status');
    Route::get('driver/requests', 'Admin\DriverController@Driverrequests')->name('driver.requested');
    Route::get('driver/requests/{id}', 'Admin\DriverController@DriverrequestsUser')->name('driver.requested.id');
    Route::get('driver/accept/{id}', 'Admin\DriverController@DriverAccept')->name('driver.accept');
    Route::get('driver/reject/{id}', 'Admin\DriverController@DriverReject')->name('driver.reject');
    Route::resource('driver', 'Admin\DriverController');
    Route::resource('brands', 'Admin\CarBrandController');
    Route::resource('models', 'Admin\CarModelController');
    Route::resource('faqs', 'Admin\FaqsController');
    Route::resource('generalsetting', 'Admin\GeneralSettingController');
    Route::resource('appbanner', 'Admin\AppBannerController');
    Route::resource('promocodes', 'Admin\PromocodeController');
    Route::resource('user-trips', 'Admin\UserTripController');
    Route::resource('payments', 'Admin\PaymentController');
    Route::resource('user-complaints', 'Admin\ComplaintController');
    Route::resource('custom-notification', 'Admin\CustomEmailController');
    Route::resource('rental-cars', 'Admin\RentalCarController');
    Route::resource('booking-cars', 'Admin\BookedRentalCarController');
    Route::get('booking-cars/{status}/{id}', 'Admin\BookedRentalCarController@updateStatus');
    Route::get('booking-cars/status/{status}/{id}', 'Admin\BookedRentalCarController@updatebookingStatus');
    Route::put('banner/status/{id}', 'Admin\AppBannerController@updateStatus')->name('banner.status');
});

Route::get('storage/app/{dir}/{filename}', function ($dir, $filename) {
    $path = storage_path('app/' . $dir . '/' . $filename);
    /*
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    */
    return response()->file($path);

});

use App\Models\User;
use App\Notifications\ChatMessage;

Route::get('/test/{email}', function ($email) {
    $receiver = User::where('email', $email)->first();
    $receiver->notify(new ChatMessage($receiver, 'Test notification'));
});

// use App\Events\LocateDriver;
// use DB;

// Route::get('/socket-test', function () {
//     $data = [
//         'user_id' => 11,
//         'latitude' => 12.45,
//         'longitude' => 12.45,
//     ];
//     event(new LocateDriver($data));
//     return "ok";
// });
// Route::get('/truncate', function () {
//     DB::statement("SET SQL_SAFE_UPDATES = 0;");
//     DB::table('users')->where('id','!=',382)->delete();
//     return "ok";
//     DB::table('user_addresses')->truncate();
//     DB::table('user_trips')->truncate();
//     DB::table('attachments')->truncate();
// });
